import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import AsyncInput from '/client/components/fuc/AsyncInput'
import InputMask from 'react-input-mask'

export default class AsyncInputContainer extends Component {

  blurHandler(e) {
    const value = e.currentTarget.value
    const {
      context,
      stateName,
      checkMethod,
      label,
      checkException
    } = this.props

    Meteor.call(checkMethod, value, checkException, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        if(res) {
          context.setState({ [`${stateName}AsyncError`]: `${label} уже есть в базе` })
        }
        else {
          context.setState({ [`${stateName}AsyncError`]: `` })
        }
      }
    })
  }

  changeHandler(e) {
    const value = e.currentTarget.value
    const {
      context,
      stateName,
      checkMethod,
      label,
      checkException
    } = this.props

    context.setState({ [stateName]: value }, () => {
      context[`${stateName}Validate`] && context[`${stateName}Validate`]()
    })
  }

  render() {
    const { context, stateName } = this.props

    const errors = context.state[`${stateName}Errors`] || []
    const asyncError = context.state[`${stateName}AsyncError`] || []

    return (
      <div>
        {
          this.props['data-mask'] ? (
            <InputMask
              mask={ this.props['data-mask'] }
              onChange={ this.changeHandler.bind(this) }
              onBlur={ this.blurHandler.bind(this) }
              value={ context.state[stateName] }
              type={ this.props.type }
              placeholder={ this.props.label }
              className='form-control'
            />
          ) : (
            <input
              onBlur={ this.blurHandler.bind(this) }
              onChange={ this.changeHandler.bind(this) }
              value={ context.state[stateName] }
              type={ this.props.type }
              className='form-control'
              placeholder={ this.props.label }
            />
          )
        }
        <ul className="parsley-errors-list filled">
          <li className="parsley-type">{ asyncError }</li>
          {
            errors.map((error, index) => (
              <li key={ index } className="parsley-type">{ error }</li>
            ))
          }
        </ul>
      </div>
    )
  }
}
