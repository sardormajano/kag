import React, { Component } from 'react'
import FileUpload from '/client/components/fuc/FileUpload'
import { DocsCollection } from '/api/docs'

const SIZE_LIMIT = 4194304

export default class FileUploadContainer extends Component {
  uploaderChangeHandler(e) {
    const files = e.currentTarget.files
    const { context, stateName, documentType } = this.props
    const { documentTypes } = context.props
    const theDocumentType = documentTypes.filter(doc => doc._id === documentType.value)[0]

    for(let i = 0, len = files.length; i < len; i++) {
      const theFile = files[i]

      if(theFile.size > SIZE_LIMIT) {
        Bert.alert({
            message: `размер файла "${theFile.name}" превышает ограничение (4МБ)`,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
        continue
      }

      const uploadingFile = {}

      const upload = DocsCollection.insert({
        file: theFile,
      }, false)

      upload.on('progress', (progress, fileObj) => {
        uploadingFile.progress = progress
        context.forceUpdate()
      })

      upload.on('start', () => {
        uploadingFile._id = upload.config.fileId
        uploadingFile.name = theFile.name
        uploadingFile.progress = 0
        uploadingFile.abort = upload.abort
        if(!context.state.steps[stateName][documentType.value]) {
          context.state.steps[stateName][documentType.value] = []
        }

        if(theDocumentType.multiple) {
          context.state.steps[stateName][documentType.value].push(uploadingFile)
        }
        else if(context.state.steps[stateName][documentType.value].length){
          this.deleteFileHandler(context.state.steps[stateName][documentType.value][0]._id)
          context.state.steps[stateName][documentType.value] = [uploadingFile]
        }
        else {
          context.state.steps[stateName][documentType.value] = [uploadingFile]
        }

        context.forceUpdate()
      })

      upload.on('end', function (error, fileObj) {
        if (error) {
          console.log('Error during upload: ' + error)
        } else {
          Bert.alert({
              message: 'Изменения сохранены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          })
          context.updateRequest()
        }
      })

      upload.start()
    }
  }

  deleteFileHandler(file) {
    const { context, stateName, documentType } = this.props

    const newArray = context.state.steps[stateName][documentType.value].filter(uf => uf !== file)
    context.state.steps[stateName][documentType.value] = newArray
    context.updateRequest()
    DocsCollection.remove(file._id)
    Bert.alert({
        message: 'Изменения сохранены!',
        type: 'success',
        style: 'growl-top-right',
        icon: 'fa-user'
    })
    context.forceUpdate()
  }

  render() {
    const {
      context,
      documentType,
      stateName
    } = this.props

    return (
      <FileUpload
        deleteFileHandler={ this.deleteFileHandler.bind(this) }
        uploaderChangeHandler={ this.uploaderChangeHandler.bind(this) }
        context={ context }
        documentType={ documentType }
        stateName={ stateName }
      />
    )
  }
}
