import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import SideMenu from '/client/components/fuc/SideMenu'

const defaultState = {
  branches: false,
  departments: false,
  enterpreneurTypes: false,
  enterpreneurCategories: false,
  subjectCategories: false,
  currencies: false,
  productTypes: false,
  users: false,
  roles: false,
  organizations: false,
  documentTypes: false,
  paymentOrders: false,
  education: false,
  steps: false,
  templates: false,
  requests: false,
  committee: false,
  logs: false,
  territories: false,
  activities: false,
  loanPrograms: false,
  banks: false,
}

class SideMenuContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    const permissions = this.state
    const { userId } = this.props

    for(let key in permissions) {
      Meteor.call('page.permission.check', userId, key, 'r', (err, res) => {
        if(err)
        {
          console.log(err.reason)
        }
        else {
          this.setState({ [key]: res })
        }
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.userId !== this.props.userId)
      return true

    if(JSON.stringify(nextState) !== JSON.stringify(this.state))
      return true

    if(nextProps.lang !== this.props.lang)
      return true

    return false
  }

  render() {
    const { locStrings, lang } = this.props

    this.pageLabels = {
      branches: locStrings[lang].branches,
      departments: locStrings[lang].departments,
      banks: locStrings[lang].banks,
      currencies: locStrings[lang].currencies,
      paymentOrders: locStrings[lang].paymentOrders,
      education: locStrings[lang].education,
      productTypes: locStrings[lang].productTypes,
      enterpreneurTypes: locStrings[lang].enterpreneurTypesShort,
      enterpreneurCategories: locStrings[lang].enterpreneurCategories,
      subjectCategories: locStrings[lang].subjectCategories,
      users: locStrings[lang].users,
      roles: locStrings[lang].roles,
      organizations: locStrings[lang].organizations,
      documentTypes: locStrings[lang].documentTypes,
      steps: locStrings[lang].steps,
      templates: locStrings[lang].templates,
      requests: locStrings[lang].myRequests,
      committee: locStrings[lang].committee,
      logs: locStrings[lang].logs,
      territories: locStrings[lang].territories,
      activities: locStrings[lang].activities,
      loanPrograms: locStrings[lang].loanPrograms
    }

    this.pageIcons = {
      branches: 'mdi mdi-map-marker',
      departments: 'mdi mdi-file-tree',
      banks: 'mdi mdi-file-tree',
      currencies: 'mdi mdi-currency-usd',
      paymentOrders: 'mdi mdi-elevation-decline',
      education: 'mdi mdi-book-open-page-variant',
      users: 'mdi mdi-account-multiple',
      roles: 'mdi mdi-account-check',
      organizations: 'mdi mdi-briefcase',
      productTypes: 'mdi mdi-file-tree',
      enterpreneurTypes: 'mdi mdi-cookie',
      enterpreneurCategories: 'mdi mdi-apps',
      subjectCategories: 'mdi mdi-basket-fill',
      documentTypes: 'mdi mdi-file-document-box',
      steps: 'mdi mdi-debug-step-over',
      templates: 'mdi mdi-vector-polyline',
      requests: 'mdi mdi-file-multiple',
      committee: 'mdi mdi-account-multiple',
      logs: 'mdi mdi-view-list',
      territories: 'mdi mdi-map-marker-multiple',
      activities: 'mdi mdi-worker',
      loanPrograms: 'mdi mdi-cash'
    }

    return (
      <SideMenu context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  return {
    ...props
  }
}, SideMenuContainer)
