import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Currencies from '/client/components/page/Currencies'
import { CurrenciesCollection } from '/api/currencies'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  ccmNameRu: '',
  ccmNameKz: '',

  ecmNameKz: '',
  ecmNameRu: '',
  ecmCurrencyId: '',
}

class CurrenciesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteCurrency(_id) {
    Meteor.call('currency.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].currencyDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertCurrency() {
    const data = {
      name: {
        ru: this.state.ccmNameRu,
        kz: this.state.ccmNameKz
      }
    }

    Meteor.call('currency.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].currencyCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  updateCurrency() {
    const data = {
      name: {
        ru: this.state.ecmNameRu,
        kz: this.state.ecmNameKz
      }
    }

    Meteor.call('currency.update', this.state.ecmCurrencyId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].currencyEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Currencies context={ this } locStrings={locStrings[lang]} />
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['currencies'])

  return {
    currencies: CurrenciesCollection.find().fetch(),
    ...props
  }
}, CurrenciesContainer)
