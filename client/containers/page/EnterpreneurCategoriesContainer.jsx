import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import EnterpreneurCategories from '/client/components/page/EnterpreneurCategories'
import { EnterpreneurCategoriesCollection } from '/api/enterpreneurCategories'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cmNameRu: '',
  cmNameKz: '',
  cmCreditLimit: '',

  emNameKz: '',
  emNameRu: '',
  emCreditLimit: '',
  emEnterpreneurCategoryId: '',
}

class EnterpreneurCategoriesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteEnterpreneurCategory(_id) {
    Meteor.call('enterpreneurCategory.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].enterpreneurCategoryDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertEnterpreneurCategory() {
    const data = {
      name: {
        ru: this.state.cmNameRu,
        kz: this.state.cmNameKz
      },
      creditLimit: this.state.cmCreditLimit
    }

    Meteor.call('enterpreneurCategory.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].enterpreneurCategoryCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  updateEnterpreneurCategory() {
    const data = {
      name: {
        ru: this.state.emNameRu,
        kz: this.state.emNameKz
      },
      creditLimit: this.state.emCreditLimit
    }

    Meteor.call('enterpreneurCategory.update', this.state.emEnterpreneurCategoryId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].enterpreneurCategoryEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <EnterpreneurCategories context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['enterpreneurCategories'])

  return {
    enterpreneurCategories: EnterpreneurCategoriesCollection.find().fetch(),
    ...props,
  }
}, EnterpreneurCategoriesContainer)
