import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import NewStep from '/client/components/page/steps/NewStep'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import { DocumentTypesCollection } from '/api/documentTypes'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  name: '',
  description: '',
  documentTypes: [],
  order: ''
}

class NewStepContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  levelDocumentUp(index) {
    if(index === 0)
      return

    const { documentTypes } = this.state

    const temp = documentTypes[index - 1]
    documentTypes[index - 1] = documentTypes[index]
    documentTypes[index] = temp

    this.setState({ documentTypes })
  }

  levelDocumentDown(index) {
    if(index === this.state.documentTypes.length - 1)
      return

    const { documentTypes } = this.state

    const temp = documentTypes[index + 1]
    documentTypes[index + 1] = documentTypes[index]
    documentTypes[index] = temp

    this.setState({ documentTypes })
  }

  createStep() {
    const data = {
      name: this.state.name,
      description: this.state.description,
      documentTypes: this.state.documentTypes,
    }

    Meteor.call('step.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].stepCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  render() {
    const { documentTypes, locStrings, lang } = this.props

    this.documentTypeOptions = documentTypes.map(documentType => {
      return {
        value: documentType._id,
        label: documentType.name.ru
      }
    })

    return (
      <NewStep context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['documentTypes'])
  return {
    ...props,
    documentTypes: DocumentTypesCollection.find({
      busy: {
        $ne: true
      }
    }).fetch()
  }
}, NewStepContainer)
