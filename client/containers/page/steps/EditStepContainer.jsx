import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import EditStep from '/client/components/page/steps/EditStep'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import { DocumentTypesCollection } from '/api/documentTypes'
import { StepsCollection } from '/api/steps'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  name: '',
  description: '',
  documentTypes: [],
  order: '',
}

class EditStepContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)

    Meteor.call('get.step', this.props.match.params._id, (err, res) => {
      this.setState(res)
    })
  }

  levelDocumentUp(index) {
    if(index === 0)
      return

    const { documentTypes } = this.state

    const temp = documentTypes[index - 1]
    documentTypes[index - 1] = documentTypes[index]
    documentTypes[index] = temp

    this.setState({ documentTypes })
  }

  levelDocumentDown(index) {
    if(index === this.state.documentTypes.length - 1)
      return

    const { documentTypes } = this.state

    const temp = documentTypes[index + 1]
    documentTypes[index + 1] = documentTypes[index]
    documentTypes[index] = temp

    this.setState({ documentTypes })
  }

  updateStep() {
    const data = {
      name: this.state.name,
      description: this.state.description,
      documentTypes: this.state.documentTypes,
    }

    Meteor.call('step.update', this.props.match.params._id, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].stepEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }
    })
  }

  render() {
    const { documentTypes, locStrings, lang } = this.props

    this.documentTypeOptions = documentTypes.map(documentType => {
      return {
        value: documentType._id,
        label: documentType.name.ru
      }
    }).concat(this.state.documentTypes)

    return (
      <EditStep context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['documentTypes'])

  return {
    ...props,
    documentTypes: DocumentTypesCollection.find({
      busy: {
        $ne: true
      }
    }).fetch(),
  }
}, EditStepContainer)
