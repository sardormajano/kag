import React, { Component } from 'react'
import ConfirmedEmail from '/client/components/page/ConfirmedEmail'
import { createContainer } from 'meteor/react-meteor-data'

import { cookie } from '/client/lib/coreLib'
import {locStrings} from '/client/lib/localization'

const defaultState = {

}

class ConfirmedEmailContainer extends Component {
  constructor(props) {
    super(props)

    let lang = cookie.get('lang')
    if(lang === '')
    {
      cookie.set('lang', 'ru', 9999);
      lang = 'ru';
    }
    defaultState.lang = lang
    this.state = defaultState
  }

  componentWillMount() {
    const { props } = this

    Meteor.call('verify.email', props.match.params._id, (err, res) => {
      if(!res) {
        props.history.push('/login')
      }
      else {
        this.setState({
          verified: true,
          address: res
        })
      }
    })
  }

  render() {
    if(!this.state.verified)
      return <div />

    return (
      <ConfirmedEmail context={ this }/>
    )
  }
}

export default createContainer(props => {
  return {
    locStrings,
    ...props
  }
}, ConfirmedEmailContainer)
