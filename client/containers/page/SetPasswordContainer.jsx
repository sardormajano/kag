import React, { Component } from 'react'
import SetPassword from '/client/components/page/SetPassword'
import { createContainer } from 'meteor/react-meteor-data'

import { cookie } from '/client/lib/coreLib'
import {locStrings} from '/client/lib/localization'

const defaultState = {
  password: '',
  passwordConfirm: '',
  resetSuccess: false,
}

function validatePassword(data) {
  const errorMessage = [];

  if(data.password !== data.passwordConfirm)
    errorMessage.push(locStrings[cookie.get('lang')].passMatchError)

  return errorMessage
}

class SetPasswordContainer extends Component {
  constructor(props) {
    super(props)

    let lang = cookie.get('lang')
    if(lang === '')
    {
      cookie.set('lang', 'ru', 9999);
      lang = 'ru';
    }
    defaultState.lang = lang
    this.state = defaultState
  }

  onResetPassword(e) {
    e.preventDefault()

    const errorMessage = validatePassword(this.state)

    if(errorMessage.length) {
      errorMessage.forEach(message => {
        Bert.alert({
            message: message,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      })

      return
    }
    Meteor.call('reset.password', this.props.match.params._id, this.state.password, (err, res) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        this.setState({resetSuccess: true})
      }
    })
  }

  onLangChangeHandler(e) {
    let lang = e.target.id

    defaultState.lang = lang
    this.setState(defaultState)
  }

  render() {
    return (
      <SetPassword context={ this }/>
    )
  }
}

export default createContainer(props => {
  return {
    locStrings,
    ...props
  }
}, SetPasswordContainer)
