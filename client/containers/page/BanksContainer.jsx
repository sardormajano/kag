import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Banks from '/client/components/page/Banks'
import { BanksCollection } from '/api/banks'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cdmNameRu: '',
  cdmNameKz: '',
  cdmCode: '',

  edmNameKz: '',
  edmNameRu: '',
  edmCode: '',
  edmBankId: '',
}

class BanksContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteBank(_id) {
    Meteor.call('bank.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].bankDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertBank() {
    const data = {
      name: {
        ru: this.state.cdmNameRu,
        kz: this.state.cdmNameKz
      },
      bik: this.state.cdmCode
    }

    Meteor.call('bank.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].bankCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updateBank() {
    const data = {
      name: {
        ru: this.state.edmNameRu,
        kz: this.state.edmNameKz
      },
      bik: this.state.edmCode
    }

    Meteor.call('bank.update', this.state.edmBankId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].bankEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Banks context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['banks'])

  return {
    banks: BanksCollection.find().fetch(),
    ...props
  }
}, BanksContainer)
