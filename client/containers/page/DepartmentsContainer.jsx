import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Departments from '/client/components/page/Departments'
import { DepartmentsCollection } from '/api/departments'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cdmNameRu: '',
  cdmNameKz: '',
  cdmCode: '',

  edmNameKz: '',
  edmNameRu: '',
  edmCode: '',
  edmDepartmentId: '',
}

class DepartmentsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteDepartment(_id) {
    Meteor.call('department.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].departmentDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertDepartment() {
    const data = {
      name: {
        ru: this.state.cdmNameRu,
        kz: this.state.cdmNameKz
      },
      code: this.state.cdmCode
    }

    Meteor.call('department.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].departmentCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updateDepartment() {
    const data = {
      name: {
        ru: this.state.edmNameRu,
        kz: this.state.edmNameKz
      },
      code: this.state.edmCode
    }

    Meteor.call('department.update', this.state.edmDepartmentId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].departmentEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Departments context={ this } locStrings={locStrings[lang]} />
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['departments'])

  return {
    departments: DepartmentsCollection.find().fetch(),
    ...props
  }
}, DepartmentsContainer)
