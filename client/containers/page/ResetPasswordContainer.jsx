import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import ResetPassword from '/client/components/page/ResetPassword'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import { iinCheck } from '/client/lib/formValidationRelated'
import { cookie } from '/client/lib/coreLib'

const defaultState = {
  login: '',
  email: '',
  verified: '',
  displayBlock: false,
  loading: false,

  loginErrors: [],
}

class ResetPasswordContainer extends Component {
  constructor(props) {
    super(props)

    let lang = cookie.get('lang')
    if(lang === '')
    {
      cookie.set('lang', 'ru', 9999);
      lang = 'ru';
    }
    defaultState.lang = lang
    this.state = defaultState
  }

  onEmailPasswordReset(e) {
    e.preventDefault()

    let _id = this.state.login
    this.setState({loading: true, displayBlock: false})

    Meteor.call('check.emailVerification', _id, (err, res) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      } else {
        const email = res.address
        const verified = res.verified

        this.setState({
          verified,
          email,
          displayBlock: true,
          loading: false
        })
      }
    })
  }

  reSendConfirmationEmail() {
    Meteor.call('send.confirmation.email', this.state.email)
    this.props.history.push(`/confirmEmail/${this.state.email}`)
  }

  onLangChangeHandler(e) {
    let lang = e.target.id

    defaultState.lang = lang
    this.setState(defaultState)
  }

  loginValidate() {
    const iinCorrect = iinCheck(this.state.login)

    this.setState({ loginErrors: iinCorrect ? [] : [this.props.locStrings[this.state.lang].validErr] })
  }

  render() {
    return (
      <ResetPassword context={ this }/>
    );
  }
}

export default createContainer(() => {
  return {}
}, ResetPasswordContainer)
