import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import { Redirect } from 'react-router-dom'

import Roles from '/client/components/page/Roles'

import { PagesCollection } from '/api/pages'
import { CustomRolesCollection } from '/api/customRoles'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  //crm for create role modal
  //erm for edit role modal

  crmName: '',
  crmPairs: [{ page: '', actions: [] }],
  ermName: '',
  ermPairs: [{ page: '', actions: [] }],
  ermRoleId: '',

}

class RolesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  setModalDataPair(index, property, value, editing) {
    const { crmPairs, ermPairs } = this.state

    if(!editing) {
      crmPairs[index][property] = value

      this.setState({
        crmPairs
      })
    }
    else {
      ermPairs[index][property] = value

      this.setState({
        ermPairs
      })
    }
  }

  insertRole() {
    const data = {
      name: this.state.crmName,
      pairs: this.state.crmPairs
    }

    Meteor.call('customRole.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].roleCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  updateRole() {
    const data = {
      name: this.state.ermName,
      pairs: this.state.ermPairs
    }

    Meteor.call('customRole.update', this.state.ermRoleId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].roleEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  deleteRole(_id) {
    Meteor.call('customRole.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].roleDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { pages, customRoles, locStrings, lang } = this.props

    this.pageOptions = pages.map(page => {
      return {
        label: page.name,
        value: page._id
      }
    })

    this.actionOptions = [
      { value: 'cu', label: 'создавать/редактировать'},
      { value: 'r', label: 'просматривать'},
      { value: 'd', label: 'удалять'},
    ]

    return (
      <Roles context={this} locStrings={locStrings[lang]}/>
    );
  }
}

export default createContainer((props) => {
  multipleSubscribe(['pages', 'customRoles'])

  return {
    customRoles: CustomRolesCollection.find({
      _id: { $ne: '0' }
    }).fetch(),
    pages: PagesCollection.find().fetch(),
    ...props,
  }
}, RolesContainer)
