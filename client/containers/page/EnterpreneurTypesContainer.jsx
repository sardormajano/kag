import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import EnterpreneurTypes from '/client/components/page/EnterpreneurTypes'
import { EnterpreneurTypesCollection } from '/api/enterpreneurTypes'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cmNameRu: '',
  cmNameKz: '',

  emNameKz: '',
  emNameRu: '',
  emEnterpreneurTypeId: '',
}

class EnterpreneurTypesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteEnterpreneurType(_id) {
    Meteor.call('enterpreneurType.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].enterpreneurTypeDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertEnterpreneurType() {
    const data = {
      name: {
        ru: this.state.cmNameRu,
        kz: this.state.cmNameKz
      }
    }

    Meteor.call('enterpreneurType.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].enterpreneurTypeCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  updateEnterpreneurType() {
    const data = {
      name: {
        ru: this.state.emNameRu,
        kz: this.state.emNameKz
      }
    }

    Meteor.call('enterpreneurType.update', this.state.emEnterpreneurTypeId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].enterpreneurTypeEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <EnterpreneurTypes context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['enterpreneurTypes'])

  return {
    enterpreneurTypes: EnterpreneurTypesCollection.find().fetch(),
    ...props,
  }
}, EnterpreneurTypesContainer)
