import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Activities from '/client/components/page/Activities'
import { ActivitiesCollection } from '/api/activities'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cmNameRus: '',
  cmNameKaz: '',
  cmCode: '',
  cmId: '',
  cmParent: '',

  emNameRus: '',
  emNameKaz: '',
  emCode: '',
  emId: '',
  emParent: '',
  em_id: '',

  page: 1,
  pageSize: 10,
  searchText: ''
}

class ActivitiesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)

    this.getPage()
  }

  deleteActivity(_id) {
    Meteor.call('activity.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].activityDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertActivity() {
    const data = {
      NameRus: this.state.cmNameRus,
      NameKaz: this.state.cmNameKaz,
      Code: parseInt(his.state.cmCode),
      Id: parseInt(this.state.cmId),
      Parent: parseInt(this.state.cmParent),
      AreaType: parseInt(this.state.cmAreaType)
    }

    Meteor.call('activity.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].activityCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updateActivity() {
    const data = {
      NameRus: this.state.emNameRus,
      NameKaz: this.state.emNameKaz,
      Code: parseInt(this.state.emCode),
      Id: parseInt(this.state.emId),
      Parent: parseInt(this.state.emParent),
      AreaType: parseInt(this.state.emAreaType)
    }

    Meteor.call('activity.update', this.state.em_id, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].activityEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.getPage()
        this.setState(defaultState)
      }
    });
  }

  updateKato() {
    Meteor.call('oked.update', (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.getPage()
      }
    })
  }

  getPage(e) {
    e && e.preventDefault()
    Meteor.call('get.activities.by.page', this.state.page, this.state.pageSize, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.setState({ activities: res })
      }
    })
  }

  searchActivity(e) {
    e.preventDefault()

    Meteor.call('get.activities.by.search', this.state.searchText, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.setState({ activities: res })
      }
    })
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Activities context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  return {
    ...props
  }
}, ActivitiesContainer)
