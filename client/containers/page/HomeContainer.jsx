import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

const defaultState = {}

class HomeContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Home context={ this } locStrings={locStrings[lang]}/>
    );
  }
}

export default createContainer((props) => {
  return {
    ...props
  }
}, HomeContainer)
