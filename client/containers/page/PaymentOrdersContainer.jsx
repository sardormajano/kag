import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import PaymentOrders from '/client/components/page/PaymentOrders'
import { PaymentOrdersCollection } from '/api/paymentOrders'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cpmNameRu: '',
  cpmNameKz: '',

  epmNameKz: '',
  epmNameRu: '',
  epmPaymentOrderId: ''
}

class PaymentOrdersContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deletePaymentOrder(_id) {
    Meteor.call('paymentOrder.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].paymentOrderDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertPaymentOrder() {
    const data = {
      name: {
        ru: this.state.cpmNameRu,
        kz: this.state.cpmNameKz
      }
    }

    Meteor.call('paymentOrder.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].paymentOrderCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updatePaymentOrder() {
    const data = {
      name: {
        ru: this.state.epmNameRu,
        kz: this.state.epmNameKz
      }
    }

    Meteor.call('paymentOrder.update', this.state.epmPaymentOrderId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].paymentOrderEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <PaymentOrders context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['paymentOrders'])

  return {
    paymentOrders: PaymentOrdersCollection.find().fetch(),
    ...props
  }
}, PaymentOrdersContainer)
