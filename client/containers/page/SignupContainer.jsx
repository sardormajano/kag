import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import Signup from '/client/components/page/Signup'

import { BranchesCollection } from '/api/branches'

import { iinCheck } from '/client/lib/formValidationRelated'
import { cookie } from '/client/lib/coreLib'
import {locStrings} from '/client/lib/localization'

const defaultState = {
  name: '',
  email: '',
  username: '',
  representative: '',
  branch: '',
  mobilePhone: '',
  password: '',
  passwordConfirm: '',
  acceptConditions: false,

  usernameErrors: []
}

function validateSignup(data) {
  const errorMessage = [];

  if(data.password !== data.passwordConfirm)
    errorMessage.push(locStrings[cookie.get('lang')].passMatchError)

  return errorMessage
}

class SignupContainer extends Component {
  constructor(props) {
    super(props)

    let lang = cookie.get('lang')
    if(lang === '')
    {
      cookie.set('lang', 'ru', 9999);
      lang = 'ru';
    }
    defaultState.lang = lang
    this.state = defaultState
  }

  usernameValidate() {
    const iinCorrect = iinCheck(this.state.username)

    this.setState({ usernameErrors: iinCorrect ? [] : [this.props.locStrings[this.state.lang].validErr] })
  }

  formSubmitHandler(e) {
    e.preventDefault()

    const errorMessage = validateSignup(this.state)

    if(errorMessage.length) {
      errorMessage.forEach(message => {
        Bert.alert({
            message: message,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      })

      return
    }

    const data = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
      profile: {
        shortName: this.state.name,
        representative: this.state.representative,
        mobilePhone: this.state.mobilePhone,
        branch: this.state.branch,
        isOrganization: true
      }
    }

    Meteor.call('create.user', data, err => {
      if(err) {
        Bert.alert({
          message: err.reason,
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.state.lang].regSuccess,
          type: 'success',
          style: 'growl-top-right',
          icon: 'fa-user'
        })
        Meteor.call('send.confirmation.email', data.email)
        this.props.history.push(`/confirmEmail/${data.email}`)
      }
    })

  }

  usernameValidate() {
    const iinCorrect = iinCheck(this.state.username)

    const { usernameErrors } = this.state

    let newUsernameErrors = []

    if(iinCorrect) {
      newUsernameErrors = usernameErrors.filter(item => item != 'Неверный ИИН/БИН')
    }
    else if (usernameErrors.indexOf('Неверный ИИН/БИН') === -1) {
      newUsernameErrors = [...usernameErrors, 'Неверный ИИН/БИН']
    }
    else {
      newUsernameErrors = usernameErrors
    }

    this.setState({ usernameErrors: newUsernameErrors })
  }

  onLangChangeHandler(e) {
    let lang = e.target.id

    defaultState.lang = lang
    this.setState(defaultState)
  }

  render() {
    const { branches } = this.props

    this.branchOptions = branches.map(branch => {
      return {
        value: branch._id,
        label: branch.name.ru
      }
    })

    return (
      <Signup context={ this }/>
    )
  }
}

export default createContainer((props) => {
  Meteor.subscribe('branches')

  return {
    ...props,
    branches: BranchesCollection.find({
      _id: { $ne: '0' }
    }).fetch()
  }
}, SignupContainer)
