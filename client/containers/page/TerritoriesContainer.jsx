import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Territories from '/client/components/page/Territories'
import { TerritoriesCollection } from '/api/territories'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cmNameRus: '',
  cmNameKaz: '',
  cmCode: '',
  cmId: '',
  cmParent: '',
  cmAreaType: '',

  emNameRus: '',
  emNameKaz: '',
  emCode: '',
  emId: '',
  emParent: '',
  emAreaType: '',
  em_id: '',

  page: 1,
  pageSize: 10,
  searchText: ''
}

class TerritoriesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)

    this.getPage()
  }

  deleteTerritory(_id) {
    Meteor.call('territory.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].territoryDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertTerritory() {
    const data = {
      NameRus: this.state.cmNameRus,
      NameKaz: this.state.cmNameKaz,
      Code: parseInt(his.state.cmCode),
      Id: parseInt(this.state.cmId),
      Parent: parseInt(this.state.cmParent),
      AreaType: parseInt(this.state.cmAreaType)
    }

    Meteor.call('territory.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].territoryCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updateTerritory() {
    const data = {
      NameRus: this.state.emNameRus,
      NameKaz: this.state.emNameKaz,
      Code: parseInt(this.state.emCode),
      Id: parseInt(this.state.emId),
      Parent: parseInt(this.state.emParent),
      AreaType: parseInt(this.state.emAreaType)
    }

    Meteor.call('territory.update', this.state.em_id, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].territoryEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.getPage()
        this.setState(defaultState)
      }
    });
  }

  updateKato() {
    Meteor.call('kato.update', (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.getPage()
      }
    })
  }

  getPage(e) {
    e && e.preventDefault()
    Meteor.call('get.territories.by.page', this.state.page, this.state.pageSize, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.setState({ territories: res })
      }
    })
  }

  searchTerritory(e) {
    e.preventDefault()

    Meteor.call('get.territories.by.search', this.state.searchText, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.setState({ territories: res })
      }
    })
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Territories context={ this } locStrings={ locStrings[lang] }/>
    )
  }
}

export default createContainer((props) => {
  return {
    ...props
  }
}, TerritoriesContainer)
