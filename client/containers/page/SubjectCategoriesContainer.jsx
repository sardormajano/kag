import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import SubjectCategories from '/client/components/page/SubjectCategories'
import { SubjectCategoriesCollection } from '/api/subjectCategories'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cmNameRu: '',
  cmNameKz: '',

  emNameKz: '',
  emNameRu: '',
  emSubjectCategoryId: '',
}

class SubjectCategoriesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteSubjectCategory(_id) {
    Meteor.call('subjectCategory.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].subjectCategoryDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertSubjectCategory() {
    const data = {
      name: {
        ru: this.state.cmNameRu,
        kz: this.state.cmNameKz
      }
    }

    Meteor.call('subjectCategory.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].subjectCategoryCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  updateSubjectCategory() {
    const data = {
      name: {
        ru: this.state.emNameRu,
        kz: this.state.emNameKz
      }
    }

    Meteor.call('subjectCategory.update', this.state.emSubjectCategoryId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].subjectCategoryEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <SubjectCategories context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['subjectCategories'])

  return {
    subjectCategories: SubjectCategoriesCollection.find().fetch(),
    ...props,
  }
}, SubjectCategoriesContainer)
