import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Login from '/client/components/page/Login'
import { iinCheck } from '/client/lib/formValidationRelated'
import { cookie } from '/client/lib/coreLib'

const defaultState = {
  login: '',
  password: '',

  loginErrors: [],
}

class LoginContainer extends Component {
  constructor(props) {
    super(props)

    let lang = cookie.get('lang')
    if(lang === '')
    {
      cookie.set('lang', 'ru', 9999);
      lang = 'ru';
    }
    defaultState.lang = lang
    this.state = defaultState
  }

  formSubmitHandler(e) {
    e.preventDefault()

    const { login, password } = this.state

    Meteor.loginWithPassword(
        login, password
    , (err) => {
        if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          })
        }
        else {
          Bert.alert({
              message: this.props.locStrings[this.state.lang].hello,
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          })
        }
    });
  }

  loginValidate() {
    const iinCorrect = iinCheck(this.state.login)

    this.setState({ loginErrors: iinCorrect ? [] : [this.props.locStrings[this.state.lang].validErr] })
  }

  componentDidMount() {
  }

  onLangChangeHandler(e) {
    let lang = e.target.id
    
    defaultState.lang = lang
    this.setState(defaultState)
  }

  render() {
    return (
      <Login context={ this }/>
    )
  }
}

export default createContainer(() => {
  return {}
}, LoginContainer)
