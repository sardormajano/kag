import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Steps from '/client/components/page/Steps'
import { StepsCollection } from '/api/steps'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
}

class StepsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteStep(_id) {
    Meteor.call('step.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].stepDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    });
  }

  levelStepUp(_id) {
    Meteor.call('level.step.up', _id, err => {
      if(err) {
        console.log(err.reason)
      }
    })
  }

  levelStepDown(_id) {
    Meteor.call('level.step.down', _id, err => {
      if(err) {
        console.log(err.reason)
      }
    })
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Steps context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['steps.names'])

  return {
    steps: StepsCollection.find({}, {
      sort: {
        order: 1
      }
    }).fetch(),
    ...props
  }
}, StepsContainer)
