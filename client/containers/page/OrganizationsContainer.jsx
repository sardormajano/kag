import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Organizations from '/client/components/page/Organizations'
import { BranchesCollection } from '/api/branches'
import { CustomRolesCollection } from '/api/customRoles'
import { DepartmentsCollection } from '/api/departments'
import { UsersCollection as OrganizationsCollection } from '/api/users'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import { iinCheck } from '/client/lib/formValidationRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  comNameKz: '',
  comNameRu: '',
  comShortName: '',
  comRepresentative: '',
  comEmail: '',
  comPassword: '',
  comMobilePhone: '',
  comPhone: '',
  comBin: '',
  comAddress: '',
  comBranch: '',

  eomNameKz: '',
  eomNameRu: '',
  eomShortName: '',
  eomRepresentative: '',
  eomEmail: '',
  eomPassword: '',
  eomMobilePhone: '',
  eomPhone: '',
  eomBin: '',
  eomAddress: '',
  eomBranch: '',

  eomOrganizationId: '',

  filterBranches: [],
  filterBin: '',
  filterText: '',
}

class OrganizationsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
  }

  deleteOrganization(_id) {
    Meteor.call('organization.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].organizationDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-briefcase'
        })

        this.setState(defaultState)
      }
    });
  }

  insertOrganization() {
    const data = {
      name: {
        kz: this.state.comNameKz,
        ru: this.state.comNameRu,
      },
      shortName: this.state.comShortName,
      representative: this.state.comRepresentative,
      email: this.state.comEmail,
      password: this.state.comPassword,
      mobilePhone: this.state.comMobilePhone,
      phone: this.state.comPhone,
      bin: this.state.comBin,
      address: this.state.comAddress,
      branch: this.state.comBranch,
    }

    Meteor.call('organization.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].organizationCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-briefcase'
        })

        this.setState(defaultState)
      }
    })
  }

  updateOrganization() {
    const data = {
      name: {
        kz: this.state.eomNameKz,
        ru: this.state.eomNameRu,
      },
      shortName: this.state.eomShortName,
      representative: this.state.eomRepresentative,
      email: this.state.eomEmail,
      password: this.state.eomPassword,
      mobilePhone: this.state.eomMobilePhone,
      phone: this.state.eomPhone,
      bin: this.state.eomBin,
      address: this.state.eomAddress,
      branch: this.state.eomBranch,
    }

    Meteor.call('organization.update', this.state.eomOrganizationId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].organizationEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-briefcase'
        })

        this.setState(defaultState)
      }
    });
  }

  comBinValidate() {
    const iinCorrect = iinCheck(this.state.comBin)

    this.setState({ comBinErrors: iinCorrect ? [] : ['Неверный ИИН/БИН'] })
  }

  eomBinValidate() {
    const iinCorrect = iinCheck(this.state.eomBin)

    this.setState({ eomBinErrors: iinCorrect ? [] : ['Неверный ИИН/БИН'] })
  }

  comIinValidate() {
    const iinCorrect = iinCheck(this.state.comIin)

    const { comIinErrors } = this.state

    let newComIinErrors = []

    if(iinCorrect) {
      newComIinErrors = comIinErrors.filter(item => item != 'Неверный ИИН/БИН')
    }
    else if (comIinErrors.indexOf('Неверный ИИН/БИН') === -1) {
      newComIinErrors = [...comIinErrors, 'Неверный ИИН/БИН']
    }
    else {
      newComIinErrors = comIinErrors
    }

    this.setState({ comIinErrors: newComIinErrors })
  }

  eomIinValidate() {
    const iinCorrect = iinCheck(this.state.eomIin)

    const { eomIinErrors } = this.state

    let newEomIinErrors = []

    if(iinCorrect) {
      newEomIinErrors = eomIinErrors.filter(item => item != 'Неверный ИИН/БИН')
    }
    else if (eomIinErrors.indexOf('Неверный ИИН/БИН') === -1) {
      newEomIinErrors = [...eomIinErrors, 'Неверный ИИН/БИН']
    }
    else {
      newEomIinErrors = eomIinErrors
    }

    this.setState({ eomIinErrors: newEomIinErrors })
  }

  render() {
    const {
      branches,
      departments,
      roles,
      organizations,
      locStrings,
      lang
    } = this.props

    this.branchOptions = branches.map(branch => {
      return {
        value: branch._id,
        label: branch.name.ru
      }
    })
    this.departmentOptions = departments.map(dep => {
      return {
        value: dep._id,
        label: dep.name.ru
      }
    })

    this.roleOptions = roles.map(role => {
      return {
        value: role._id,
        label: role.name
      }
    })
    this.branch = branches.map(branch => {
      return {
        value: branch._id,
        label: branch.name.ru
      }
    })

    return (
      <Organizations context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe([
    'branches.names',
    'customRoles.names',
    'departments.names',
    'organizations',
  ])

  return {
    branches: BranchesCollection.find().fetch(),
    departments: DepartmentsCollection.find().fetch(),
    roles: CustomRolesCollection.find().fetch(),
    organizations: OrganizationsCollection.find({
      _id: {
        $ne: props.userId
      }
    }).fetch(),
    ...props,
  }
}, OrganizationsContainer)
