import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import NewRequest from '/client/components/page/requests/NewRequest'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import { ActivitiesCollection } from '/api/activities'
import { BanksCollection } from '/api/banks'
import { CurrenciesCollection } from '/api/currencies'
import { CustomRolesCollection } from '/api/customRoles'
import { DocumentTypesCollection } from '/api/documentTypes'
import { EnterpreneurTypesCollection } from '/api/enterpreneurTypes'
import { EnterpreneurCategoriesCollection } from '/api/enterpreneurCategories'
import { EducationsCollection } from '/api/educations'
import { LoanProgramsCollection } from '/api/loanPrograms'
import { PaymentOrdersCollection } from '/api/paymentOrders'
import { ProductTypesCollection } from '/api/productTypes'
import { StepsCollection } from '/api/steps'
import { SubjectCategoriesCollection } from '/api/subjectCategories'
import { TerritoriesCollection } from '/api/territories'
import { UsersCollection } from '/api/users'
import { UsersCollection as OrganizationsCollection } from '/api/users'

import { iinCheck } from '/client/lib/formValidationRelated'
import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'
import {
  borrowerInformation,
  borrowerInformationIndividualPerson,
  borrowerInformationIndividualPersonForm,
  borrowerInformationLegalEntity,
  loanInformation,
  lecm, leem, ipcm, ipem, pcm, pem, spcm, spem,
  errorInformation
 } from '/client/containers/page/requests/StateRelated/NewRequestStates'

const requiredFields = [
  'liCreditOrganization',
  'liProductType',
  'liLoanProgram',
  'liPaymentOrderPercent',
  'liPaymentOrderLoan',
  'liMicrocreditSum',
  'liInterestRate',
  'liTerm',
  'liGuaranteeAmount',
  'liGracePeriodPercent',
  'liGracePeriodLoan',
  'liOwnfunds',
]

const defaultState = {
  activeStep: 0,
  status: 0,
  terrParentId: ['',],
  arMessage: '',
  arAgreeing: '',
  arApproving: '',
  steps: {},

  sendable: false,

  // BorrowerInformation
  ...borrowerInformation,

  //BorrowerInformation IndividualPerson
  ...borrowerInformationIndividualPerson,

  // BorrowerInformation IndividualPersonForm
  ...borrowerInformationIndividualPersonForm,

  //BorrowerInformation LegalEntity
  ...borrowerInformationLegalEntity,

  // LoanInformation
  ...loanInformation,

  //LECM for legal entity create modal
  ...lecm,

  //LEEM for legal entity edit modal
  ...leem,

  //IPCM for individual person create modal
  ...ipcm,

  //IPEM for individual person edit modal
  ...ipem,

  //PCM for pledge create modal
  ...pcm,

  //PEM for pledge edit modal
  ...pem,

  //SPCM for special purpose create modal
  ...spcm,

  //SPEM for special purpose edit modal
  ...spem,

  // Errors & Warnings
  ...errorInformation
}

class NewRequestContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  requestStateLoad(res){
    this.setState({
      _id: res._id,
      author: res.author,
      incomplete: res.incomplete,
      requestNumber: res.requestNumber,
      status: res.status,
      steps: res.documents,
    })
    this.setState(res.loanInformation)
    this.setState(res.borrowerInformation.borrowerInformationLegalEntity)
    this.setState(res.borrowerInformation.borrowerInformationLegalEntity.partnerInformationLegalEntity)
    this.setState(res.borrowerInformation.borrowerInformationIndividualPerson)
    this.setState(res.borrowerInformation.borrowerInformationIndividualPerson.partnerInformationIndividualPerson)

    const {liGracePeriodLoan, liCurrency, liGracePeriodPercent} = res.loanInformation

    if(!liGracePeriodLoan || !liCurrency || !liGracePeriodPercent) {
      let gracePeriodLoan = !liGracePeriodLoan ? '0' : liGracePeriodLoan
      let gracePeriodPercent = !liGracePeriodPercent ? '0' : liGracePeriodPercent
      let currency = !currency ? 'BZxzz4d33tWteCHwS' : liCurrency

      this.setState({
        liGracePeriodLoan: gracePeriodLoan,
        liCurrency: currency,
        liGracePeriodPercent: gracePeriodPercent
      })
    }
  }

  updateTheRequest() {
    Meteor.call('request.get', this.props.match.params._id, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
        this.requestStateLoad(res)
      }
    })
  }

  initialContainerLoad() {
    if(this.props.userIsOrganization) {
      this.setState({ liCreditOrganization: this.props.userId})
    }

    Meteor.call('request.get.incomplete', (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
        if(res._id != this.props.match.params._id){
          this.updateTheRequest()
          return
        }
        this.requestStateLoad(res)
      } else if(!res) {
        this.updateTheRequest()
        this.setState({
          liGracePeriodLoan: '0',
          liCurrency: 'BZxzz4d33tWteCHwS',
          liGracePeriodPercent: '0'
        })
      }
    })
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)

    this.initialContainerLoad()
  }

  //BorrowerInformation IndividualPersonFormMethods
  biipfIinValidate() {
    const iinCorrect = iinCheck(this.state.biipfIin)

    this.setState({ biipfIinErrors: iinCorrect ? [] : [this.props.locStrings[this.props.lang].validErr] })
  }

  biipfSpouseIinValidate() {
    const iinCorrect = iinCheck(this.state.biipfSpouseIin)

    this.setState({ biipfSpouseIinErrors: iinCorrect ? [] : [this.props.locStrings[this.props.lang].validErr] })
  }

  biipfIinBlurHandler() {
    const iin = this.state.biipfIin
    Meteor.call('individual.person.get', iin, (err, res) => {
      if(err) {
        console.log(err)
      }
      else if(res){
        this.setState({
          biipfFio: res.fullName,
          biipfDob: res.birthDate,
          biipfMobilePhone: res.phone
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].ipInfoAutoFill,
          type: 'warning',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })
  }

  saveChanges() {
    const data = {
      borrowerInformation: {
        borrowerInformationLegalEntity: {
          biActivities: this.state.biActivities,
          biActualAddress: this.state.biActualAddress,
          biActualAddressExtra: this.state.biActualAddressExtra,
          biBin: this.state.biBin,
          biBorrowerMarried: this.state.biBorrowerMarried,
          partnerInformationLegalEntity: {
            biBorrowerPartnerBirthdate: this.state.biBorrowerPartnerBirthdate,
            biBorrowerPartnerEducation: this.state.biBorrowerPartnerEducation,
            biBorrowerPartnerFullName: this.state.biBorrowerPartnerFullName,
            biBorrowerPartnerIin: this.state.biBorrowerPartnerIin,
            biBorrowerPartnerIsEmployed: this.state.biBorrowerPartnerIsEmployed,
          },
          biCharterCapital: this.state.biCharterCapital,
          biDebtSum: this.state.biDebtSum,
          biDebtsInfo: this.state.biDebtsInfo,
          biEmployeesNumber: this.state.biEmployeesNumber,
          biEnterpreneurCategory: this.state.biEnterpreneurCategory,
          biEnterpreneurType: this.state.biEnterpreneurType,
          biFounders: this.state.biFounders,
          biHasDebts: this.state.biHasDebts,
          biHeadBirthdate: this.state.biHeadBirthdate,
          biHeadEducation: this.state.biHeadEducation,
          biHeadFullName: this.state.biHeadFullName,
          biHeadIin: this.state.biHeadIin,
          biImportantInformation: this.state.biImportantInformation,
          biLegalAddress: this.state.biLegalAddress,
          biLegalAddressExtra: this.state.biLegalAddressExtra,
          biOrganizationName: this.state.biOrganizationName,
          biRegistrationDate: this.state.biRegistrationDate,
          biServingBanks: this.state.biServingBanks,
          biSubjectCategory: this.state.biSubjectCategory,
        },
        borrowerInformationIndividualPerson: {
          biipfDob: this.state.biipfDob,
          biipfEducation: this.state.biipfEducation,
          biipfEmail: this.state.biipfEmail,
          biipfFio: this.state.biipfFio,
          biipfIin: this.state.biipfIin,
          biipfIndividualPersonAddress: this.state.biipfIndividualPersonAddress,
          biipfIndividualPersonAddressExtra: this.state.biipfIndividualPersonAddressExtra,
          biipfIndividualPersonCertificate: this.state.biipfIndividualPersonCertificate,
          biipfMarried: this.state.biipfMarried,
          biipfMobilePhone: this.state.biipfMobilePhone,
          partnerInformationIndividualPerson: {
            biipfSpouseDob: this.state.biipfSpouseDob,
            biipfSpouseEducation: this.state.biipfSpouseEducation,
            biipfSpouseFio: this.state.biipfSpouseFio,
            biipfSpouseIin: this.state.biipfSpouseIin,
            biipfspouseEmployment: this.state.biipfspouseEmployment,
          }
        }
      },
      loanInformation: {
        liCreditOrganization: this.state.liCreditOrganization,
        liCurrency: this.state.liCurrency,
        liDate: this.state.liDate,
        liGracePeriodLoan: this.state.liGracePeriodLoan,
        liGracePeriodPercent: this.state.liGracePeriodPercent,
        liGuaranteeAmount: this.state.liGuaranteeAmount,
        liInterestRate: this.state.liInterestRate,
        liLendingPurpose: this.state.liLendingPurpose,
        liLoanAgreementDate: this.state.liLoanAgreementDate,
        liLoanAgreementNumber: this.state.liLoanAgreementNumber,
        liLoanProgram: this.state.liLoanProgram,
        liMicrocreditSum: this.state.liMicrocreditSum,
        liOwnfunds: this.state.liOwnfunds,
        liPaymentOrderLoan: this.state.liPaymentOrderLoan,
        liPaymentOrderPercent: this.state.liPaymentOrderPercent,
        liPledges: this.state.liPledges,
        liProductType: this.state.liProductType,
        liProjectName: this.state.liProjectName,
        liProjectPlace: this.state.liProjectPlace,
        liProjectPurpose: this.state.liProjectPurpose,
        liRequestAdmissionDate: this.state.liRequestAdmissionDate,
        liSpecialPurpose: this.state.liSpecialPurpose,
        liTerm: this.state.liTerm,
      },
      incomplete: true,
      documents: this.state.steps,
    }

    Meteor.call('request.upsert', this.state._id, data, this.state.userIp, (err, res) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        res.insertedId && this.setState({ _id: res.insertedId })

        this.props.history.replace(`/requests/newRequest/${this.state._id}`);

        Bert.alert({
            message: this.props.locStrings[this.props.lang].requestCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }
    })
  }

  // goToNextStep(e) {
  //   if(this.state.activeStep === this.props.steps.length)
  //     return
  //
  //   this.setState({ activeStep: this.state.activeStep + 1 })
  // }
  //
  // goToPrevStep(e) {
  //   e.preventDefault()
  //
  //   if(this.state.activeStep === 0)
  //     return
  //
  //   this.setState({ activeStep: this.state.activeStep - 1 })
  // }

  fieldsWarningState() {
    return requiredFields.filter(field => !this.state[field] )
  }

  liFieldsFilledOut() {
    return !requiredFields.some(field => {
      if(!this.state[field])
        return true
      if(typeof this.state[field] === 'number')
        return false
      else {
        return this.state[field] === '' || !this.state[field].length
      }
    })
  }

  otherFieldsAreFilledOut() {
    const { steps } = this.state

    if(!steps)
      return false

    const { documentTypes } = this.props

    for(let key in steps) {
      for(let innerKey in steps[key]) {
        const theDocumentType = documentTypes.filter(dType => dType._id === innerKey)[0]

        if(!theDocumentType || (theDocumentType.required && !steps[key][innerKey].length))
        {
          return false
        }
      }
    }

    return true
  }

  updateRequest() {
    const data = {
      borrowerInformation: {
        borrowerInformationLegalEntity: {
          biActivities: this.state.biActivities,
          biActualAddress: this.state.biActualAddress,
          biActualAddressExtra: this.state.biActualAddressExtra,
          biBin: this.state.biBin,
          biBorrowerMarried: this.state.biBorrowerMarried,
          partnerInformationLegalEntity: {
            biBorrowerPartnerBirthdate: this.state.biBorrowerPartnerBirthdate,
            biBorrowerPartnerEducation: this.state.biBorrowerPartnerEducation,
            biBorrowerPartnerFullName: this.state.biBorrowerPartnerFullName,
            biBorrowerPartnerIin: this.state.biBorrowerPartnerIin,
            biBorrowerPartnerIsEmployed: this.state.biBorrowerPartnerIsEmployed,
          },
          biCharterCapital: this.state.biCharterCapital,
          biDebtSum: this.state.biDebtSum,
          biDebtsInfo: this.state.biDebtsInfo,
          biEmployeesNumber: this.state.biEmployeesNumber,
          biEnterpreneurCategory: this.state.biEnterpreneurCategory,
          biEnterpreneurType: this.state.biEnterpreneurType,
          biFounders: this.state.biFounders,
          biHasDebts: this.state.biHasDebts,
          biHeadBirthdate: this.state.biHeadBirthdate,
          biHeadEducation: this.state.biHeadEducation,
          biHeadFullName: this.state.biHeadFullName,
          biHeadIin: this.state.biHeadIin,
          biImportantInformation: this.state.biImportantInformation,
          biLegalAddress: this.state.biLegalAddress,
          biLegalAddressExtra: this.state.biLegalAddressExtra,
          biOrganizationName: this.state.biOrganizationName,
          biRegistrationDate: this.state.biRegistrationDate,
          biServingBanks: this.state.biServingBanks,
          biSubjectCategory: this.state.biSubjectCategory,
        },
        borrowerInformationIndividualPerson: {
          biipfDob: this.state.biipfDob,
          biipfEducation: this.state.biipfEducation,
          biipfEmail: this.state.biipfEmail,
          biipfFio: this.state.biipfFio,
          biipfIin: this.state.biipfIin,
          biipfIndividualPersonAddress: this.state.biipfIndividualPersonAddress,
          biipfIndividualPersonAddressExtra: this.state.biipfIndividualPersonAddressExtra,
          biipfIndividualPersonCertificate: this.state.biipfIndividualPersonCertificate,
          biipfMarried: this.state.biipfMarried,
          biipfMobilePhone: this.state.biipfMobilePhone,
          partnerInformationIndividualPerson: {
            biipfSpouseDob: this.state.biipfSpouseDob,
            biipfSpouseEducation: this.state.biipfSpouseEducation,
            biipfSpouseFio: this.state.biipfSpouseFio,
            biipfSpouseIin: this.state.biipfSpouseIin,
            biipfspouseEmployment: this.state.biipfspouseEmployment,
          }
        }
      },
      loanInformation: {
        liCreditOrganization: this.state.liCreditOrganization,
        liCurrency: this.state.liCurrency,
        liDate: this.state.liDate,
        liGracePeriodLoan: this.state.liGracePeriodLoan,
        liGracePeriodPercent: this.state.liGracePeriodPercent,
        liGuaranteeAmount: this.state.liGuaranteeAmount,
        liInterestRate: this.state.liInterestRate,
        liLendingPurpose: this.state.liLendingPurpose,
        liLoanAgreementDate: this.state.liLoanAgreementDate,
        liLoanAgreementNumber: this.state.liLoanAgreementNumber,
        liLoanProgram: this.state.liLoanProgram,
        liMicrocreditSum: this.state.liMicrocreditSum,
        liOwnfunds: this.state.liOwnfunds,
        liPaymentOrderLoan: this.state.liPaymentOrderLoan,
        liPaymentOrderPercent: this.state.liPaymentOrderPercent,
        liPledges: this.state.liPledges,
        liProductType: this.state.liProductType,
        liProjectName: this.state.liProjectName,
        liProjectPlace: this.state.liProjectPlace,
        liProjectPurpose: this.state.liProjectPurpose,
        liRequestAdmissionDate: this.state.liRequestAdmissionDate,
        liSpecialPurpose: this.state.liSpecialPurpose,
        liTerm: this.state.liTerm,
      },
      incomplete: true,
      documents: this.state.steps
    }

    Meteor.call('request.update', this.state._id, data, this.state.userIp, err => err && console.log(err.reason))
  }

  completeRequest() {
    Meteor.call('request.complete', this.state._id, err => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: "Заявка успешно отправлена на обработку!",
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.props.history.push('/requests')
      }
    })
  }

  //SPECIAL PURPOSE BEGIN
  addSpecialPurpose() {
    const { liSpecialPurpose } = this.state

    this.setState({
      liSpecialPurpose: [...liSpecialPurpose, {
        name: this.state.spcmName,
        price: this.state.spcmPrice,
        unit: this.state.spcmUnit,
        number: this.state.spcmNumber
      }],
      spcmName: '',
      spcmPrice: '',
      spcmUnit: '',
      spcmNumber: '',
    })
  }

  deleteSpecialPurpose(obj) {
    const { liSpecialPurpose } = this.state
    const newSpecialPurpose = liSpecialPurpose.filter(item => item !== obj)

    this.setState({
      liSpecialPurpose: newSpecialPurpose
    })
  }

  updateSpecialPurpose() {
    const { liSpecialPurpose } = this.state

    const thePurpose = liSpecialPurpose[this.state.spemIndex]

    thePurpose.name = this.state.spemName
    thePurpose.unit = this.state.spemUnit
    thePurpose.price = this.state.spemPrice
    thePurpose.number = this.state.spemNumber

    this.setState({
      spemName: '',
      spemPrice: '',
      spemUnit: '',
      spemNumber: '',
      spemObj: {},
    })
  }
  //SPECIAL PURPOSE END

  //ACTIVITY BEGIN
  activitySelectHandler(index, value) {
    const { activities } = this.props
    const { biModalActivities } = this.state
    biModalActivities[index] = value
    biModalActivities[index+1] = ''

    const theActivity = activities.find(t => t._id === value)

    Meteor.subscribe('activities.by.parent', theActivity.Id)

    this.setState({
      biModalActivities
    })
  }

  addActivityHandler() {
    const selectedActivity = this.state.biModalActivities[this.state.biModalActivities.length - 2]

    Meteor.call('get.activity', selectedActivity, (err, res) => {
      if(err) {
        console.log(err)
      } else {
        this.setState({
          biModalActivities: ['',],
          biActivities: [
            ...this.state.biActivities,
            {
              cityId: res._id,
              nameKz: res.NameKaz,
              nameRu: res.NameRus
            }
          ]
        })
      }
    })
  }

  deleteActivityHandler(itemToDelete) {
    const newBiActivities = this.state.biActivities.filter(item => item !== itemToDelete)

    this.setState({ biActivities: newBiActivities })
  }
  //ACTIVITY END

  //PROJECT PLACE BEGIN
  placeSelectHandler(index, value) {
    const { territories } = this.props
    const { liProjectPlaces, terrParentId } = this.state
    liProjectPlaces[index] = value
    liProjectPlaces[index+1] = ''

    const theTerritory = territories.find(t => t._id === value)

    if(theTerritory.Id === 17108 || theTerritory.Id === 17112) {
      this.setState({ liProjectPlaceSavable: true })
      return
    }

    terrParentId[index+1] = theTerritory.Id

    Meteor.subscribe('territories.by.parent', theTerritory.Id)
    Meteor.call('territory.has.children', theTerritory.Id, (err, res) => {
      if(!res) {
        this.setState({ liProjectPlaceSavable: true })
      }
    })

    this.setState({
      terrParentId,
      liProjectPlaces
    })
  }

  placeSaveHandler(e) {
    const { territories } = this.props
    const { liProjectPlaces } = this.state
    const theProjectPlace = liProjectPlaces[liProjectPlaces.length - 2]

    Meteor.call('territory.childrenPlace', theProjectPlace, (err, res) => {
      if(err) {
        console.log(err)
      } else {
        this.setState({
          terrParentId: ['',],
          liProjectPlace: {
            cityId: res._id,
            nameKz: res.NameKaz,
            nameRu: res.NameRus
          }
        })
      }
    })
  }
  //PROJECT PLACE END

  //Individual Person ADDRESS BEGIN
  individualPersonAddressSelectHandler(index, value) {
    const { territories } = this.props
    const { biipfIndividualPersonAddresses, terrParentId } = this.state
    biipfIndividualPersonAddresses[index] = value
    biipfIndividualPersonAddresses[index+1] = ''

    const theTerritory = territories.find(t => t._id === value)

    if(theTerritory.Id === 17108 || theTerritory.Id === 17112) {
      this.setState({ biipfIndividualPersonSavable: true })
      return
    }

    terrParentId[index+1] = theTerritory.Id

    Meteor.subscribe('territories.by.parent', theTerritory.Id)
    Meteor.call('territory.has.children', theTerritory.Id, (err, res) => {
      if(!res) {
        this.setState({ biipfIndividualPersonSavable: true })
      }
    })

    this.setState({
      terrParentId,
      biipfIndividualPersonAddresses
    })
  }

  individualPersonAddressSaveHandler() {
    const { territories } = this.props
    const { biipfIndividualPersonAddresses } = this.state
    const biipfIndividualPersonAddress = biipfIndividualPersonAddresses[biipfIndividualPersonAddresses.length - 2]

    Meteor.call('territory.childrenPlace', biipfIndividualPersonAddress, (err, res) => {
      if(err) {
        console.log(err)
      } else {
        this.setState({
          terrParentId: ['',],
          biipfIndividualPersonAddress: {
            cityId: res._id,
            nameKz: res.NameKaz,
            nameRu: res.NameRus
          }
        })
      }
    })
  }

  //LEGAL ADDRESS BEGIN
  legalAddressSelectHandler(index, value) {
    const { territories } = this.props
    const { biLegalAddresses, terrParentId } = this.state
    biLegalAddresses[index] = value
    biLegalAddresses[index+1] = ''

    const theTerritory = territories.find(t => t._id === value)

    if(theTerritory.Id === 17108 || theTerritory.Id === 17112) {
      this.setState({ biLegalAddressSavable: true })
      return
    }

    terrParentId[index+1] = theTerritory.Id

    Meteor.subscribe('territories.by.parent', theTerritory.Id)
    Meteor.call('territory.has.children', theTerritory.Id, (err, res) => {
      if(!res) {
        this.setState({ biLegalAddressSavable: true })
      }
    })

    this.setState({
      terrParentId,
      biLegalAddresses
    })
  }

  legalAddressSaveHandler() {
    const { territories } = this.props
    const { biLegalAddresses } = this.state
    const biLegalAddress = biLegalAddresses[biLegalAddresses.length - 2]

    Meteor.call('territory.childrenPlace', biLegalAddress, (err, res) => {
      if(err) {
        console.log(err)
      } else {
        this.setState({
          terrParentId: ['',],
          biLegalAddress: {
            cityId: res._id,
            nameKz: res.NameKaz,
            nameRu: res.NameRus
          }
        })
      }
    })
  }
  //LEGAL ADDRESS END

  //ACTUAL ADDRESS BEGIN
  actualAddressSelectHandler(index, value) {
    const { territories } = this.props
    const { biActualAddresses, terrParentId } = this.state
    biActualAddresses[index] = value
    biActualAddresses[index+1] = ''

    const theTerritory = territories.find(t => t._id === value)

    if(theTerritory.Id === 17108 || theTerritory.Id === 17112) {
      this.setState({ biActualAddressSavable: true })
      return
    }

    terrParentId[index+1] = theTerritory.Id

    Meteor.subscribe('territories.by.parent', theTerritory.Id)
    Meteor.call('territory.has.children', theTerritory.Id, (err, res) => {
      if(!res) {
        this.setState({ biActualAddressSavable: true })
      }
    })

    this.setState({
      terrParentId,
      biActualAddresses
    })
  }

  actualAddressSaveHandler() {
    const { territories } = this.props
    const { biActualAddresses } = this.state
    const biActualAddress = biActualAddresses[biActualAddresses.length - 2]

    Meteor.call('territory.childrenPlace', biActualAddress, (err, res) => {
      if(err) {
        console.log(err)
      } else {
        this.setState({
          terrParentId: ['',],
          biActualAddress: {
            cityId: res._id,
            nameKz: res.NameKaz,
            nameRu: res.NameRus
          }
        })
      }
    })
  }
  //ACTUAL ADDRESS END

  //LEGAL ENTITY FOUNDER ADDRESS BEGIN
  bileFounderAddressSelectHandler(index, value) {
    const { territories } = this.props
    const { bileFounderAddresses, terrParentId } = this.state
    bileFounderAddresses[index] = value
    bileFounderAddresses[index+1] = ''

    const theTerritory = territories.find(t => t._id === value)

    if(theTerritory.Id === 17108 || theTerritory.Id === 17112) {
      this.setState({ bileFounderAddressSavable: true })
      return
    }

    terrParentId[index+1] = theTerritory.Id

    Meteor.subscribe('territories.by.parent', theTerritory.Id)
    Meteor.call('territory.has.children', theTerritory.Id, (err, res) => {
      if(!res) {
        this.setState({ bileFounderAddressSavable: true })
      }
    })

    this.setState({
      terrParentId,
      bileFounderAddresses
    })
  }

  bileFounderSaveHandler() {
    const { territories } = this.props
    const { bileFounderAddresses } = this.state
    const bileFounderAddress = bileFounderAddresses[bileFounderAddresses.length - 2]

    Meteor.call('territory.childrenPlace', bileFounderAddress, (err, res) => {
      if (err) {
        console.log(err)
      } else {
        this.setState({
          terrParentId: ['',],
          bileFounderAddress: {
            cityId: res._id,
            nameKz: res.NameKaz,
            nameRu: res.NameRus
          }
        })
        this.createFounderLegalEntity()
      }
    })
  }
  //LEGAL ENTITY FOUNDER ADDRESS END

  //INDIVIDUAL PERSON FOUNDER ADDRESS BEGIN
  biipFounderAddressSelectHandler(index, value) {
    const { territories } = this.props
    const { biipFounderAddresses, terrParentId } = this.state
    biipFounderAddresses[index] = value
    biipFounderAddresses[index+1] = ''

    const theTerritory = territories.find(t => t._id === value)

    if(theTerritory.Id === 17108 || theTerritory.Id === 17112) {
      this.setState({ biipFounderAddressSavable: true })
      return
    }

    terrParentId[index+1] = theTerritory.Id

    Meteor.subscribe('territories.by.parent', theTerritory.Id)
    Meteor.call('territory.has.children', theTerritory.Id, (err, res) => {
      if(!res) {
        this.setState({ biipFounderAddressSavable: true })
      }
    })

    this.setState({
      terrParentId,
      biipFounderAddresses
    })
  }

  biipFounderSaveHandler() {
    const { territories } = this.props
    const { biipFounderAddresses } = this.state
    const biipFounderAddress = biipFounderAddresses[biipFounderAddresses.length - 2]

    Meteor.call('territory.childrenPlace', biipFounderAddress, (err, res) => {
      if (err) {
        console.log(err)
      } else {
        this.setState({
          terrParentId: ['',],
          biipFounderAddress: {
            cityId: res._id,
            nameKz: res.NameKaz,
            nameRu: res.NameRus
          }
        })
        this.createFounderIndividualPerson()
      }
    })
  }
  //INDIVIDUAL PERSON FOUNDER ADDRESS END

  //PLEDGE BEGIN
  addPledge() {
    const { liPledges} = this.state

    this.setState({
      liPledges: [...liPledges, {
        subject: this.state.pcmSubject,
        place: this.state.pcmPlace,
        pledgeCost: this.state.pcmPledgeCost,
        estimatedCost: this.state.pcmEstimatedCost,
        estimationDate: this.state.pcmEstimationDate,
        kl: this.state.pcmKl,
        coverageShare: this.state.pcmCoverageShare,
        relation: this.state.pcmRelation,
        otherInfo: this.state.pcmOtherInfo,
      }],
      pcmSubject: '',
      pcmPlace: '',
      pcmPledgeCost: '',
      pcmEstimatedCost: '',
      pcmEstimationDate: '',
      pcmKl: '',
      pcmCoverageShare: '',
      pcmRelation: '',
      pcmOtherInfo: '',
    })
  }
  deletePledge(obj) {
    const { liPledges } = this.state
    const newPledges = liPledges.filter(item => item !== obj)

    this.setState({
      liPledges: newPledges
    })
  }
  updatePledge() {
    const { liPledges } = this.state

    const thePledge = liPledges[this.state.pemIndex]

    thePledge.subject = this.state.pemSubject
    thePledge.place = this.state.pemPlace
    thePledge.pledgeCost = this.state.pemPledgeCost
    thePledge.estimatedCost = this.state.pemEstimatedCost
    thePledge.estimationDate = this.state.pemEstimationDate
    thePledge.kl = this.state.pemKl
    thePledge.coverageShare = this.state.pemCoverageShare
    thePledge.relation = this.state.pemRelation
    thePledge.otherInfo = this.state.pemOtherInfo

    this.setState({
      pemSubject: '',
      pemPlace: '',
      pemPledgeCost: '',
      pemEstimatedCost: '',
      pemEstimationDate: '',
      pemKl: '',
      pemCoverageShare: '',
      pemRelation: '',
      pemOtherInfo: '',
      pemIndex: '',
    })
  }
  //PLEDGE END

  //PLEDGE LEGAL ENTITY BEGIN
  createLegalEntity() {
    const data = {
      bin: this.state.lecmBin,
      shortName: this.state.lecmShortName,
      enterpreneurType: this.state.lecmEnterpreneurType,
      subjectCategory: this.state.lecmSubjectCategory,
      registrationDate: this.state.lecmRegistrationDate,
      phone: this.state.lecmPhone,
    }

    Meteor.call('legalEntity.insert', data, this.state.userIp, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].reqLegEntAddSuccess,
          type: 'success',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })

    this.state.liActivePledge.pledgeOwner = {
      bin: data.bin,
      name: data.shortName,
      type: 'le'
    }

    this.setState({
      lecmBin: '',
      lecmShortName: '',
      lecmEnterpreneurType: '',
      lecmSubjectCategory: '',
      lecmRegistrationDate: '',
      lecmPhone: '',
      liActivePledge: {}
    })
  }
  editLegalEntity() {
    const data = {
      bin: this.state.leemBin,
      shortName: this.state.leemShortName,
      enterpreneurType: this.state.leemEnterpreneurType,
      subjectCategory: this.state.leemSubjectCategory,
      registrationDate: this.state.leemRegistrationDate,
      phone: this.state.leemPhone,
    }

    Meteor.call('legalEntity.update', this.state.leemId, data, this.state.userIp, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].reqLegEntEditSuccess,
          type: 'success',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })

    this.state.liActivePledge.pledgeOwner = {
      bin: data.bin,
      name: data.shortName,
      type: 'le'
    }

    this.setState({
      leemId: '',
      lecmBin: '',
      lecmShortName: '',
      lecmEnterpreneurType: '',
      lecmSubjectCategory: '',
      lecmRegistrationDate: '',
      lecmPhone: '',
      liActivePledge: {}
    })
  }
  nlemBinBlurHandler() {
    const bin = this.state.lecmBin
    Meteor.call('legalEntity.get', bin, (err, res) => {
      if(err) {
        console.log(err)
      }
      else if(res){
        this.setState({
          lecmBin: res.bin,
          lecmShortName: res.shortName,
          lecmEnterpreneurType: res.enterpreneurType,
          lecmSubjectCategory: res.subjectCategory,
          lecmRegistrationDate: res.registrationDate,
          lecmPhone: res.phone
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].reqLegEntNotFound,
          type: 'warning',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })
  }
  elemBinBlurHandler() {
    const bin = this.state.leemBin
    Meteor.call('legalEntity.get', bin, (err, res) => {
      if(err) {
        console.log(err)
      }
      else if(res){
        this.setState({
          leemBin: res.bin,
          leemShortName: res.shortName,
          leemEnterpreneurType: res.enterpreneurType,
          leemSubjectCategory: res.subjectCategory,
          leemRegistrationDate: res.registrationDate,
          leemPhone: res.phone
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].reqLegEntNotFound,
          type: 'warning',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })
  }
  lePreUpdate(item) {
    const { bin } = item.pledgeOwner

    this.setState({ liActivePledge: item })

    Meteor.call('legalEntity.get', bin, (err, res) => {
      if(err) {
        console.log(err)
      }
      else if(res){
        this.setState({
          leemId: res._id,
          leemBin: res.bin,
          leemShortName: res.shortName,
          leemEnterpreneurType: res.enterpreneurType,
          leemSubjectCategory: res.subjectCategory,
          leemRegistrationDate: res.registrationDate,
          leemPhone: res.phone
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].reqLegEntNotFound,
          type: 'warning',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })
  }
  //PLEDGE LEGAL ENTITY END

  //PLEDGE INDIVIDUAL PERSON BEGIN
  createIndividualPerson() {
    const data = {
      iin: this.state.ipcmIin,
      fullName: this.state.ipcmFullName,
      birthDate: this.state.ipcmBirthDate,
      phone: this.state.ipcmPhone,
    }

    Meteor.call('individualPerson.insert', data, this.state.userIp, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].reqIndPersonAddSuccess,
          type: 'success',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })

    this.state.liActivePledge.pledgeOwner = {
      iin: data.iin,
      name: data.fullName,
      type: 'ip'
    }

    this.setState({
      ipcmIin: '',
      ipcmFullName: '',
      ipcmBirthDate: '',
      ipcmPhone: '',
      liActivePledge: {}
    })
  }
  editIndividualPerson() {
    const data = {
      iin: this.state.ipemIin,
      fullName: this.state.ipemFullName,
      birthDate: this.state.ipemBirthDate,
      phone: this.state.ipemPhone,
    }

    Meteor.call('individualPerson.update', this.state.ipemId, data, this.state.userIp, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].reqIndPersonEditSuccess,
          type: 'success',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })

    this.state.liActivePledge.pledgeOwner = {
      iin: data.iin,
      name: data.fullName,
      type: 'ip'
    }

    this.setState({
      ipemId: '',
      ipemIin: '',
      ipemFullName: '',
      ipemBirthDate: '',
      ipemPhone: '',
      liActivePledge: {}
    })
  }
  nipmIinBlurHandler() {
    const iin = this.state.ipcmIin
    Meteor.call('individual.person.get', iin, (err, res) => {
      if(err) {
        console.log(err)
      }
      else if(res){
        this.setState({
          ipcmFullName: res.fullName,
          ipcmBirthDate: res.birthDate,
          ipcmPhone: res.phone
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].ipInfoAutoFill,
          type: 'warning',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })
  }
  eipmIinBlurHandler() {
    const iin = this.state.ipcmIin
    Meteor.call('individual.person.get', iin, (err, res) => {
      if(err) {
        console.log(err)
      }
      else if(res){
        this.setState({
          ipemFullName: res.fullName,
          ipemBirthDate: res.birthDate,
          ipemPhone: res.phone
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].ipInfoAutoFill,
          type: 'warning',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })
  }
  ipPreUpdate(item) {
    const { iin } = item.pledgeOwner

    this.setState({ liActivePledge: item })

    Meteor.call('individual.person.get', iin, (err, res) => {
      if(err) {
        console.log(err)
      }
      else if(res){
        this.setState({
          ipemId: res._id,
          ipemIin: res.iin,
          ipemFullName: res.fullName,
          ipemBirthDate: res.birthDate,
          ipemPhone: res.phone
        })
      }
      else {
        Bert.alert({
          message: this.props.locStrings[this.props.lang].ipInfoAutoFill,
          type: 'warning',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
        })
      }
    })
  }
  //PLEDGE INDIVIDUAL PERSON END

  //FOUNDER LEGAL ENTITY BEGIN
  createFounderLegalEntity() {
    const {
      bileFounderAddress,
      bileFounderAddressExtra,
      bileBin,
      bileShortName,
      bileEnterpreneurType,
      bileSubjectCategory,
      bileRegistrationDate,
      bilePhone,
      bileShare,
    } = this.state

    const founder = {
      type: 'le',
      address: bileFounderAddress,
      addressExtra: bileFounderAddressExtra,
      share: bileShare,
      bin: bileBin,
      shortName: bileShortName,
      enterpreneurType: bileEnterpreneurType,
      subjectCategory: bileSubjectCategory,
      registrationDate: bileRegistrationDate,
      phone: bilePhone
    }

    this.setState({
      biFounders: [...this.state.biFounders, founder],
      bileFounderAddresses: ['',],
      bileFounderAddress: {},
      bileFounderAddressSavable: false,
      bileFounderAddressExtra: '',
      bileBin: '',
      bileShortName: '',
      bileEnterpreneurType: '',
      bileSubjectCategory: '',
      bileRegistrationDate: '',
      bilePhone: '',
      bileShare: '',
    })
  }
  //FOUNDER LEGAL ENTITY END
  //FOUNDER INDIVIDUAL PERSON BEGIN
  createFounderIndividualPerson() {
    const {
      biipIin,
      biipBirthDate,
      biipFullName,
      biipPhone,
      biipShare,
      biipEducation,
      biipFounderAddress,
      biipFounderAddressExtra
    } = this.state

    const founder = {
      type: 'ip',
      address: biipFounderAddress,
      addressExtra: biipFounderAddressExtra,
      share: biipShare,
      iin: biipIin,
      fullName: biipFullName,
      education: biipEducation,
      phone: biipPhone,
      birthDate: biipBirthDate,
    }

    this.setState({
      biFounders: [...this.state.biFounders, founder],
      biipIin: '',
      biipFullName: '',
      biipPhone: '',
      biipBirthDate: '',
      biipShare: '',
      biipEducation: '',
      biipFounderAddresses: ['',],
      biipFounderAddress: {},
      biipFounderAddressSavable: false,
      biipFounderAddressExtra: '',
    })
  }
  //FOUNDER INDIVIDUAL PERSON END

  //FOUNDER BEGIN
  deleteFounder(item) {
    const { biFounders } = this.state
    const newBiFounders = biFounders.filter(founder => founder !== item)

    this.setState({ biFounders: newBiFounders })
  }
  //FOUNDER END

  // SENDAPPLICATIONREVIEW
  writeMessageHandler() {
    Meteor.call('request.write.message', this.state._id, this.state.arMessage, this.state.userIp, (err, res) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        this.setState({ arMessage: '' })
        this.updateTheRequest()

        Bert.alert({
            message: 'Сообщение успешно сохранено!',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }
    })
  }

  sendForApprovalHandler() {
    Meteor.call(
      'request.send.for.approval',
      this.props.match.params._id,
      this.state.arApproving,
      this.state.userIp,
      err => {
        if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          })
        }
        else {
          this.props.history.push('/')
        }
      }
    )
  }

  sendForAgreementHandler() {
    Meteor.call(
      'request.send.for.agreement',
      this.props.match.params._id,
      this.state.arAgreeing,
      this.state.userIp,
      err => {
        if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          })
        }
        else {
          this.props.history.push('/')
        }
      }
    )
  }

  render() {
    const {
      activities,
      banks,
      creditOrganizations,
      currencies,
      enterpreneurTypes,
      enterpreneurCategories,
      educations,
      loanPrograms,
      paymentOrders,
      productTypes,
      subjectCategories,
      territories,
    } = this.props


    this.isApplicationCreatedByKAG = !Meteor.user().profile.isOrganization;

    this.bankOptions = banks.map(bank => {
      return {
        value: bank._id,
        label: bank.name.ru
      }
    })

    this.creditOrganizationOptions = creditOrganizations.map(org => {
      return {
        value: org._id,
        label: org.profile.shortName
      }
    })

    this.currencyOptions = currencies.map(currency => {
      return {
        value: currency._id,
        label: currency.name.ru
      }
    })


    this.paymentOrderOptions = paymentOrders.map(paymentOrder => {
      return {
        value: paymentOrder._id,
        label: paymentOrder.name.ru
      }
    })

    this.productTypeOptions = productTypes.map(productType => {
      return {
        value: productType._id,
        label: productType.name.ru
      }
    })

    this.enterpreneurTypeOptions = enterpreneurTypes.map(enterpreneurType => {
      return {
        value: enterpreneurType._id,
        label: enterpreneurType.name.ru
      }
    })

    this.enterpreneurCategoryOptions = enterpreneurCategories.map(enterpreneurCategory => {
      return {
        value: enterpreneurCategory._id,
        label: enterpreneurCategory.name.ru
      }
    })

    this.educationOptions = educations.map(education => {
      return {
        value: education._id,
        label: education.name.ru
      }
    })

    this.loanProgramOptions = loanPrograms.map(loanProgram => {
      return {
        value: loanProgram._id,
        label: loanProgram.name.ru
      }
    })

    this.subjectCategoryOptions = subjectCategories.map(category => {
      return {
        value: category._id,
        label: category.name.ru
      }
    })

    this.territoryOptions = []
    for(let i = 0, len = this.state.liProjectPlaces.length; i < len; i++) {
      this.territoryOptions[i] = territories
        .filter(territory => territory.Parent === this.state.terrParentId[i])
        .map(territory => {
          return {
            value: territory._id,
            label: territory.NameRus,
            id: territory.Id
          }
        })
    }

    this.legalTerritoriesOptions = []
    for(let i = 0, len = this.state.biLegalAddresses.length; i < len; i++) {
      this.legalTerritoriesOptions[i] = territories
        .filter(territory => territory.Parent === this.state.terrParentId[i])
        .map(territory => {
          return {
            value: territory._id,
            label: territory.NameRus,
            id: territory.Id
          }
        })
    }

    this.actualTerritoriesOptions = []
    for(let i = 0, len = this.state.biActualAddresses.length; i < len; i++) {
      this.actualTerritoriesOptions[i] = territories
        .filter(territory => territory.Parent === this.state.terrParentId[i])
        .map(territory => {
          return {
            value: territory._id,
            label: territory.NameRus,
            id: territory.Id
          }
        })
    }

    this.individualPersonTerritoriesOptions = []
    for(let i = 0, len = this.state.biipfIndividualPersonAddresses.length; i < len; i++) {
      this.individualPersonTerritoriesOptions[i] = territories
        .filter(territory => territory.Parent === this.state.terrParentId[i])
        .map(territory => {
          return {
            value: territory._id,
            label: territory.NameRus,
            id: territory.Id
          }
        })
    }

    this.bileFounderTerritoriesOptions = []
    for(let i = 0, len = this.state.bileFounderAddresses.length; i < len; i++) {
      this.bileFounderTerritoriesOptions[i] = territories
        .filter(territory => territory.Parent === this.state.terrParentId[i])
        .map(territory => {
          return {
            value: territory._id,
            label: territory.NameRus,
            id: territory.Id
          }
        })
    }

    this.biipFounderTerritoriesOptions = []
    for(let i = 0, len = this.state.biipFounderAddresses.length; i < len; i++) {
      this.biipFounderTerritoriesOptions[i] = territories
        .filter(territory => territory.Parent === this.state.terrParentId[i])
        .map(territory => {
          return {
            value: territory._id,
            label: territory.NameRus,
            id: territory.Id
          }
        })
    }

    this.activityOptions = []
    for(let i = 0, len = this.state.biModalActivities.length; i < len; i++) {
      const parentActivity = this.state.biModalActivities[i - 1]
      let parentId

      if(parentActivity) {
        parentObj = activities.find(item => item._id === parentActivity)
        parentId = parentObj.Id
      }
      else {
        parentId = ''
      }

      this.activityOptions[i] = activities
        .filter(ativity => ativity.Parent === parentId)
        .map(ativity => {
          return {
            value: ativity._id,
            label: ativity.NameRus,
            id: ativity.Id
          }
        })
    }

    return (
      <NewRequest context={ this } locStrings={this.props.locStrings[this.props.lang]}/>
    )
  }
}

export default createContainer(() => {
  multipleSubscribe([
    'banks.names',
    'currencies',
    'customRoles.names',
    'documentTypes.all',
    'enterpreneurCategories',
    'enterpreneurTypes',
    'educations',
    'loanPrograms',
    'organizations',
    'paymentOrders',
    'productTypes',
    'steps',
    'subjectCategories',
    'users'
  ])
  Meteor.subscribe('territories.by.level', 2)
  Meteor.subscribe('activities.no.parent')

  const user = Meteor.user()
  const userId = Meteor.userId()
  const userIsOrganization = user.profile.isOrganization

  return {
    user,
    userId,
    userIsOrganization,
    activities: ActivitiesCollection.find().fetch(),
    banks: BanksCollection.find().fetch(),
    currencies: CurrenciesCollection.find().fetch(),
    documentTypes: DocumentTypesCollection.find().fetch(),
    enterpreneurTypes: EnterpreneurTypesCollection.find().fetch(),
    enterpreneurCategories: EnterpreneurCategoriesCollection.find().fetch(),
    educations: EducationsCollection.find().fetch(),
    loanPrograms: LoanProgramsCollection.find().fetch(),
    paymentOrders: PaymentOrdersCollection.find().fetch(),
    productTypes: ProductTypesCollection.find().fetch(),
    territories: TerritoriesCollection.find().fetch(),
    steps: StepsCollection.find({}, {
      sort: {
        order: 1
      }
    }).fetch(),
    subjectCategories: SubjectCategoriesCollection.find().fetch(),
    creditOrganizations: OrganizationsCollection.find({ 'profile.isOrganization' : true }).fetch(),
    users: UsersCollection.find().fetch(),
    customRoles: CustomRolesCollection.find().fetch()
  }
}, NewRequestContainer)
