import React from 'react'

export const borrowerInformation = {
  biActivities: [],
  biActualAddress: '',
  biActualAddressExtra: '',
  biActualAddressSavable: false,
  biActualAddresses: ['',],
  biBin: '',
  biBorrowerMarried: false,
  biBorrowerPartnerBirthdate: '',
  biBorrowerPartnerEducation: '',
  biBorrowerPartnerFullName: '',
  biBorrowerPartnerIin: '',
  biBorrowerPartnerIsEmployed: '',
  biBorrowerType: '',
  biCharterCapital: '',
  biDebtSum: '',
  biDebtsInfo: '',
  biEmployeesNumber: '',
  biEnterpreneurCategory: '',
  biEnterpreneurType: '',
  biFounders: [],
  biHasDebts: false,
  biHeadBirthdate: '',
  biHeadEducation: '',
  biHeadFullName: '',
  biHeadIin: '',
  biImportantInformation: '',
  biLegalAddress: {},
  biLegalAddressExtra: '',
  biLegalAddressSavable: false,
  biLegalAddresses: ['',],
  biModalActivities: ['',],
  biOrganizationName: '',
  biRegistrationDate: '',
  biServingBanks: [],
  biSubjectCategory: '',
}

export const borrowerInformationIndividualPerson = {
  biipBirthDate: '',
  biipEducation: '',
  biipFounderAddress: '',
  biipFounderAddressExtra: '',
  biipFounderAddressSavable: false,
  biipFounderAddresses: ['',],
  biipFullName: '',
  biipIin: '',
  biipPhone: '',
  biipShare: '',
}

export const borrowerInformationIndividualPersonForm = {
  biipfDob: '',
  biipfEducation: '',
  biipfEmail: '',
  biipfFio: '',
  biipfIin: '',
  biipfIndividualPersonAddress: '',
  biipfIndividualPersonAddressExtra: '',
  biipfIndividualPersonAddresses: ['',],
  biipfIndividualPersonCertificate: '',
  biipfIndividualPersonSavable: false,
  biipfMarried: false,
  biipfMobilePhone: '',
  biipfSpouseDob: '',
  biipfSpouseEducation: '',
  biipfSpouseFio: '',
  biipfSpouseIin: '',
  biipfspouseEmployment: false,
}

export const borrowerInformationLegalEntity = {
  bileBin: '',
  bileEnterpreneurType: '',
  bileFounderAddress: {},
  bileFounderAddressExtra: '',
  bileFounderAddressSavable: false,
  bileFounderAddresses: ['',],
  bilePhone: '',
  bileRegistrationDate: '',
  bileShare: '',
  bileShortName: '',
  bileSubjectCategory: '',
}

export const loanInformation = {
  liActivePledge: {},
  liCreditOrganization : '',
  liCurrency: '',
  liDate: '17/08/2017',
  liGracePeriodLoan: '',
  liGracePeriodPercent: '',
  liGuaranteeAmount: '',
  liInterestRate: '',
  liLendingPurpose: '',
  liLoanAgreementDate: '',
  liLoanAgreementNumber: '',
  liLoanProgram: '',
  liMicrocreditSum: '',
  liOwnfunds: '',
  liPaymentOrderLoan: '',
  liPaymentOrderPercent: '',
  liPledges: [],
  liProductType: '',
  liProjectName: '',
  liProjectPlace: {},
  liProjectPlaceSavable: false,
  liProjectPlaces: ['',],
  liProjectPurpose: '',
  liRequestAdmissionDate: '',
  liSpecialPurpose: [],
  liTerm: '',
}

export const lecm = {
  lecmBin: '',
  lecmEnterpreneurType: '',
  lecmPhone: '',
  lecmRegistrationDate: '',
  lecmShortName: '',
  lecmSubjectCategory: '',
}

export const leem = {
  leemBin: '',
  leemEnterpreneurType: '',
  leemId: '',
  leemPhone: '',
  leemRegistrationDate: '',
  leemShortName: '',
  leemSubjectCategory: '',
}

export const ipcm = {
  ipcmBirthDate: '',
  ipcmFullName: '',
  ipcmIin: '',
  ipcmPhone: '',
  ipcmLoading: false,
}

export const ipem = {
  ipemBirthDate: '',
  ipemFullName: '',
  ipemId: '',
  ipemIin: '',
  ipemLoading: false,
  ipemPhone: '',
}

export const pcm = {
  pcmCoverageShare: '',
  pcmEstimatedCost: '',
  pcmEstimationDate: '',
  pcmKl: '',
  pcmOtherInfo: '',
  pcmPlace: '',
  pcmPledgeCost: '',
  pcmRelation: '',
  pcmSubject: '',
}

export const pem = {
  pemCoverageShare: '',
  pemEstimatedCost: '',
  pemEstimationDate: '',
  pemIndex: '',
  pemKl: '',
  pemOtherInfo: '',
  pemPlace: '',
  pemPledgeCost: '',
  pemRelation: '',
  pemSubject: '',
}

export const spcm = {
  spcmName: '',
  spcmNumber: '',
  spcmPrice: '',
  spcmUnit: '',
}

export const spem = {
  spemIndex: '',
  spemName: '',
  spemNumber: '',
  spemPrice: '',
  spemUnit: '',
}

export const errorInformation = {
  biipfIinErrors: [],
  biipfSpouseIinErrors: [],
  liCreditOrganizationWarn: false,
  liProductTypeWarn: false,
  liLoanProgramWarn: false,
  liPaymentOrderPercentWarn: false,
  liPaymentOrderLoanWarn: false,
  liMicrocreditSumWarn: false,
  liInterestRateWarn: false,
  liTermWarn: false,
  liGuaranteeAmountWarn: false,
  liOwnfundsWarn: false,
  liGracePeriodPercentWarn: false,
  liGracePeriodLoanWarn: false,
  spcmNameWarn: true
}
