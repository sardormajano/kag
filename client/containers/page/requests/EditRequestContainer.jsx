import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import EditRequest from '/client/components/page/requests/EditRequest'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import { CurrenciesCollection } from '/api/currencies'
import { CustomRolesCollection } from '/api/customRoles'
import { DocumentTypesCollection } from '/api/documentTypes'
import { EnterpreneurTypesCollection } from '/api/enterpreneurTypes'
import { ProductTypesCollection } from '/api/productTypes'
import { StepsCollection } from '/api/steps'
import { UsersCollection } from '/api/users'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const requiredFields = [
  'mdCurrency',
  'mdProductType',
  'mdEnterpreneurType',
  'mdMicrocreditSum',
  'mdInterestRate',
  'mdTerm',
  'mdGuaranteeAmount',
  'mdLendingPurpose',
  'mdMainDebtOrderAndTerm',
  'mdRemunerationOrderAndTerm'
]

const defaultState = {
  activeStep: 0,

  mdCurrency: '',
  mdProductType: '',
  mdEnterpreneurType: '',
  mdMicrocreditSum: '',
  mdInterestRate: '',
  mdTerm: '',
  mdGuaranteeAmount: '',
  mdLendingPurpose: '',
  mdMainDebtOrderAndTerm: '',
  mdRemunerationOrderAndTerm: '',
  mdSpecialConditions: '',

  arMessage: '',
  arAgreeing: '',
  arApproving: '',

  phFiles: []
}

class EditRequestContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  updateTheRequest() {
    Meteor.call('request.get', this.props.match.params._id, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
        // if(res.holders.includes(Meteor.userId())) {
          this.setState(res)
        // }
        // else {
        //   this.props.history.push('/')
        // }
      }
    })
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)

    this.updateTheRequest()
  }

  writeMessageHandler() {
    Meteor.call('request.write.message', this.state._id, this.state.arMessage, this.state.userIp, (err, res) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        this.setState({ arMessage: '' })
        this.updateTheRequest()

        Bert.alert({
            message: 'Сообщение успешно сохранено!',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }
    })
  }

  sendForAgreementHandler() {
    Meteor.call(
      'request.send.for.agreement',
      this.props.match.params._id,
      this.state.arAgreeing,
      this.state.userIp,
      err => {
        if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          })
        }
        else {
          this.props.history.push('/')
        }
      }
    )
  }

  sendForApprovalHandler() {
    Meteor.call(
      'request.send.for.approval',
      this.props.match.params._id,
      this.state.arApproving,
      this.state.userIp,
      err => {
        if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          })
        }
        else {
          this.props.history.push('/')
        }
      }
    )
  }

  saveChanges() {
    const data = {
      mdCurrency: this.state.mdCurrency,
      mdProductType: this.state.mdProductType,
      mdEnterpreneurType: this.state.mdEnterpreneurType,
      mdMicrocreditSum: this.state.mdMicrocreditSum,
      mdInterestRate: this.state.mdInterestRate,
      mdTerm: this.state.mdTerm,
      mdGuaranteeAmount: this.state.mdGuaranteeAmount,
      mdLendingPurpose: this.state.mdLendingPurpose,
      mdMainDebtOrderAndTerm: this.state.mdMainDebtOrderAndTerm,
      mdRemunerationOrderAndTerm: this.state.mdRemunerationOrderAndTerm,
      mdSpecialConditions: this.state.mdSpecialConditions,
      incomplete: true,
      phFiles: this.state.phFiles
    }

    Meteor.call('request.upsert', this.state._id, data, this.state.userIp, (err, res) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        res.insertedId && this.setState({ _id: res.insertedId })

        Bert.alert({
            message: 'Заявка успешно сохранена!',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }
    })
  }

  goToNextStep(e) {
    if(this.state.activeStep === this.props.steps.length)
      return

    this.setState({ activeStep: this.state.activeStep + 1 })
  }

  goToPrevStep(e) {
    e.preventDefault()

    if(this.state.activeStep === 0)
      return

    this.setState({ activeStep: this.state.activeStep - 1 })
  }

  mdFieldsFilledOut() {
    return !requiredFields.some(field => {
      if(!this.state[field])
        return true
      if(typeof this.state[field] === 'number')
        return false
      else {
        return this.state[field] === '' || !this.state[field].length
      }
    })
  }

  otherFieldsAreFilledOut() {
    const { steps } = this.state

    if(!steps)
      return false

    const { documentTypes } = this.props

    for(let key in steps) {
      for(let innerKey in steps[key]) {
        const theDocumentType = documentTypes.filter(dType => dType._id === innerKey)[0]

        if(!theDocumentType || (theDocumentType.required && !steps[key][innerKey].length))
        {
          return false
        }
      }
    }

    return true
  }

  updateRequest() {
    Meteor.call('request.update', this.state._id, this.state, this.state.userIp, err => err && console.log(err.reason))
  }

  completeRequest(e) {
    e.preventDefault()

    Meteor.call('request.complete', this.state._id, err => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: 'Заявка успешно завершена!',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.props.history.push('/requests')
      }
    })
  }

  render() {
    const { currencies, productTypes, enterpreneurTypes } = this.props

    this.currencyOptions = currencies.map(currency => {
      return {
        value: currency._id,
        label: currency.name.ru
      }
    })

    this.productTypeOptions = productTypes.map(productType => {
      return {
        value: productType._id,
        label: productType.name.ru
      }
    })

    this.enterpreneurTypeOptions = enterpreneurTypes.map(enterpreneurType => {
      return {
        value: enterpreneurType._id,
        label: enterpreneurType.name.ru
      }
    })

    return (
      <EditRequest context={ this }/>
    )
  }
}

export default createContainer(() => {
  multipleSubscribe([
    'currencies',
    'customRoles.names',
    'documentTypes.all',
    'enterpreneurTypes',
    'productTypes',
    'steps',
    'users'
  ])
  const user = Meteor.user()
  const userId = Meteor.userId()
  const userIsOrganization = user.profile.isOrganization

  return {
    user,
    userId,
    userIsOrganization,
    currencies: CurrenciesCollection.find().fetch(),
    documentTypes: DocumentTypesCollection.find().fetch(),
    enterpreneurTypes: EnterpreneurTypesCollection.find().fetch(),
    productTypes: ProductTypesCollection.find().fetch(),
    steps: StepsCollection.find({}, {
      sort: {
        order: 1
      }
    }).fetch(),
    users: UsersCollection.find().fetch(),
    custoRoles: CustomRolesCollection.find().fetch()
  }
}, EditRequestContainer)
