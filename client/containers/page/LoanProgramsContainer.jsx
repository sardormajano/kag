import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import LoanPrograms from '/client/components/page/LoanPrograms'
import { LoanProgramsCollection } from '/api/loanPrograms'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cbmNameRu: '',
  cbmNameKz: '',

  ebmNameKz: '',
  ebmNameRu: '',
  ebmLoanProgramId: ''
}

class LoanProgramsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteLoanProgram(_id) {
    Meteor.call('loanProgram.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].loanProgramDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertLoanProgram() {
    const data = {
      name: {
        ru: this.state.cbmNameRu,
        kz: this.state.cbmNameKz
      }
    }

    Meteor.call('loanProgram.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].loanProgramCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updateLoanProgram() {
    const data = {
      name: {
        ru: this.state.ebmNameRu,
        kz: this.state.ebmNameKz
      },
    }

    Meteor.call('loanProgram.update', this.state.ebmLoanProgramId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].loanProgramEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <LoanPrograms context={ this } locStrings={locStrings[lang]} />
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['loanPrograms'])

  return {
    loanPrograms: LoanProgramsCollection.find().fetch(),
    ...props
  }
}, LoanProgramsContainer)
