import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import DocumentTypes from '/client/components/page/DocumentTypes'
import { DocumentTypesCollection } from '/api/documentTypes'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cmNameRu: '',
  cmNameKz: '',
  cmTooltip: '',
  cmMultiple: false,
  cmRequired: false,

  emNameRu: '',
  emNameKz: '',
  emTooltip: '',
  emMultiple: false,
  emRequired: false,
  ebmDocumentTypeId: '',

  filterRu: '',
  filterKz: ''
}

class DocumentTypesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteDocumentType(_id) {
    Meteor.call('documentType.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].documentTypeDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertDocumentType() {
    const data = {
      name: {
        ru: this.state.cmNameRu,
        kz: this.state.cmNameKz
      },
      tooltip: this.state.cmTooltip,
      multiple: this.state.cmMultiple,
      required: this.state.cmRequired
    }

    Meteor.call('documentType.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].documentTypeCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  updateDocumentType() {
    const data = {
      name: {
        ru: this.state.emNameRu,
        kz: this.state.emNameKz,
      },
      tooltip: this.state.emTooltip,
      multiple: this.state.emMultiple,
      required: this.state.emRequired
    }

    Meteor.call('documentType.update', this.state.emDocumentTypeId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].documentTypeEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <DocumentTypes context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['documentTypes.all'])

  return {
    documentTypes: DocumentTypesCollection.find().fetch(),
    ...props,
  }
}, DocumentTypesContainer)
