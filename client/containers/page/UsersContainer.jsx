import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Users from '/client/components/page/Users'
import { BranchesCollection } from '/api/branches'
import { CustomRolesCollection } from '/api/customRoles'
import { DepartmentsCollection } from '/api/departments'
import { UsersCollection } from '/api/users'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'
import { iinCheck } from '/client/lib/formValidationRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cumIin: '',
  cumFirstName: '',
  cumLastName: '',
  cumMiddleName: '',
  cumPassword: '',
  cumEmail: '',
  cumMobilePhone: '',
  cumRoles: [],
  cumDepartment: '',
  cumBranch: '',

  eumIin: '',
  eumFirstName: '',
  eumLastName: '',
  eumMiddleName: '',
  eumPassword: '',
  eumEmail: '',
  eumMobilePhone: '',
  eumRoles: [],
  eumDepartment: '',
  eumUserId: '',
  eumBranch: '',

  cumIinErrors: [],
  eumIinErrors: [],

  branchId: '',
}

class UsersContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  cumIinValidate() {
    const iinCorrect = iinCheck(this.state.cumIin)

    const { cumIinErrors } = this.state

    let newCumIinErrors = []

    if(iinCorrect) {
      newCumIinErrors = cumIinErrors.filter(item => item != 'Неверный ИИН/БИН')
    }
    else if (cumIinErrors.indexOf('Неверный ИИН/БИН') === -1) {
      newCumIinErrors = [...cumIinErrors, 'Неверный ИИН/БИН']
    }
    else {
      newCumIinErrors = cumIinErrors
    }

    this.setState({ cumIinErrors: newCumIinErrors })
  }

  eumIinValidate() {
    const iinCorrect = iinCheck(this.state.eumIin)

    const { eumIinErrors } = this.state

    let newEumIinErrors = []

    if(iinCorrect) {
      newEumIinErrors = eumIinErrors.filter(item => item != 'Неверный ИИН/БИН')
    }
    else if (eumIinErrors.indexOf('Неверный ИИН/БИН') === -1) {
      newEumIinErrors = [...eumIinErrors, 'Неверный ИИН/БИН']
    }
    else {
      newEumIinErrors = eumIinErrors
    }

    this.setState({ eumIinErrors: newEumIinErrors })
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteUser(_id) {
    Meteor.call('user.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].userDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertUser() {
    const data = {
      iin: this.state.cumIin,
      firstName: this.state.cumFirstName,
      lastName: this.state.cumLastName,
      middleName: this.state.cumMiddleName,
      email: this.state.cumEmail,
      mobilePhone: this.state.cumMobilePhone,
      password: this.state.cumPassword,
      department: this.state.cumDepartment,
      branch: this.state.cumBranch,
      roles: this.state.cumRoles,
    }

    Meteor.call('user.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].userCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  updateUser() {
    const data = {
      iin: this.state.eumIin,
      firstName: this.state.eumFirstName,
      lastName: this.state.eumLastName,
      middleName: this.state.eumMiddleName,
      email: this.state.eumEmail,
      mobilePhone: this.state.eumMobilePhone,
      password: this.state.eumPassword,
      department: this.state.eumDepartment,
      branch: this.state.eumBranch,
      roles: this.state.eumRoles,
    }

    Meteor.call('user.update', this.state.eumUserId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].userEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const {
      branches,
      departments,
      customRoles,
      users,
      locStrings,
      lang
    } = this.props

    this.branchCapital = branches.some( branch => {
      if(branch._id === this.state.branchId) {
          return branch.code === 'AST' || branch.code === 'AQM'
      }
    })

    this.branchOptions = branches.map(branch => {
      return {
        value: branch._id,
        label: branch.name.ru
      }
    })
    this.departmentOptions = departments.map(dep => {
      return {
        value: dep._id,
        label: dep.name.ru
      }
    })

    this.roleOptions = customRoles.map(role => {
      return {
        value: role._id,
        label: role.name
      }
    })
    this.branch = branches.map(branch => {
      return {
        value: branch._id,
        label: branch.name.ru
      }
    })
    return (
      <Users context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe([
    'customRoles',
    'branches',
    'departments.names',
    'users'
  ])

  return {
    branches: BranchesCollection.find().fetch(),
    departments: DepartmentsCollection.find().fetch(),
    customRoles: CustomRolesCollection.find().fetch(),
    users: UsersCollection.find({
      _id: {
        $ne: props.userId
      }
    }).fetch(),
    ...props,
  }
}, UsersContainer)
