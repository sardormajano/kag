import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Branches from '/client/components/page/Branches'
import { BranchesCollection } from '/api/branches'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cbmNameRu: '',
  cbmNameKz: '',
  cbmShortName: '',
  cbmCode: '',

  ebmNameKz: '',
  ebmNameRu: '',
  ebmShortName: '',
  ebmCode: '',
  ebmBranchId: '',
}

class BranchesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteBranch(_id) {
    Meteor.call('branch.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].branchDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertBranch() {
    const data = {
      name: {
        ru: this.state.cbmNameRu,
        kz: this.state.cbmNameKz
      },
      shortName: this.state.cbmShortName,
      code: this.state.cbmCode,
    }

    Meteor.call('branch.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].branchCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updateBranch() {
    const data = {
      name: {
        ru: this.state.ebmNameRu,
        kz: this.state.ebmNameKz
      },
      shortName: this.state.ebmShortName,
      code: this.state.ebmCode,
    }

    Meteor.call('branch.update', this.state.ebmBranchId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].branchEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Branches context={ this } locStrings={locStrings[lang]} />
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['branches'])

  return {
    branches: BranchesCollection.find().fetch(),
    ...props
  }
}, BranchesContainer)
