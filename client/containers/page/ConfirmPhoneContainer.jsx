import React, { Component } from 'react'
import ConfirmPhone from '/client/components/page/ConfirmPhone'
import { createContainer } from 'meteor/react-meteor-data'

import { UsersCollection } from '/api/users'

const defaultState = {
  code: '',
  alertInfo: '',
  alertDanger: '',
  changeNumber: false,

  mobilePhone: '',
  hiddenPhone: '',
}

class ConfirmPhoneContainer extends Component {
  componentWillMount() {
    const { mobilePhone } = this.props.user.profile
    this.setState({
      hiddenPhone: mobilePhone.replace(mobilePhone.slice(9, 16), '** ***')
    })
  }

  componentDidUpdate() {
    if(this.props.user.profile.mobileConfirmed)
      this.props.history.push('/')
  }

  constructor(props) {
    super(props)

    this.state = defaultState
  }

  changeNumber(e) {
    e.preventDefault()

    const { locStrings, lang } = this.props

    if(this.state.mobilePhoneAsyncError) {
      Bert.alert({
          message: locStrings[lang].changeNumberError,
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
      })

      return false
    }

    Meteor.call('change.number', this.props.userId, this.state.mobilePhone, (err, res) => {
      const { mobilePhone } = this.state

      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: locStrings[lang].changeNumberSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
        this.setState({
          changeNumber: false,
          hiddenPhone: mobilePhone.replace(mobilePhone.slice(9, 16), '** ***')
        })
        this.resendCode(e)
      }
    })
  }

  verifyPhone(e) {
    e.preventDefault()

    const { locStrings, lang } = this.props

    Meteor.call('verify.phone', this.props.userId, parseInt(this.state.code), (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res === -1)
      {
        this.setState({
          alertDanger: '',
          alertInfo: locStrings[lang].verifyPhoneSuccess
        })
        this.forceUpdate()
      }
      else {
        if(res <= 3) {
          this.setState({ alertDanger: locStrings[lang].verifyPhoneError })
        }
        else {
          this.setState({ alertDanger: locStrings[lang].contactAdmin })
        }
      }
    })
  }

  resendCode(e) {
    const { locStrings, lang } = this.props
    
    Meteor.call('send.confirmation.phone', this.props.userId)
    this.setState({
      alertInfo: locStrings[lang].resendCodeInfo,
      alertDanger: ''
    }, () => {
      setTimeout(() => this.setState({
        alertInfo: ''
      }), 5000)
    })
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <ConfirmPhone context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer(props => {
  Meteor.subscribe('user', props.userId)

  return {
    user: UsersCollection.find().fetch(),
    ...props
  }
}, ConfirmPhoneContainer)
