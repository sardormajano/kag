import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Requests from '/client/components/page/Requests'
import { RequestsCollection } from '/api/requests'
import { RequestStatusesCollection } from '/api/requestStatuses'

// added by Talgat start 20.08.2017
import { BranchesCollection } from '/api/branches'
import { UsersCollection as OrganizationsCollection } from '/api/users'
// added by Talgat end

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  filterBranches: [],
  filterOrganizations: '',
  filterStatus: '',

  toRequest: '',
  buttonLabel: '...загрузка',
}

class RequestsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)

    Meteor.call('request.get.incomplete', (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.setState({
          toRequest: res ? res._id : '',
          buttonLabel: res ? this.props.locStrings[this.props.lang].reqFillContinueTrue : this.props.locStrings[this.props.lang].reqFillContinueFalse
         })
      }
    })
  }

  deleteRequest(_id) {
    Meteor.call('request.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].reqDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }
    });
  }

  render() {
    const {
      branches,
      organizations,
      requestStatuses
    } = this.props

    this.requestStatusOptions = requestStatuses.map(status => {
      return {
        value: status.code,
        label: status.nameRu
      }
    })

    this.organizationOptions = organizations.map(organization => {
      return {
        value: organization._id,
        label: organization.profile.shortName
      }
    })

    this.branchOptions = branches.map(branch => {
      return {
        value: branch._id,
        label: branch.name.ru
      }
    })

    return (
      <Requests context={ this } locStrings={this.props.locStrings[this.props.lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe([
    'requests',
    'requestStatuses',
    'branches',
    'organizations'
  ])

  return {
    requests: RequestsCollection.find().fetch(),
    branches: BranchesCollection.find().fetch(),
    requestStatuses: RequestStatusesCollection.find().fetch(),
    organizations: OrganizationsCollection.find().fetch(),
    ...props
  }
}, RequestsContainer)
