;import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import ProductTypes from '/client/components/page/ProductTypes'
import { ProductTypesCollection } from '/api/productTypes'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cmNameRu: '',
  cmNameKz: '',

  emNameKz: '',
  emNameRu: '',
  emProductTypeId: '',
}

class ProductTypesContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteProductType(_id) {
    Meteor.call('productType.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].productTypeDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertProductType() {
    const data = {
      name: {
        ru: this.state.cmNameRu,
        kz: this.state.cmNameKz
      }
    }

    Meteor.call('productType.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].productTypeCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    })
  }

  updateProductType() {
    const data = {
      name: {
        ru: this.state.emNameRu,
        kz: this.state.emNameKz
      }
    }

    Meteor.call('productType.update', this.state.emProductTypeId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].productTypeEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <ProductTypes context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['productTypes'])

  return {
    productTypes: ProductTypesCollection.find().fetch(),
    ...props,
  }
}, ProductTypesContainer)
