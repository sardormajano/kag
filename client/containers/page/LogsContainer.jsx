import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Logs from '/client/components/page/Logs'
import { LogsCollection } from '/api/logs'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cbmNameRu: '',
  cbmNameKz: '',

  ebmNameKz: '',
  ebmNameRu: '',
  ebmLogId: '',

  payload: '',
  page: 1,
  pageSize: 10,

  filterIin: '',
  filterFio: '',
  filterIp: '',
  filterDate: ''
}

class LogsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)

    this.getPage()
  }

  getPage(e) {
    e && e.preventDefault()
    Meteor.call('get.logs.by.page', this.state.page, this.state.pageSize, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else {
        this.setState({ logs: res })
      }
    })
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Logs context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  return {
    ...props,
  }
}, LogsContainer)
