import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import Educations from '/client/components/page/Educations'
import { EducationsCollection } from '/api/educations'

import { multipleSubscribe } from '/client/lib/subscriptionsRelated'

import {
  setCanCreateUpdate,
  setCanDelete,
  redirectIfNotPermitted,
  setUserIP,
} from '/client/lib/permissionsRelated'

const defaultState = {
  cemNameRu: '',
  cemNameKz: '',

  eemNameKz: '',
  eemNameRu: '',
  eemEducationId: ''
}

class EducationsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = defaultState
  }

  componentWillMount() {
    redirectIfNotPermitted(this)
    setCanCreateUpdate(this)
    setCanDelete(this)
    setUserIP(this)
  }

  deleteEducation(_id) {
    Meteor.call('education.delete', _id, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].educationDeleteSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  insertEducation() {
    const data = {
      name: {
        ru: this.state.cemNameRu,
        kz: this.state.cemNameKz
      }
    }

    Meteor.call('education.insert', data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].educationCreateSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })
      }

      this.setState(defaultState)
    })
  }

  updateEducation() {
    const data = {
      name: {
        ru: this.state.eemNameRu,
        kz: this.state.eemNameKz
      }
    }

    Meteor.call('education.update', this.state.eemEducationId, data, this.state.userIp, (err, result) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        })
      }
      else {
        Bert.alert({
            message: this.props.locStrings[this.props.lang].educationEditSuccess,
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        })

        this.setState(defaultState)
      }
    });
  }

  render() {
    const { locStrings, lang } = this.props
    return (
      <Educations context={ this } locStrings={locStrings[lang]}/>
    )
  }
}

export default createContainer((props) => {
  multipleSubscribe(['educations'])

  return {
    educations: EducationsCollection.find().fetch(),
    ...props
  }
}, EducationsContainer)
