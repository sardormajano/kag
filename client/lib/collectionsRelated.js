export const idToName = (_id, collectionArray, nameProperty, nameSubProperty) => {
  if(!nameProperty) {
    const doc = collectionArray.filter(item => item._id === _id)[0];

    return doc ? `${doc.name} ${doc.surname || ''}` : 'не найдено'
  }
  else if(!nameSubProperty){
    const doc = collectionArray.filter(item => item._id === _id)[0];

    return doc ? `${doc[nameProperty]}` : 'не найдено'
  } else {
    const doc = collectionArray.filter(item => item._id === _id)[0];

    return doc ? `${doc[nameProperty][nameSubProperty]}` : 'не найдено'
  }
}

export const passwordPlaceholder = '__password__'
