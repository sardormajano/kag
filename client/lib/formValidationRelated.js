export const iinCheck = iin => {
  if (!iin)
    return false;
  if(iin === 'superadmin')
    return true
  if (iin.length != 12)
    return false;
  if (!(/[0-9]{12}/.test(iin)))
    return false;

  if (parseInt(iin.substring(2, 4)) > 12)
    return false;

  //Проверяем контрольный разряд
  let b1 = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11
  ];
  let b2 = [
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    1,
    2
  ];
  let a = [];
  let controll = 0;

  for (let i = 0; i < 12; i++) {
    a[i] = parseInt(iin.substring(i, i + 1));
    if (i < 11)
      controll += a[i] * b1[i];
    }

  controll = controll % 11;

  if (controll == 10) {
    controll = 0;
    for (let i = 0; i < 11; i++)
      controll += a[i] * b2[i];
    controll = controll % 11;
  }

  if (controll != a[11])
    return false;

  return true;
}
