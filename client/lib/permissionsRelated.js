export const redirectIfNotPermitted = (context) => {

  const { userId } = context.props
  const route = context.props.location.pathname.split('/')[1]
  const actionName = 'r'

  Meteor.call('page.permission.check', userId, route, actionName, (err, res) => {
    if(err) {
      console.log('error');
    }
    else if(!res){
      Bert.alert({
          message: "К сожалению у Вас нет доступа к этой странице. Пожалуйста подтвердите почту, либо обратитесь к администратору",
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
      })
      context.props.history.push('/')
    }
  })
}

export const setCanCreateUpdate = (context) => {
  const { userId } = context.props
  const route = context.props.location.pathname.split('/')[1]
  const actionName = 'cu'

  Meteor.call('page.permission.check', userId, route, actionName, (err, res) => {
    if(err) {
      console.log('error');
    }
    else {
      context.setState({ canCreateUpdate: res })
    }
  })
}

export const setCanDelete = (context) => {
  const { userId } = context.props
  const route = context.props.location.pathname.split('/')[1]
  const actionName = 'd'

  Meteor.call('page.permission.check', userId, route, actionName, (err, res) => {
    if(err) {
      console.log('error');
    }
    else {
      context.setState({ canDelete: res })
    }
  })
}

export const setUserIP = (context) => {
  $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
    context.setState({ userIp: data.ip})
  });
}
