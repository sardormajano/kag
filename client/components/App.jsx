import React from 'react'

import { createContainer } from 'meteor/react-meteor-data'

import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch
} from 'react-router-dom'

import Public from '/client/components/fuc/Public'
import Authenticated from '/client/components/fuc/Authenticated'

import SideMenuContainer from '/client/containers/fuc/SideMenuContainer'
import TopBar from '/client/components/fuc/TopBar'

import LoginContainer from '/client/containers/page/LoginContainer'
import SignupContainer from '/client/containers/page/SignupContainer'
import HomeContainer from '/client/containers/page/HomeContainer'
import RolesContainer from '/client/containers/page/RolesContainer'
import BranchesContainer from '/client/containers/page/BranchesContainer'
import PaymentOrdersContainer from '/client/containers/page/PaymentOrdersContainer'
import EducationsContainer from '/client/containers/page/EducationsContainer'
import CurrenciesContainer from '/client/containers/page/CurrenciesContainer'
import ProductTypesContainer from '/client/containers/page/ProductTypesContainer'
import EnterpreneurTypesContainer from '/client/containers/page/EnterpreneurTypesContainer'
import EnterpreneurCategoriesContainer from '/client/containers/page/EnterpreneurCategoriesContainer'
import SubjectCategoriesContainer from '/client/containers/page/SubjectCategoriesContainer'
import DepartmentsContainer from '/client/containers/page/DepartmentsContainer'
import BanksContainer from '/client/containers/page/BanksContainer'
import UsersContainer from '/client/containers/page/UsersContainer'
import OrganizationsContainer from '/client/containers/page/OrganizationsContainer'
import TerritoriesContainer from '/client/containers/page/TerritoriesContainer'
import ActivitiesContainer from '/client/containers/page/ActivitiesContainer'
import LoanProgramsContainer from '/client/containers/page/LoanProgramsContainer'

import RequestsContainer from '/client/containers/page/RequestsContainer'
import NewRequestContainer from '/client/containers/page/requests/NewRequestContainer'
import EditRequestContainer from '/client/containers/page/requests/EditRequestContainer'

import StepsContainer from '/client/containers/page/StepsContainer'
import NewStepContainer from '/client/containers/page/steps/NewStepContainer'
import EditStepContainer from '/client/containers/page/steps/EditStepContainer'

import DocumentTypesContainer from '/client/containers/page/DocumentTypesContainer'
import LogsContainer from '/client/containers/page/LogsContainer'

import ConfirmEmail from '/client/components/page/ConfirmEmail'
import ConfirmedEmailContainer from '/client/containers/page/ConfirmedEmailContainer'
import ConfirmPhoneContainer from '/client/containers/page/ConfirmPhoneContainer'

import StimulsoftDemo from '/client/components/page/stimulsoft/Stimulsoft'
import StimulsoftViewer from '/client/components/page/stimulsoft/StimulsoftViewer'

import ResetPasswordContainer from '/client/containers/page/ResetPasswordContainer'
import SetPasswordContainer from '/client/containers/page/SetPasswordContainer'

import {locStrings} from '/client/lib/localization'
// start of talgat testing area
// import TalgatTestComponent from '/client/TalgatTestComponent';
// end of talgat testing area

const App = props => (
  <Router>
    <Switch>
      <Route exact path="/stimulsoft-demo" component={ StimulsoftDemo }></Route>
      <Route exact path="/stimulsoft-viewer/:_id/:_reportName" component={ StimulsoftViewer }></Route>

      <Public exact path="/confirmEmail/:email" component={ ConfirmEmail } { ...props } />
      <Route exact path="/confirmedEmail/:_id" component={ ConfirmedEmailContainer } { ...props } />
      <Authenticated exact path="/confirmPhone" component={ ConfirmPhoneContainer } { ...props } />

      <Public exact path="/reset-password" component={ ResetPasswordContainer} {...props} />
      <Route exact path="/reset-password/:_id" component={ SetPasswordContainer} {...props} />

      <Route>
        <div id="wrapper">
          <Authenticated path="/" component={ SideMenuContainer} {...props} />
          <Authenticated path="/" component={ TopBar} {...props} />
          <Public exact path="/login" component={ LoginContainer } { ...props } />
          <Public exact path="/signup" component={ SignupContainer } { ...props } />
          <Authenticated exact path="/roles" component={ RolesContainer } { ...props } />
          <Authenticated exact path="/branches" component={ BranchesContainer } { ...props } />
          <Authenticated exact path="/paymentOrders" component={ PaymentOrdersContainer } { ...props } />
          <Authenticated exact path="/education" component={ EducationsContainer } { ...props } />
          <Authenticated exact path="/currencies" component={ CurrenciesContainer } { ...props } />
          <Authenticated exact path="/productTypes" component={ ProductTypesContainer } { ...props } />
          <Authenticated exact path="/enterpreneurTypes" component={ EnterpreneurTypesContainer } { ...props } />
          <Authenticated exact path="/enterpreneurCategories" component={ EnterpreneurCategoriesContainer } { ...props } />
          <Authenticated exact path="/subjectCategories" component={ SubjectCategoriesContainer } { ...props } />
          <Authenticated exact path="/departments" component={ DepartmentsContainer } { ...props } />
          <Authenticated exact path="/banks" component={ BanksContainer } { ...props } />
          <Authenticated exact path="/users" component={ UsersContainer } { ...props } />
          <Authenticated exact path="/organizations" component={ OrganizationsContainer } { ...props } />
          <Authenticated exact path="/logs" component={ LogsContainer } { ...props } />
          <Authenticated exact path="/territories" component={ TerritoriesContainer } { ...props } />
          <Authenticated exact path="/activities" component={ ActivitiesContainer } { ...props } />
          <Authenticated exact path="/loanPrograms" component={ LoanProgramsContainer } { ...props } />
          <Authenticated exact path="/documentTypes" component={ DocumentTypesContainer } { ...props } />
          <Authenticated exact path="/" component={ HomeContainer } { ...props } />

          <Authenticated exact path="/requests" component={ RequestsContainer } { ...props } />
          <Authenticated exact path="/requests/newRequest" component={ NewRequestContainer } { ...props } />
          <Authenticated exact path="/requests/newRequest/:_id" component={ NewRequestContainer } { ...props } />

          <Authenticated exact path="/requests/edit" component={ EditRequestContainer } { ...props } />

          <Authenticated exact path="/steps" component={ StepsContainer } { ...props } />
          <Authenticated exact path="/steps/newStep" component={ NewStepContainer } { ...props } />
          <Authenticated exact path="/steps/editStep/:_id" component={ EditStepContainer } { ...props } />
        </div>
      </Route>
    </Switch>
  </Router>
)

export default createContainer(() => {

  const loggingIn = Meteor.loggingIn()
  const user = Meteor.user()
  const userId = Meteor.userId()
  const userIsOrganization = user ? user.profile.isOrganization : false
  const loading = Roles.subscription ? !Roles.subscription.ready() : true

  return {
    loggingIn,
    loading,
    user,
    userId,
    userIsOrganization,
    locStrings,
    authenticated: !loggingIn && !!userId,
    roles: !loading && Roles.getRolesForUser(userId),
  }
}, App)
