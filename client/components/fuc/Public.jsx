import React from 'react'
import {
  Route,
  Redirect
} from 'react-router-dom'

export default Public = ({ loggingIn, authenticated, component, locStrings, ...rest }) => (
  <Route
    { ...rest }
    render={ props => {
      if(loggingIn) return <div />
      return !authenticated ?
      (React.createElement(component, { ...props, loggingIn, authenticated, locStrings })) :
      <Redirect to='/' />
    } }
  />
)
