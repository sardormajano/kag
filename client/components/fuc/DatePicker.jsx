import React, { Component } from 'react'

export default class DatePicker extends Component {
  componentDidMount() {
    const { context, stateName } = this.props

    $(this.i).datepicker({
        autoclose: true,
        todayHighlight: true
    })
    .on('hide', e => {
      context.setState({
        [stateName]: e.target.value
      })
    })

  }

  componentDidUpdate() {
    const { context, stateName } = this.props

    $(this.i).datepicker('update', context.state[stateName])
  }

  render() {
    const { context, stateName, ...rest } = this.props

    return (
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="дд/мм/гггг"
          data-date-format="dd/mm/yyyy"
          ref={ i => this.i = i }
        />
        <span className="input-group-addon bg-custom b-0"><i className="mdi mdi-calendar text-white" /></span>
      </div>
    )
  }
}
