import React from 'react'

export default Footer = () => (
  <footer className="footer text-right">
    2016 © АО "КазАгроГарант".
  </footer>
)
