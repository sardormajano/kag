import React, { Component } from 'react'

export default class DatePicker extends Component {
  addSpaces(str) {
    if(!str || str === 'null')
      return ''

    str = str.trim().split(' ').join('')
    str = str.replace(/[^\d]/g, '')
    str = parseInt(str).toLocaleString('ru-RU')

    return str
  }

  render() {
    const { context, stateName, ...rest } = this.props

    return (
      <input
        className="form-control"
        placeholder="введите сумму"
        onChange={ e => {
          let val = e.currentTarget.value
          val = this.addSpaces(val)
          context.setState({ [stateName]: val })
        }}
        value={ context.state[stateName] }
      />
    )
  }
}
