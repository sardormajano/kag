import React, { Component } from 'react'

export default class Textarea extends Component {
  componentDidMount() {
    $(this.i).maxlength({
        alwaysShow: true
    })
  }

  render() {
    const { context, stateName, ...rest } = this.props

    return (
      <textarea
        ref={i => this.i = i}
        onChange={ e => {
          context.setState({ [stateName]: e.currentTarget.value},
            () => context[`${stateName}Validate`] && context[`${stateName}Validate`]())
        } }
        value={ context.state[stateName] }
        { ...rest }
      />
    )
  }
}
