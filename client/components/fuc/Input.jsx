import React, { Component } from 'react'
import InputMask from 'react-input-mask'

export default class Input extends Component {
  render() {
    const { context, stateName, ...rest } = this.props

    return (
      <div>
        {
          this.props['data-mask'] ? (
            <InputMask
              ref={i => this.i = i}
              mask={ this.props['data-mask'] }
              onChange={ e => {
                context.setState({ [stateName]: e.currentTarget.value},
                  () => context[`${stateName}Validate`] && context[`${stateName}Validate`]())
              } }
              value={ context.state[stateName] }
              { ...rest }
            />
          ) : (
            <input
              ref={i => this.i = i}
              onChange={ e => {
                context.setState({ [stateName]: e.currentTarget.value},
                  () => context[`${stateName}Validate`] && context[`${stateName}Validate`]())
              } }
              value={ context.state[stateName] }
              { ...rest }
            />
          )
        }
        <ul className="parsley-errors-list filled">
          {
            context.state[`${stateName}Errors`] && context.state[`${stateName}Errors`].map((error, index) => (
              <li key={ index } className="parsley-type">{ error }</li>
            ))
          }
        </ul>
      </div>
    )
  }
}
