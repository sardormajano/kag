import React from 'react'

export default Preloader = ({ context }) => (
  <div id="preloader" style={{display: context.preloader.visible ? '' : 'none'}}>
    <div id="status">
      <div className="spinner">
        <div className="spinner-wrapper">
          <div className="rotator">
            <div className="inner-spin" />
            <div className="inner-spin" />
          </div>
        </div>
      </div>
    </div>
  </div>
)
