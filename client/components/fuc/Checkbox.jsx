import React, { Component } from 'react'

export default class Checkbox extends Component {
  render() {
    const { context, stateName, ...rest } = this.props

    return (
      <input
        type="checkbox"
        onChange={ e => {
          context.setState({ [stateName]: e.currentTarget.checked })
        } }
        checked={ context.state[stateName] }
        { ...rest }
      />
    )
  }
}
