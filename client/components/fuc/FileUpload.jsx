import React, { Component } from 'react'

import { idToName } from '/client/lib/collectionsRelated'
import Confirm from 'react-confirm-bootstrap'
import {locStrings} from '../../lib/localization'
import { cookie } from '/client/lib/coreLib'

export default class FileUpload extends Component {
  renderUploadingFiles() {
    let lang = cookie.get('lang')
    const {
      context,
      stateName,
      documentType,
      deleteFileHandler
    } = this.props


    if(!context.state.steps) {
      context.state.steps = {}
    }

    if(!context.state.steps[stateName]) {
      context.state.steps[stateName] = {}
    }

    if(!context.state.steps[stateName][documentType.value]) {
      context.state.steps[stateName][documentType.value] = []
    }

    const uploadingFiles = context.state.steps[stateName][documentType.value]

    if (stateName === 'phFiles') {
      return uploadingFiles.map((file, index) => {
        let iconClassName

        if(file.progress < 100) {
          iconClassName = 'fa fa-spin fa-spinner'
        }
        else {
          iconClassName = ''
        }

        Meteor.call('get.link', file, (err, res) => {
          if(err) {
            console.log(err.reason)
          }
          else {
            const link = document.querySelector(`#link-${file._id}`)
            const img = document.querySelector(`#img-${file._id}`)
            const src = document.querySelector(`#src-${file._id}`)
            link.href = res
            img.src = res
            src.href = res
          }
        })

        return (
          <div className="col-md-4" key={ index }>
            <br />
            <div className="thumbnail">
              <span
                className="col-md-2"
                style={ {
                  display: file.progress === 100 ? 'none' : ''
                } }
              >
                <i className={iconClassName} /> &nbsp; &nbsp; { file.progress }%
              </span>
              <a id={ `link-${file._id}` } href="#" data-lightbox="photoreport" data-title={file.name}>
                <img className="img-rounded img-responsive" id={ `img-${file._id}` } src="#" alt="" />
              </a>
              <br />
              <div className="row" >
                  <a
                    className="col-md-6 text-right"
                    id={ `src-${file._id}` }
                    href="#"
                    download
                    target='_blank'
                  >
                    <i className="mdi mdi-download mdi-18px"/>
                  </a>
                  <Confirm
                      onConfirm={() => deleteFileHandler(file)}
                      body={locStrings[lang].photoDeleteConfirm}
                      confirmText={locStrings[lang].yes}
                      cancelText={locStrings[lang].cancel}
                      title={locStrings[lang].photoDelete}>
                      <a
                        className="col-md-6"
                        onClick={ e => {
                          e.preventDefault()
                        } }
                      >
                        <i className="mdi mdi-delete mdi-18px"/>
                      </a>
                  </Confirm>
              </div>
            </div>
          </div>
        )
      })
    }

    return uploadingFiles.map((file, index) => {
      let iconClassName


      if(file.progress < 100) {
        iconClassName = 'fa fa-spin fa-spinner'
      }
      else {
        iconClassName = ''
      }

      Meteor.call('get.link', file._id, (err, res) => {
        if(err) {
          console.log(err.reason)
        }
        else {
          const link = document.querySelector(`#link-${file._id}`)
          link.href = res
        }
      })

      return (
        <div
          key={ index }
          className="input-group m-t-10"
        >
          <span
            className="input-group-addon font-13"
            style={ {
              display: file.progress === 100 ? 'none' : ''
            } }
          >
            <i className={iconClassName} /> &nbsp; &nbsp; { file.progress }%
          </span>
          <input
            className="form-control"
            disabled={ true }
            value={ file.name }
          />
          <span className="input-group-addon font-13">
            <a
              id={ `link-${file._id}` }
              href="#"
              download
              target='_blank'
            >
              {locStrings[lang].download}
            </a>
          </span>
          <span className="input-group-addon font-13">
            <a
              href="javascript:;"
              onClick={ e => deleteFileHandler(file) }
            >
              {locStrings[lang].delete}
            </a>
          </span>
        </div>
      )
    })
  }

  render() {
    const { context, documentType, uploaderChangeHandler } = this.props
    const { documentTypes } = context.props
    let lang = cookie.get('lang')

    const theDocumentType = documentTypes.filter(doc => doc._id === documentType.value)[0]

    if(!theDocumentType)
      return <div />

    return (
      <div
        className="form-group"
      >
        <div className="panel panel-purple panel-border">
          <div className="panel-heading"></div>
          <div className="panel-body">
            <label>
              { theDocumentType.required ? '* ' : '' }
              { theDocumentType.name.ru }
            </label>
            &nbsp;&nbsp;&nbsp;
            <i
              ref={ tooltip => this.tooltip = tooltip }
              title={ theDocumentType.tooltip }
              className="fa fa-question-circle"
            />
            &nbsp;&nbsp;&nbsp;
            <input
              style={ { display: 'none' } }
              ref={ fu => this.fu = fu }
              type="file"
              onChange={ uploaderChangeHandler }
              multiple={ theDocumentType.multiple === true ? true : false}
            />
            <button
              type="button"
              className="btn btn-default"
              onClick={ () => this.fu.click() }
            >
              <i className="fa fa-download" />
              &nbsp;&nbsp;
              {locStrings[lang].upload}
            </button>
            <div className="row">
              { this.renderUploadingFiles() }
            </div>
          </div>
        </div>
      </div>
    )
  }
}
