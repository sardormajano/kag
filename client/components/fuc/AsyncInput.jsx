import React, { Component } from 'react'
import InputMask from 'react-input-mask'

export default class Input extends Component {
  render() {
    const {
      blurHandler,
      changeHandler,
      value,
      type,

      errors,
    } = this.props


    return (
      <div>
        {
          this.props['data-mask'] ? (
            <InputMask
              ref={i => this.i = i}
              mask={ this.props['data-mask'] }
              onChange={ changeHandler }
              onBlur={ blurHandler }
              value={ value }
              type={ type }
              className='form-control'
            />
          ) : (
            <input
              ref={i => this.i = i}
              onBlur={ blurHandler }
              onChange={ changeHandler }
              value={ value }
              type={ type }
              className='form-control'
            />
          )
        }
        <ul className="parsley-errors-list filled">
          
          {
            errors.map((error, index) => (
              <li key={ index } className="parsley-type">{ error }</li>
            ))
          }
        </ul>
      </div>
    )
  }
}
