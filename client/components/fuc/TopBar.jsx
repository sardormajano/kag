import React, { Component } from 'react'

import { cookie } from '/client/lib/coreLib'

export default class TopBar extends Component {
  onLangChange(lang) {
    switch(lang){
      case 'kz': cookie.set('lang', 'ru', 9999)
        break
      case 'ru': cookie.set('lang', 'kz', 9999)
        break
    }
  }

  render() {
    const { locStrings, loggingIn, user, userIsOrganization, lang } = this.props

    return (
      <div className="topbar">
        {/* LOGO */}
        <div className="topbar-left">
          {/* <a href="index.html" class="logo"><span>KAG</span><i class="mdi mdi-cube"></i></a> */}
          {/* Image logo */}
          <a href="/" className="logo">
            <span>
              <img src="/assets/images/logo2.svg" alt height={30} />
            </span>
            <i>
              <img src="/assets/images/logo_sm.svg" alt height={28} />
            </i>
          </a>
        </div>
        {/* Button mobile view to collapse sidebar menu */}
        <div className="navbar navbar-default" role="navigation">
          <div className="container">
            {/* Navbar-left */}
            <ul className="nav navbar-nav navbar-left">
              <li>
                <button className="button-menu-mobile open-left waves-effect waves-light">
                  <i className="mdi mdi-menu" />
                </button>
              </li>
              <li className="hidden-xs">
                <form role="search" className="app-search">
                  <input type="text" placeholder={`${locStrings[lang].search}...`} className="form-control" />
                  <a href><i className="fa fa-search" /></a>
                </form>
              </li>
              {/* <li class="hidden-xs"> */}
              {/* <a href="#" class="menu-item waves-effect waves-light">New</a> */}
              {/* </li> */}
              <li className="dropdown hidden-xs">
                <a data-toggle="dropdown" className="dropdown-toggle menu-item waves-effect waves-light" href="#" aria-expanded="false">{lang === 'ru' ? 'Русский' : 'Қазақ'}
                  <span className="caret" /></a>
                <ul role="menu" className="dropdown-menu">
                  <li><a href="#" onClick={() => this.onLangChange(lang)}>{lang === 'kz' ? 'Русский' : 'Қазақ'}</a></li>
                </ul>
              </li>
            </ul>
            {/* Right(Notification) */}
            <ul className="nav navbar-nav navbar-right">
              <li>
                <a href="#" className="right-menu-item dropdown-toggle" data-toggle="dropdown">
                  <i className="mdi mdi-bell" />
                  <span className="badge up bg-primary">4</span>
                </a>
                <ul className="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-s-lg user-list notify-list">
                  <li>
                    <h5>{locStrings[lang].notifications}</h5>
                  </li>
                  <li>
                    <a href="#" className="user-list-item">
                      <div className="icon bg-success">
                        <i className="mdi mdi-check" />
                      </div>
                      <div className="user-desc">
                        <span className="name">Юрист: Заявка №256 утверждена</span>
                        <span className="time">20 минут назад</span>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="user-list-item">
                      <div className="icon bg-danger">
                        <i className="mdi mdi-close" />
                      </div>
                      <div className="user-desc">
                        <span className="name">Риск-менеджер: Заявка №685 отклонена</span>
                        <span className="time">2 часа назад</span>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="user-list-item">
                      <div className="icon bg-info">
                        <i className="mdi mdi-plus" />
                      </div>
                      <div className="user-desc">
                        <span className="name">Поступила новая заявка на утверждение</span>
                        <span className="time">3 часа назад</span>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="user-list-item">
                      <div className="icon bg-danger">
                        <i className="mdi mdi-clock-alert" />
                      </div>
                      <div className="user-desc">
                        <span className="name">Истекает срок рассмотрения заявки №455</span>
                        <span className="time">15 минут назад</span>
                      </div>
                    </a>
                  </li>
                  <li className="all-msgs text-center">
                    <p className="m-0"><a href="#">{locStrings[lang].allNotifications}</a></p>
                  </li>
                </ul>
              </li>
              <li className="dropdown user-box">
                <a href className="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown" aria-expanded="true">
                  <img src={ `/assets/images/${ userIsOrganization ? 'organization-ava' : 'user-ava'}.png` } alt="user-img" className="img-circle user-img" />
                </a>
                <ul className="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                  <li><a href="profile.html"><i className="ti-user m-r-5" /> Профиль</a></li>
                  <li>
                    <a
                      href="#"
                      onClick={() => Meteor.logout()}
                    >
                      <i className="ti-power-off m-r-5" />
                      {locStrings[lang].logout}
                    </a>
                  </li>
                </ul>
              </li>
            </ul> {/* end navbar-right */}
          </div>{/* end container */}
        </div>{/* end navbar */}
      </div>
    )
  }
}
