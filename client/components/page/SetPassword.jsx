import React from 'react'
import { Link } from 'react-router-dom'

import Input from '/client/components/fuc/Input'
import { cookie } from '/client/lib/coreLib'

export default ({context}) => (
  <section>
    <div className="container-alt">
      <div className="row">
        <div className="col-sm-12">
          <div className="wrapper-page">
            <div className="m-t-40 account-pages">
              <div className="text-center account-logo-box">
                <h2 className="text-uppercase">
                  <a href="index.html" className="text-success">
                    <span><img src="/assets/images/logo.svg" alt height={36} /></span>
                  </a>
                </h2>
              </div>
              <div className="account-content">
                <form
                  className="form-horizontal"
                  action="#"
                  style={{display: !context.state.resetSuccess ? '' : 'none'}}
                  onSubmit={ context.onResetPassword.bind(context) }
                >
                  <div className="form-group">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].typeNewPassword}<span className="text-danger">*</span></label>
                      <Input
                        context={ context }
                        stateName='password'
                        className="form-control"
                        type="password"
                        required
                        placeholder={context.props.locStrings[context.state.lang].newPassword}
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].typeNewPassword}<span className="text-danger">*</span></label>
                      <Input
                        context={ context }
                        stateName='passwordConfirm'
                        className="form-control"
                        type="password"
                        required
                        placeholder={context.props.locStrings[context.state.lang].typeNewPassword}
                      />
                    </div>
                  </div>
                  <div className="form-group account-btn text-center m-t-10">
                    <div className="col-xs-12">
                      <button
                        className="btn w-md btn-bordered btn-danger waves-effect waves-light"
                        type="submit"
                        disabled={ context.state.password.length < 5 ? true : false }
                      >
                        {context.props.locStrings[context.state.lang].changePassword}
                      </button>
                    </div>
                  </div>
                </form>
                <div className="text-center m-b-20" style={{display: context.state.resetSuccess ? '' : 'none'}}>
                  <div className="m-b-20">
                    <div className="checkmark">
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 161.2 161.2" enableBackground="new 0 0 161.2 161.2">
                        <path className="path" fill="none" stroke="#4bd396" strokeMiterlimit={10} d="M425.9,52.1L425.9,52.1c-2.2-2.6-6-2.6-8.3-0.1l-42.7,46.2l-14.3-16.4
                                                    c-2.3-2.7-6.2-2.7-8.6-0.1c-1.9,2.1-2,5.6-0.1,7.7l17.6,20.3c0.2,0.3,0.4,0.6,0.6,0.9c1.8,2,4.4,2.5,6.6,1.4c0.7-0.3,1.4-0.8,2-1.5
                                                    c0.3-0.3,0.5-0.6,0.7-0.9l46.3-50.1C427.7,57.5,427.7,54.2,425.9,52.1z" />
                        <circle className="path" fill="none" stroke="#4bd396" strokeWidth={4} strokeMiterlimit={10} cx="80.6" cy="80.6" r="62.1" />
                        <polyline className="path" fill="none" stroke="#4bd396" strokeWidth={6} strokeLinecap="round" strokeMiterlimit={10} points="113,52.8
                                                    74.1,108.4 48.2,86.4 " />
                        <circle className="spin" fill="none" stroke="#4bd396" strokeWidth={4} strokeMiterlimit={10} strokeDasharray="12.2175,12.2175" cx="80.6" cy="80.6" r="73.9" />
                      </svg>
                    </div>
                  </div>
                  <p className="text-muted font-13 m-t-10">{context.props.locStrings[context.state.lang].passwordResetSuccess}</p>
                </div>
              </div>
            </div>
            {/* end card-box*/}
            <div className="row m-t-30" >
              <div className="col-sm-12 text-center">
                <p className="text-muted">{context.props.locStrings[context.state.lang].backToPage}
                  <Link to="/login" className="text-primary m-l-5">
                    <b>{context.props.locStrings[context.state.lang].logIn}</b>
                  </Link>
                </p>
              </div>
            </div>
          </div>
          {/* end wrapper */}
        </div>
      </div>
    </div>
  </section>
)
