import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'

export default class CreateSubjectCategoryModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { locStrings, lang } = context.props
    const allFieldsFilledOut = context.state.cmNameRu && context.state.cmNameKz

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].subjectCategoryCreate}</h4>
            </div>
            <div className="modal-body">
              <form className="form-horizontal" role="form">
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].ruName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='cmNameRu'
                    />

                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].kzName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='cmNameKz'
                    />

                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                type="button"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                className="btn btn-primary waves-effect waves-light"
                onClick={ () => {
                  this.closeButton.click()
                  context.insertSubjectCategory()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
