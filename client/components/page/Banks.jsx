import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateBankModal from '/client/components/page/banks/CreateBankModal'
import EditBankModal from '/client/components/page/banks/EditBankModal'

import Confirm from 'react-confirm-bootstrap'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'
import NumericInput from 'react-numeric-input'

export default class Banks extends Component {
  renderBanksTable() {
    const { context, locStrings } = this.props
    const { banks } = context.props

    let { filterName, filterBik } = context.state

    const filteredBanks = banks.filter(bank => {

      if(  !(new RegExp(filterBik, 'i')).test(bank.bik)
      ) {
          return false;
      }

      if(  !(new RegExp(filterName, 'i')).test(bank.name.ru) &&
           !(new RegExp(filterName, 'i')).test(bank.name.kz)
      ) {
          return false;
      }

      // //filtering by BIK
      // if(!filterBik) {
      //   filterBik = ''
      // }

      return true
    })

    return filteredBanks.map((bank, index) => (
      <tr className="gradeX" key={ index }>
        <td>{bank.name.ru}</td>
        <td>{bank.name.kz}</td>
        <td>{bank.bik}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-bank-modal"
            onClick={() => {
              context.setState({
                edmNameRu: bank.name.ru,
                edmNameKz: bank.name.kz,
                edmCode: bank.bik,
                edmBankId: bank._id
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={ () => context.deleteBank(bank._id) }
            body={locStrings.bankDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.bankDelete}
          >
            <a
              href="#"
              className="on-default remove-row"
              title={locStrings.delete}
              onClick={ e => {
                e.preventDefault()
              } }
            >
              <i className="fa fa-trash-o" />
            </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.banks}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.banks}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-bank-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>

                  <div className="panel panel-info">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          {locStrings.filters}
                        </a>
                      </h4>
                    </div>
                    <div className="panel-body">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-md-2 control-label">{locStrings.name}</label>
                            <div className="col-md-10">
                              <Input
                                type="text"
                                placeholder={locStrings.typeName}
                                className="form-control"
                                context={ context }
                                stateName='filterName'
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-md-2 control-label">{locStrings.bik}</label>
                            <div className="col-md-10">
                              <Input
                                type="text"
                                placeholder={locStrings.typeBik}
                                className="form-control"
                                context={ context }
                                stateName='filterBik'
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th>{locStrings.bik}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderBanksTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateBankModal
                id='create-bank-modal'
                context={ context }
              />
              <EditBankModal
                id='edit-bank-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
