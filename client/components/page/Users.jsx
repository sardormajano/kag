import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateUserModal from '/client/components/page/users/CreateUserModal'
import EditUserModal from '/client/components/page/users/EditUserModal'

import Confirm from 'react-confirm-bootstrap'

import { idToName, passwordPlaceholder } from '/client/lib/collectionsRelated'

export default class Users extends Component {
  renderUsersTable() {
    const { context, locStrings } = this.props
    const {
      branches,
      departments,
      customRoles,
      users,
    } = context.props

    return users.map((user, index) => (
      <tr className="gradeX" key={ index }>
        <td>
          {user.profile.firstName} {user.profile.lastName} {user.profile.middleName}
        </td>
        <td>{ user.emails[0].address }</td>
        <td>{ idToName(user.profile.branch, branches, 'name', 'ru') }</td>
        <td>{ user.profile.department ? idToName(user.profile.department, departments, 'name', 'ru') : '' }</td>
        <td>{ user.profile.roles && user.profile.roles.map(role => idToName(role.value, customRoles, 'name')).join('; ') }
        </td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-user-modal"
            onClick={() => {
              context.setState({
                eumIin: user.username,
                eumFirstName: user.profile.firstName,
                eumLastName: user.profile.lastName,
                eumMiddleName: user.profile.middleName,
                eumPassword: passwordPlaceholder,
                eumEmail: user.emails[0].address,
                eumMobilePhone: user.profile.mobilePhone,
                eumRoles: user.profile.roles,
                eumBranch: user.profile.branch,
                eumDepartment: user.profile.department,
                eumUserId: user._id,
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
              style={{
                display: context.state.canDelete ? '' : 'none'
              }}
              onConfirm={() => context.deleteUser(user._id)}
              body={locStrings.userDeleteConfirm}
              confirmText={locStrings.yes}
              cancelText={locStrings.cancel}
              title={locStrings.userDelete}>
              <a
                href="#"
                className="on-default remove-row"
                title={locStrings.delete}
                onClick={ e => {
                  e.preventDefault()
                } }
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.users}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.users}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-user-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.fio}</th>
                          <th>{locStrings.email}</th>
                          <th>{locStrings.branch}</th>
                          <th>{locStrings.department}</th>
                          <th>{locStrings.roles}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderUsersTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateUserModal
                id='create-user-modal'
                context={ context }
              />
              <EditUserModal
                id='edit-user-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
