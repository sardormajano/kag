import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'

import { Link } from 'react-router-dom'

import Confirm from 'react-confirm-bootstrap'

export default class Steps extends Component {
  renderStepsTable() {
    const { context, locStrings } = this.props
    const { steps } = context.props
    const { levelStepUp, levelStepDown } = context

    return steps.map((step, index) => (
      <tr className="gradeX" key={ index }>
        <td>{step.order}</td>
        <td>{step.name}</td>
        <td>{step.documentTypes.length}</td>
        <td className="actions text-center">
          <Link
            to={`/steps/editStep/${step._id}`}
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
          >
            <i className="fa fa-pencil" />
          </Link>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={ () => context.deleteStep.call(context, step._id) }
            body={locStrings.stepDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.stepDelete}
          >
            <a
              href="#"
              className="on-default remove-row"
              title={locStrings.delete}
              onClick={ e => {
                e.preventDefault()
              } }
            >
              <i className="fa fa-trash-o" />
            </a>
          </Confirm>
          <a
            href="#"
            title={locStrings.stepUp}
            className="on-default remove-row"
            onClick={ e =>  {
              e.preventDefault()
              levelStepUp.call(context, step._id)
            } }
          >
            <i className="fa fa-level-up" />
          </a>{' '}
          <a
            href="#"
            title={locStrings.stepDown}
            className="on-default remove-row"
            onClick={ e =>  {
              e.preventDefault()
              levelStepDown.call(context, step._id)
            } }
          >
            <i className="fa fa-level-down" />
          </a>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.steps}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.steps}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <Link
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          className="btn btn-primary waves-effect waves-light"
                          to="/steps/newStep"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.order}</th>
                          <th>{locStrings.name}</th>
                          <th>{locStrings.docCount}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderStepsTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
