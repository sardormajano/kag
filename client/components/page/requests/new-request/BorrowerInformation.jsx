import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Textarea from '/client/components/fuc/Textarea'
import DatePicker from '/client/components/fuc/DatePicker'
import FileUpload from '/client/containers/fuc/FileUploadContainer'
import Select from 'react-select'

import LegalEntityForm from '/client/components/page/requests/new-request/borrowerInformation/LegalEntityForm'
import IndividualPersonForm from '/client/components/page/requests/new-request/borrowerInformation/IndividualPersonForm'

function addSpaces(str) {
  if(!str || str === 'null')
    return ''

  str = str.trim().split(' ').join('')
  str = str.replace(/[^\d]/g, '')
  str = parseInt(str).toLocaleString('ru-RU')

  return str
}

function spacedStringToNum(str) {
  return parseInt(str.replace(/\s/g, ''))
}

export default class BorrowerInformation extends Component {

  render() {
    const { context } = this.props

    return (
      <section
        role="tabpanel"
        aria-labelledby="wizard-vertical-h-0"
        className="body current"
        aria-hidden="false"
        style={ {
          display: this.props.activeStep === 1 ? '' : 'none'
        } }
      >
        <div className="row">
          <div id="buttons-div">
            <h4 className="m-t-0 header-title text-center">
              <b>Заемщиком является</b>
            </h4>
            <div className="row m-t-20 text-center">
              <div className="col-md-6 col-md-offset-3">
                <div className="form-group">
                  <Select
                    options={[
                      { value: '1', label: 'Юридическое лицо' },
                      { value: '2', label: 'Физическое лицо' }
                    ]}
                    placeholder='Выберите тип заемщика'
                    onChange={ ({ value }) => context.setState({ biBorrowerType: value }) }
                    value={ context.state.biBorrowerType }
                  />
                </div>
              </div>
            </div>
            <LegalEntityForm
              context={ context }
            />
            <IndividualPersonForm
              context={ context }
            />
          </div>
        </div><br />
      </section>
    )
  }
}
