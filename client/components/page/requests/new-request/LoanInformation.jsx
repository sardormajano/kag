import React, { Component } from 'react'
import Scroll, {Element, scroller} from 'react-scroll';

import Input from '/client/components/fuc/Input'
import Textarea from '/client/components/fuc/Textarea'
import DatePicker from '/client/components/fuc/DatePicker'
import FileUpload from '/client/containers/fuc/FileUploadContainer'
import Select from 'react-select'
import NumericInput from 'react-numeric-input'

import SpecialPurpose from '/client/components/page/requests/new-request/loanInformation/SpecialPurpose'
import ProjectPlaceModal from '/client/components/page/requests/new-request/loanInformation/ProjectPlaceModal'
import Pledge from '/client/components/page/requests/new-request/loanInformation/Pledge'

function addSpaces(str) {
  if(!str || str === 'null')
    return ''

  str = str.trim().split(' ').join('')
  str = str.replace(/[^\d]/g, '')
  str = parseInt(str).toLocaleString('ru-RU')

  return str
}

function spacedStringToNum(str) {
  return parseInt(str.replace(/\s/g, ''))
}

function territoryIdToName(_id, territories) {
  return territories.find(t => t._id === _id).NameRus
}

export default class LoanInformation extends Component {
  render() {
    const { context } = this.props

    return (
      <section
        role="tabpanel"
        aria-labelledby="wizard-vertical-h-0"
        className="body current"
        aria-hidden="false"
        style={ {
          display: this.props.activeStep === 0 ? '' : 'none'
        } }
      >
        <h4 className="m-t-0 header-title">
          <b>Информация о кредите</b>
        </h4>
        <Element name="liCreditOrganization"></Element>
        <div className="row"
          style={ {
            display: context.isApplicationCreatedByKAG ? '' : 'none'
          } }
          >
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">МФО/КТ *</label>
              <Select
                placeholder="Выберите организацию"
                options={ context.creditOrganizationOptions }
                onChange={ ({ value }) => context.setState({ liCreditOrganization: value, liCreditOrganizationWarn: false }) }
                value={ context.state.liCreditOrganization }
                onBlur={() => context.state.liCreditOrganization ? '' : context.setState({liCreditOrganizationWarn: true})}
              />
              <div className="form-group has-error" role="alert" style={{display: context.state.liCreditOrganizationWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Валюта кредитования *</label>
              <Select
                options={ context.currencyOptions }
                onChange={ ({ value }) => context.setState({ liCurrency: value }) }
                value={ context.state.liCurrency }
              />
            </div>
          </div>
          <Element name="liProductType"></Element>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Вид продукта *</label>
              <Select
                options={ context.productTypeOptions }
                onChange={ ({ value }) => context.setState({ liProductType: value, liProductTypeWarn: false }) }
                value={ context.state.liProductType }
                onBlur={() => context.state.liProductType ? '' : context.setState({liProductTypeWarn: true})}
              />
              <div className="form-group has-error" role="alert" style={{display: context.state.liProductTypeWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <Element name="liLoanProgram"></Element>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Программа кредитования *</label>
              <Select
                options={ context.loanProgramOptions }
                onChange={ ({ value }) => context.setState({ liLoanProgram: value, liLoanProgramWarn: false }) }
                value={ context.state.liLoanProgram }
                onBlur={() => context.state.liLoanProgram ? '' : context.setState({liLoanProgramWarn: true})}
              />
              <div className="form-group has-error" role="alert" style={{display: context.state.liLoanProgramWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
        </div><br />
        <div className="row">
          <Element name="liPaymentOrderPercent"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Порядок погашения % *</label>
              <Select
                options={ context.paymentOrderOptions }
                onChange={ ({ value }) => context.setState({ liPaymentOrderPercent: value, liPaymentOrderPercentWarn: false }) }
                value={ context.state.liPaymentOrderPercent }
                onBlur={() => context.state.liPaymentOrderPercent ? '' : context.setState({liPaymentOrderPercentWarn: true})}
              />
              <div className="form-group has-error" style={{display: context.state.liPaymentOrderPercentWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <Element name="liPaymentOrderLoan"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Порядок погашения ОД *</label>
              <Select
                options={ context.paymentOrderOptions }
                onChange={ ({ value }) => context.setState({ liPaymentOrderLoan: value, liPaymentOrderLoanWarn: false }) }
                value={ context.state.liPaymentOrderLoan }
                onBlur={() => context.state.liPaymentOrderLoan ? '' : context.setState({liPaymentOrderLoanWarn: true})}
              />
              <div className="form-group has-error" style={{display: context.state.liPaymentOrderLoanWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <Element name="liGracePeriodPercent"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Льготный период по %-там *</label>
              <NumericInput
                onChange={ val => context.setState({ liGracePeriodPercent: val }) }
                onBlur={() => !context.state.liGracePeriodPercent ? context.setState({liGracePeriodPercentWarn : true}) : context.setState({liGracePeriodPercentWarn : false})}
                value={ context.state.liGracePeriodPercent ? context.state.liGracePeriodPercent : 0 }
                placeholder="период в месяцах"
                className="form-control"
                style={ false }
                min={ 0 }
              />
              <div className="form-group has-error" style={{display: context.state.liGracePeriodPercentWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <Element name="liGracePeriodLoan"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Льготный период по ОД *</label>
              <NumericInput
                onChange={ val => context.setState({ liGracePeriodLoan: val }) }
                onBlur={() => !context.state.liGracePeriodLoan ? context.setState({liGracePeriodLoanWarn : true}) : context.setState({liGracePeriodLoanWarn : false})}
                value={ context.state.liGracePeriodLoan ? context.state.liGracePeriodLoan : 0 }
                placeholder="период в месяцах"
                className="form-control"
                style={ false }
                min={ 0 }
              />
              <div className="form-group has-error" style={{display: context.state.liGracePeriodLoanWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
        </div><br />
        <div className="row">
          <Element name="liMicrocreditSum"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Сумма микрокредита *</label>
              <input
                className="form-control"
                placeholder="введите сумму"
                onChange={ e => {
                  let val = e.currentTarget.value
                  val = addSpaces(val)
                  context.setState({ liMicrocreditSum: val, liMicrocreditSumWarn: false })
                }}
                onBlur={() => context.state.liMicrocreditSum ? '' : context.setState({liMicrocreditSumWarn: true})}
                value={ context.state.liMicrocreditSum }
              />
              <div className="form-group has-error" style={{display: context.state.liMicrocreditSumWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <Element name="liInterestRate"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Ставка вознаграждения *</label>
              <NumericInput
                onChange={ val => context.setState({ liInterestRate: val }) }
                onBlur={() => !context.state.liInterestRate ? context.setState({liInterestRateWarn : true}) : context.setState({liInterestRateWarn : false})}
                value={ context.state.liInterestRate }
                placeholder="введите ставку вознаграждения"
                className="form-control"
                style={ false }
                min={ 0 }
                max={ 100 }
                format={ num => {
                  return `${ num }%`
                }}
              />
              <div className="form-group has-error" style={{display: context.state.liInterestRateWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <Element name="liTerm"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Срок микрокредита *</label>
              <NumericInput
                onChange={ val => {
                  val = addSpaces('' + val)
                  context.setState({ liTerm: val })
                } }
                onBlur={() => !context.state.liTerm ? context.setState({liTermWarn : true}) : context.setState({liTermWarn : false})}
                value={ context.state.liTerm }
                placeholder="срок в месяцах"
                className="form-control"
                style={ false }
              />
              <div className="form-group has-error" style={{display: context.state.liTermWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <Element name="liGuaranteeAmount"></Element>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Размер гарантии *</label>
              <input
                className="form-control"
                placeholder="введите размер гарантии"
                onChange={ e => {
                  let val = e.currentTarget.value
                  if(spacedStringToNum(val) > spacedStringToNum(context.state.liMicrocreditSum))
                    return

                  val = addSpaces(val)
                  context.setState({ liGuaranteeAmount: val, liGuaranteeAmountWarn: false })
                }}
                onBlur={() => context.state.liGuaranteeAmount ? '' : context.setState({liGuaranteeAmountWarn: true})}
                value={ context.state.liGuaranteeAmount }
              />
              <div className="form-group has-error" style={{display: context.state.liGuaranteeAmountWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
        </div><br />
        <div className="row">
          <Element name="liOwnfunds"></Element>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Собственные средства *</label>
              <input
                className="form-control"
                placeholder="введите сумму"
                onChange={ e => {
                  let val = e.currentTarget.value
                  val = addSpaces(val)
                  context.setState({ liOwnfunds: val, liOwnfundsWarn: false })
                }}
                onBlur={() => context.state.liOwnfunds ? '' : context.setState({liOwnfundsWarn: true})}
                value={ context.state.liOwnfunds }
              />
              <div className="form-group has-error" style={{display: context.state.liOwnfundsWarn ? '' : 'none'}}>
                <label className="control-label">* Обязательное поле</label>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Дата поступления заявки в МФО/КТ</label>
              <DatePicker
                context={ context }
                stateName='liRequestAdmissionDate'
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Номер договора займа</label>
              <Input
                className='form-control'
                placeholder='номер договора: 12/57-ВАД56'
                context={ context }
                stateName='liLoanAgreementNumber'
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Дата договора займа</label>
              <DatePicker
                context={ context }
                stateName='liLoanAgreementDate'
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <SpecialPurpose
            context={ context }
            stateName='liSpecialPurpose'
          />
        </div><br />
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Цель кредитования</label>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder="опишите цель кредитования"
                context={ context }
                stateName='liLendingPurpose'
              />
            </div>
          </div>
        </div>
        <div className="row">
          <Pledge
            context={ context }
            stateName='liPledges'
          />
        </div>
        <br /><br />
        <h4 className="m-t-0 header-title">
          <b>Информация о проекте</b>
        </h4>
        <div className="row">
          <div className="col-md-6">
           <div className="form-group">
             <label htmlFor="field-1" className="control-label">Наименование проекта</label>
             <Input
               className='form-control'
               placeholder='введите наименование проекта'
               context={ context }
               stateName='liProjectName'
             />
           </div>
          </div>
          <div className="col-md-6">
           <div className="form-group">
             <label htmlFor="field-1" className="control-label">Место реализации проекта</label>
             <div className="input-group">
               <input
                 className="form-control"
                 placeholder="Нажмите кнопку Выбрать"
                 value={ context.state.liProjectPlace.nameRu ? context.state.liProjectPlace.nameRu : '' }
                 readOnly
               />
               <span className="input-group-btn">
                 <button
                   type="button"
                   className="btn waves-effect waves-light bg-custom"
                   style={{color: '#ffffff'}}
                   data-toggle="modal"
                   data-target="#create-place-modal"
                 >
                   Выбрать
                 </button>
               </span>
             </div>
           </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Цель проекта</label>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder="опишите цель проекта"
                context={ context }
                stateName='liProjectPurpose'
              />
            </div>
          </div>
        </div><br />
        <ProjectPlaceModal
          id="create-place-modal"
          context={ context }
        />
      </section>
    )
  }
}
