import React, { Component } from 'react'

import Textarea from '/client/components/fuc/Textarea'
import DatePicker from '/client/components/fuc/DatePicker'
import Confirm from 'react-confirm-bootstrap'
import CurrencyInput from '/client/components/fuc/CurrencyInput'
import NumericInput from 'react-numeric-input'
import Input from '/client/components/fuc/Input'

import NewIndividualPersonModal from '/client/components/page/requests/new-request/loanInformation/pledge/NewIndividualPersonModal'
import NewLegalEntityModal from '/client/components/page/requests/new-request/loanInformation/pledge/NewLegalEntityModal'

import EditIndividualPersonModal from '/client/components/page/requests/new-request/loanInformation/pledge/EditIndividualPersonModal'
import EditLegalEntityModal from '/client/components/page/requests/new-request/loanInformation/pledge/EditLegalEntityModal'

export default class Pledge extends Component {
  renderPledges() {
    const { context, stateName } = this.props

    return context.state[stateName].map((item, index) => (
      <tr key={ index }>
        <th scope="row">{ index + 1 }</th>
        <td>{ item.subject }</td>
        <td className="text-center">{ item.estimatedCost }</td>
        <td className="text-center">{ item.estimationDate }</td>
        <td className="text-center">
          { item.pledgeOwner ? item.pledgeOwner.name : (
          <div className="btn-group m-b-10">
            <button
              title="нажмите для того чтобы добавить информацию о залогодателе"
              type="button"
              className="btn waves-effect waves-light btn-primary dropdown-toggle btn-sm"
              data-toggle="dropdown"
            >
              Добавить залогодателя{' '}
              <span className="caret"></span>
            </button>
            <ul className="dropdown-menu">
                <li>
                  <a
                    className="btn-sm"
                    href="#"
                    data-toggle="modal"
                    data-target="#new-legal-entity-modal"
                    onClick={ () => context.setState({ liActivePledge: item }) }
                  >
                    Юр. лицо
                  </a>
                </li>
                <li>
                  <a
                    className="btn-sm"
                    href="#"
                    data-toggle="modal"
                    data-target="#new-individual-person-modal"
                    onClick={ () => context.setState({ liActivePledge: item }) }
                  >
                    Физ. лицо
                  </a>
                </li>
            </ul>
          </div>
        ) } {' '}
        { item.pledgeOwner && (
          <a
            href="#"
            className="on-default edit-row"
            title="редактировать информацию о залогодателе"
            data-toggle="modal"
            onClick={
              item.pledgeOwner.type === 'ip'
                ? context.ipPreUpdate.bind(context, item)
                : context.lePreUpdate.bind(context, item)
            }
            data-target={
              item.pledgeOwner.type === 'ip'
                ? '#edit-individual-person-modal'
                : '#edit-legal-entity-modal' }
          >
            <i className="fa fa-pencil" />
          </a>
        ) }
      </td>
        <td className="actions text-center">
          <a
            href="#"
            className="on-default text-primary"
            title="разрешения"
            data-toggle="modal"
            data-target="#edit-pledge-modal"
            onClick={e => {
              e.preventDefault()
              context.setState({
                pemSubject: item.subject,
                pemPlace: item.place,
                pemPledgeCost: item.pledgeCost,
                pemEstimatedCost: item.estimatedCost,
                pemEstimationDate: item.estimationDate,
                pemKl: item.kl,
                pemCoverageShare: item.coverageShare,
                pemRelation: item.relation,
                pemOtherInfo: item.otherInfo,
                pemIndex: index,
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>{' '}
          <Confirm
            onConfirm={ () => context.deletePledge(item) }
            body="Вы уверены, что хотите удалить залоговое обеспечение?"
            confirmText="Да"
            cancelText="Отмена"
            title="Удаление целевого назначения"
          >
              <a
                className="on-default remove-row text-danger"
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context } = this.props
    const {
      pcmSubject,
      pcmPlace,
      pcmPledgeCost,
      pcmEstimatedCost,
      pcmEstimationDate,
      pcmKl,
      pcmCoverageShare,
      pcmRelation,
      pemSubject,
      pemPlace,
      pemPledgeCost,
      pemEstimatedCost,
      pemEstimationDate,
      pemKl,
      pemCoverageShare,
      pemRelation
    } = context.state
    const allFieldsFilledOut = pcmSubject && pcmPlace && pcmPledgeCost && pcmEstimatedCost && pcmEstimationDate && pcmKl && pcmCoverageShare && pcmRelation ||
                               pemSubject && pemPlace && pemPledgeCost && pemEstimatedCost && pemEstimationDate && pemKl && pemCoverageShare && pemRelation
    return (
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Залоговое обеспечение</label>{ ' ' }
              <a
                className="btn btn-default waves-effect waves-light btn-xs"
                data-toggle="modal"
                data-target="#create-pledge-modal"
                title="нажмите для добавления информации о залоговом имуществе"
              >
                <i className="fa fa-plus m-r-5" />
                <span>добавить залог</span>
              </a>
              <p className="text-muted font-13">
                Для добавления информации о залоговом обеспечении необходимо в первую очередь добавить информацию о залоговом имуществе.
                Для этого нажмите кнопку <strong>+добавить залог</strong> и заполните форму.
                После добавления информации о залоге будет необходимо добавить информацию о залогодателе.
                Для этого после добавления информации о залоге нажмите кнопку <strong>+добавить</strong> в таблице, выберите соответствующий тип залогодателя и заполните поля.
              </p>
              <div className="table-responsive">
                <table
                  className="table m-0"
                >
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Предмет залога</th>
                      <th>Оц. стоимость</th>
                      <th>Дата оценки</th>
                      <th className="text-center">Залогодатель</th>
                      <th className="text-center">Действия</th>
                    </tr>
                  </thead>
                  <tbody>
                    { this.renderPledges() }
                  </tbody>
                </table>
                <br /><br /><br />
              </div>
            </div>
          </div>
        </div>
        <div
          id="create-pledge-modal"
          className="modal fade"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title">Добавление залогового обеспечения</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="pledge-thing" className="control-label">Предмет залога *</label>
                      <Input
                        className='form-control'
                        placeholder='Жилой дом'
                        context={ context }
                        stateName='pcmSubject'
                      />
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="pledge-location" className="control-label">Место расположения *</label>
                      <Input
                        className='form-control'
                        placeholder='Введите название'
                        context={ context }
                        stateName='pcmPlace'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="pledge-cost" className="control-label">Залоговая стоимость *</label>
                      <CurrencyInput
                        placeholder="В тенге"
                        className="form-control"
                        stateName='pcmPledgeCost'
                        context={ context }
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Оценочная стоимость *</label>
                      <CurrencyInput
                        placeholder="В тенге"
                        className="form-control"
                        stateName='pcmEstimatedCost'
                        context={ context }
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="assessment-date" className="control-label">Дата оценки *</label>
                      <DatePicker
                        context={ context }
                        stateName='pcmEstimationDate'
                      />
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">КЛ *</label>
                      <Input
                        className='form-control'
                        placeholder=''
                        context={ context }
                        stateName='pcmKl'
                      />
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Доля покрытия *</label>
                      <Input
                        className='form-control'
                        placeholder=''
                        context={ context }
                        stateName='pcmCoverageShare'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Связь залогодателя с заемщиком *</label>
                      <Input
                        className='form-control'
                        placeholder=''
                        context={ context }
                        stateName='pcmRelation'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Иная информация по залогу</label>
                      <Textarea
                        className="form-control"
                        rows={4}
                        maxLength={500}
                        placeholder=""
                        context={ context }
                        stateName='pcmOtherInfo'
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="alert alert-warning" role="alert" style={{display: !allFieldsFilledOut ? '' : 'none'}}>
                Необходимо заполнить поля помеченные звездочкой для продолжения ( * )
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-default waves-effect"
                  data-dismiss="modal"
                  ref={ closeButton => this.closeButton = closeButton }
                >
                  Закрыть
                </button>
                <button
                  type="button"
                  data-dismiss="modal"
                  disabled={!allFieldsFilledOut}
                  className="btn btn-info waves-effect waves-light"
                  onClick={ context.addPledge.bind(context) }
                >Сохранить</button>
              </div>
            </div>
          </div>
        </div>
        <div
          id="edit-pledge-modal"
          className="modal fade"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title">Редактирование залогового обеспечения</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="pledge-thing" className="control-label">Предмет залога *</label>
                      <Input
                        className='form-control'
                        placeholder='Жилой дом'
                        context={ context }
                        stateName='pemSubject'
                      />
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="pledge-location" className="control-label">Место расположения *</label>
                      <Input
                        className='form-control'
                        placeholder='Введите название'
                        context={ context }
                        stateName='pemPlace'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="pledge-cost" className="control-label">Залоговая стоимость *</label>
                      <CurrencyInput
                        placeholder="В тенге"
                        className="form-control"
                        stateName='pemPledgeCost'
                        context={ context }
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Оценочная стоимость *</label>
                      <CurrencyInput
                        placeholder="В тенге"
                        className="form-control"
                        stateName='pemEstimatedCost'
                        context={ context }
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="assessment-date" className="control-label">Дата оценки *</label>
                      <DatePicker
                        context={ context }
                        stateName='pemEstimationDate'
                      />
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">КЛ *</label>
                      <Input
                        className='form-control'
                        placeholder=''
                        context={ context }
                        stateName='pemKl'
                      />
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Доля покрытия *</label>
                      <Input
                        className='form-control'
                        placeholder=''
                        context={ context }
                        stateName='pemCoverageShare'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Связь залогодателя с заемщиком *</label>
                      <Input
                        className='form-control'
                        placeholder=''
                        context={ context }
                        stateName='pemRelation'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="assessed-value" className="control-label">Иная информация по залогу</label>
                      <Textarea
                        className="form-control"
                        rows={4}
                        maxLength={500}
                        placeholder=""
                        context={ context }
                        stateName='pemOtherInfo'
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="alert alert-warning" role="alert" style={{display: !allFieldsFilledOut ? '' : 'none'}}>
                Необходимо заполнить поля помеченные звездочкой для продолжения ( * )
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-default waves-effect"
                  data-dismiss="modal"
                  ref={ closeButton => this.closeButton = closeButton }
                >
                  Закрыть
                </button>
                <button
                  type="button"
                  data-dismiss="modal"
                  disabled={!allFieldsFilledOut}
                  className="btn btn-primary waves-effect waves-light"
                  onClick={ context.updatePledge.bind(context) }
                >Сохранить</button>
              </div>
            </div>
          </div>
        </div>
        <NewIndividualPersonModal
          id="new-individual-person-modal"
          context={ context }
        />
        <NewLegalEntityModal
          id="new-legal-entity-modal"
          context={ context }
        />
        <EditIndividualPersonModal
          id="edit-individual-person-modal"
          context={ context }
        />
        <EditLegalEntityModal
          id="edit-legal-entity-modal"
          context={ context }
        />
      </div>
    )
  }
}
