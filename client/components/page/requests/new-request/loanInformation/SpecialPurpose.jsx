import React, { Component } from 'react'

import Confirm from 'react-confirm-bootstrap'
import NumericInput from 'react-numeric-input'
import Input from '/client/components/fuc/Input'

export default class SpecialPurpose extends Component {
  renderPurposes() {
    const { context, stateName } = this.props

    return context.state[stateName].map((item, index) => (
      <tr key={ index }>
        <th scope="row">{ index + 1 }</th>
        <td>{ item.name }</td>
        <td className="text-center">{ item.price }</td>
        <td className="text-center">{ item.number }</td>
        <td className="text-center">{ item.unit }</td>
        <td className="actions text-center">
          <a
            href="#"
            className="on-default text-primary"
            title="разрешения"
            data-toggle="modal"
            data-target="#edit-special-purpose"
            onClick={e => {
              e.preventDefault()
              context.setState({
                spemName: item.name,
                spemPrice: item.price,
                spemUnit: item.unit,
                spemNumber: item.number,
                spemIndex: index,
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>{' '}
          <Confirm
            onConfirm={ () => context.deleteSpecialPurpose(item) }
            body="Вы уверены, что хотите удалить целевое назначение?"
            confirmText="Да"
            cancelText="Отмена"
            title="Удаление целевого назначения"
          >
              <a
                className="on-default remove-row text-danger"
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context } = this.props
    const allFieldsFilledOut = context.state.spcmName || context.state.spemName

    return (
      <div>
        <div className="col-md-12">
          <div className="form-group">
            <label className="control-label">Целевое назначение</label>{' '}
            <a
              className="btn btn-default waves-effect waves-light btn-xs"
              data-toggle="modal"
              data-target="#create-special-purpose"
            >
              <i className="fa fa-plus m-r-5" />
              <span>добавить запись</span>
            </a>
            <div className="table-responsive">
              <table className="table m-0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Наименование</th>
                    <th className="text-center">Стоимость</th>
                    <th className="text-center">Количество</th>
                    <th className="text-center">Ед.измерения</th>
                    <th className="text-center">Действия</th>
                  </tr>
                </thead>
                <tbody>
                  { this.renderPurposes() }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div
          id="create-special-purpose"
          className="modal fade"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="myModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title">Добавить целевое назначение</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-thing" className="control-label">Наименование</label>
                      <Input
                        type="text"
                        className="form-control"
                        context={ context }
                        stateName='spcmName'
                      />
                      <div className="form-group has-error" role="alert" style={{display: !allFieldsFilledOut ? '' : 'none'}}>
                        <label className="control-label">* Обязательное поле</label>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-cost" className="control-label">Стоимость</label>
                      <Input
                        type="number"
                        className="form-control"
                        context={ context }
                        stateName='spcmPrice'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-cost" className="control-label">Ед.измерения</label>
                      <Input
                        type="text"
                        className="form-control"
                        context={ context }
                        stateName='spcmUnit'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-amount" className="control-label">Количество</label>
                      <Input
                        type="number"
                        className="form-control"
                        context={ context }
                        stateName='spcmNumber'
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Закрыть</button>
                <button
                  data-dismiss="modal"
                  type="button"
                  disabled={!allFieldsFilledOut}
                  className="btn btn-info waves-effect waves-light"
                  onClick={ context.addSpecialPurpose.bind(context) }
                >
                  Сохранить
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          id="edit-special-purpose"
          className="modal fade"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="myModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title">Редактировать информацию о залоге</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-thing" className="control-label">Наименование</label>
                      <Input
                        type="text"
                        className="form-control"
                        context={ context }
                        stateName='spemName'
                      />
                      <div className="form-group has-error" role="alert" style={{display: !context.state.spemName ? '' : 'none'}}>
                        <label className="control-label">* Обязательное поле</label>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-cost" className="control-label">Стоимость</label>
                      <Input
                        type="number"
                        className="form-control"
                        context={ context }
                        stateName='spemPrice'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-cost" className="control-label">Ед.измерения</label>
                      <Input
                        type="text"
                        className="form-control"
                        context={ context }
                        stateName='spemUnit'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="purpose-amount" className="control-label">Количество</label>
                      <Input
                        type="number"
                        className="form-control"
                        context={ context }
                        stateName='spemNumber'
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Закрыть</button>
                <button
                  data-dismiss="modal"
                  type="button"
                  disabled={!allFieldsFilledOut}
                  className="btn btn-info waves-effect waves-light"
                  onClick={ context.updateSpecialPurpose.bind(context) }
                >
                  Сохранить
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
