import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import InputMask from 'react-input-mask'
import Select from 'react-select'
import DatePicker from '/client/components/fuc/DatePicker'

export default class EditIndividualPersonModal extends Component {
  render() {
    const { context, ...rest } = this.props

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >
                Редактировать физ. лицо {' '}
                <i
                  className="fa fa-spin fa-circle-o-notch text-info"
                  style={{ display: context.ipemLoading ? '' : 'none' }}
                />
              </h4>
            </div>
            <div className="modal-body" style={{ zIndex: 2 }}>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">ИИН</label>
                    <Input
                      className="form-control"
                      context={ context }
                      stateName='ipemIin'
                      onBlur={ context.eipmIinBlurHandler.bind(context) }
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">ФИО</label>
                    <Input
                      className="form-control"
                      context={ context }
                      stateName='ipemFullName'
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor="director-name" className="control-label">Дата рождения</label>
                    <DatePicker
                      context={ context }
                      stateName='ipemBirthDate'
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">Контактный телефон</label>
                    <InputMask
                      className="form-control"
                      value={ context.state.ipemPhone }
                      onChange={ e => context.setState({ ipemPhone: e.currentTarget.value })}
                      mask="+7(999) 999-99-99"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                Закрыть
              </button>
              <button
                type="button"
                data-dismiss="modal"
                className="btn btn-primary waves-effect waves-light"
                onClick={ context.editIndividualPerson.bind(context) }
              >
                Сохранить
              </button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
