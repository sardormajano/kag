import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import InputMask from 'react-input-mask'
import Select from 'react-select'
import DatePicker from '/client/components/fuc/DatePicker'

export default class NewILegalEntityModal extends Component {
  render() {
    const { context, ...rest } = this.props

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >Добавить юр. лицо</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">ИИН/БИН</label>
                    <Input
                      className="form-control"
                      context={ context }
                      stateName='lecmBin'
                      onBlur={ context.nlemBinBlurHandler.bind(context) }
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">Краткое наименование заемщика</label>
                    <Input
                      className="form-control"
                      context={ context }
                      stateName='lecmShortName'
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label className="control-label">Организационно правовая форма</label>
                    <Select
                      options={ context.enterpreneurTypeOptions }
                      value={ context.state.lecmEnterpreneurType }
                      onChange={ ({ value }) => context.setState({ lecmEnterpreneurType: value }) }
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label className="control-label">Категория субъекта</label>
                    <Select
                      options={ context.subjectCategoryOptions }
                      value={ context.state.lecmSubjectCategory }
                      onChange={ ({ value }) => context.setState({ lecmSubjectCategory: value }) }
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label htmlFor className="control-label">Контактный телефон</label>
                    <InputMask
                      className="form-control"
                      value={ context.state.lecmPhone }
                      onChange={ e => context.setState({ lecmPhone: e.currentTarget.value })}
                      mask="+7(999) 999-99-99"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label htmlFor="director-name" className="control-label">Дата регистрации/перерегистрации</label>
                    <DatePicker
                      context={ context }
                      stateName='lecmRegistrationDate'
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                Закрыть
              </button>
              <button
                type="button"
                data-dismiss="modal"
                className="btn btn-primary waves-effect waves-light"
                onClick={ context.createLegalEntity.bind(context) }
              >
                Сохранить
              </button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
