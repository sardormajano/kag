import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Textarea from '/client/components/fuc/Textarea'
import DatePicker from '/client/components/fuc/DatePicker'
import FileUpload from '/client/containers/fuc/FileUploadContainer'
import Select from 'react-select'
import NumericInput from 'react-numeric-input'
import CurrencyInput from '/client/components/fuc/CurrencyInput'

import ActivitiesTable from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/ActivitiesTable'
import LegalAddress from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/LegalAddress'
import ActualAddress from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/ActualAddress'
import PartnerInfo from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/PartnerInfo'
import FoundersTable from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/FoundersTable'

function addSpaces(str) {
  if(!str || str === 'null')
    return ''

  str = str.trim().split(' ').join('')
  str = str.replace(/[^\d]/g, '')
  str = parseInt(str).toLocaleString('ru-RU')

  return str
}

function spacedStringToNum(str) {
  return parseInt(str.replace(/\s/g, ''))
}

function territoryIdToName(_id, territories) {
  return territories.find(t => t._id === _id).NameRus
}

export default class LegalEntityForm extends Component {

  render() {
    const { context } = this.props

    return (
      <section
        role="tabpanel"
        className="body current"
        style={{
          display: context.state.biBorrowerType === '1' ? '' : 'none',
          margin: '1em'
        }}
      >
        <h4 className="m-t-0 header-title">
          <b>Информация о заемщике - Юридическое лицо</b>
        </h4>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label className="control-label">ИИН/БИН *</label>
              <Input
                className='form-control'
                placeholder='введите ИИН или БИН'
                context={ context }
                stateName='biBin'
              />
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label className="control-label">Полное наименование заемщика *</label>
              <Input
                className='form-control'
                placeholder='Название организации'
                context={ context }
                stateName='biOrganizationName'
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-4" style={{ zIndex: 1 }}>
            <div className="form-group">
              <label className="control-label">Категории предпринимателя *</label>
              <Select
                options={ context.enterpreneurCategoryOptions }
                onChange={ ({ value }) => context.setState({ biEnterpreneurCategory: value }) }
                value={ context.state.biEnterpreneurCategory }
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Организационно Правовая Форма *</label>
              <Select
                options={ context.enterpreneurTypeOptions }
                onChange={ ({ value }) => context.setState({ biEnterpreneurType: value }) }
                value={ context.state.biEnterpreneurType }
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Категория субъекта *</label>
              <Select
                options={ context.subjectCategoryOptions }
                onChange={ ({ value }) => context.setState({ biSubjectCategory: value }) }
                value={ context.state.biSubjectCategory }
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-4" style={{ zIndex: 0 }}>
            <div className="form-group">
              <label className="control-label">Дата регистрации/перерегистрации *</label>
              <DatePicker
                context={ context }
                stateName='biRegistrationDate'
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <ActivitiesTable
            context={ context }
            stateName='biActivities'
          />
        </div><br /><br />
        <div className="row">
          <h5 className="m-t-0">
            Юридический адрес
          </h5>
          <LegalAddress
            context={ context }
          />
        </div>
        <div className="row">
          <h5 className="m-t-0">
            Фактический адрес
          </h5>
          <ActualAddress
            context={ context }
          />
        </div>
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Уставной капитал *</label>
              <CurrencyInput
                placeholder="В тенге"
                className="form-control"
                stateName='biCharterCapital'
                context={ context }
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Численность сотрудников *</label>
              <NumericInput
                onChange={ val => context.setState({ biEmployeesNumber: val }) }
                value={ context.state.biEmployeesNumber }
                className="form-control"
                style={ false }
                min={ 1 }
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">ФИО первого руководителя *</label>
              <Input
                className='form-control'
                placeholder='введите ИИН или БИН'
                context={ context }
                stateName='biHeadFullName'
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">ИИН первого руководителя *</label>
              <Input
                className='form-control'
                placeholder='Название организации'
                context={ context }
                stateName='biHeadIin'
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Дата рождения первого руководителя *</label>
              <DatePicker
                context={ context }
                stateName='biHeadBirthdate'
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Выберите образование *</label>
              <Select
                options={ context.educationOptions }
                onChange={ ({ value }) => context.setState({ biHeadEducation: value }) }
                value={ context.state.biHeadEducation }
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="form-group m-b-20">
            <label className="m-b-10">Семейное положение заемщика:</label>
            <br />
            <div className="radio radio-info radio-inline">
              <input
                type="radio"
                id="biBorrowerMarried"
                onChange={ () => context.setState({ biBorrowerMarried: true }) }
                checked={ context.state.biBorrowerMarried}
                value={ context.state.biBorrowerMarried ? 'on' : 'off'}
              />
              <label htmlFor="biBorrowerMarried"> Женат/Замужем </label>
            </div>
            <div className="radio radio-info radio-inline">
              <input
                type="radio"
                id="biBorrowerNotMarried"
                onChange={ () => context.setState({ biBorrowerMarried: false }) }
                checked={ !context.state.biBorrowerMarried }
                value={ context.state.biBorrowerMarried ? 'off' : 'on'}
              />
              <label htmlFor="biBorrowerNotMarried"> Не женат/Не замужем </label>
            </div>
          </div>
        </div>
        <div className="row">
          <PartnerInfo
            context={ context }
          />
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group" style={{ zIndex: 2 }}>
              <label className="control-label">Обслуживающие банки *</label>
              <Select
                options={ context.bankOptions }
                value={ context.state.biServingBanks }
                multi
                onChange={ values => context.setState({ biServingBanks: values }) }
              />
            </div>
          </div>
        </div>
        <div className="row">
          <FoundersTable
            stateName='biFounders'
            context={ context }
          />
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group m-b-20">
              <label className="m-b-10">Имеются ли задолженности перед НК:</label>
              <br />
              <div className="radio radio-info radio-inline">
                <input
                  type="radio"
                  id="biHasDebts"
                  onChange={ () => context.setState({ biHasDebts: true }) }
                  checked={ context.state.biHasDebts}
                  value={ context.state.biHasDebts ? 'on' : 'off'}
                />
                <label htmlFor="biHasDebts"> Да </label>
              </div>
              <div className="radio radio-info radio-inline">
                <input
                  type="radio"
                  id="biHasNoDebts"
                  onChange={ () => context.setState({ biHasDebts: false }) }
                  checked={ !context.state.biHasDebts }
                  value={ context.state.biHasDebts ? 'off' : 'on'}
                />
                <label htmlFor="biHasNoDebts"> Нет </label>
              </div>
            </div>
          </div>
          <div
            className="col-md-6"
            style={{ display: context.state.biHasDebts ? '' : 'none' }}
          >
            <div className="form-group">
              <label className="control-label">Сумма *</label>
              <NumericInput
                onChange={ val => context.setState({ biDebtSum: val }) }
                value={ context.state.biDebtSum }
                className="form-control"
                placeholder='введите сумму, если имеется задолженность'
                style={ false }
                min={ 0 }
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Иная важная информация</label>
              <p className="text-muted font-13">
                краткая информация из истории создания компании, происхождение капитала, основные этапы развития,
                изменения в структуре собственников, изменения в направлениях деятельности,
                созданию дочерних компании, филиалов, иная важная информация по усмотрению менеджера
              </p>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder=""
                context={ context }
                stateName='biImportantInformation'
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Имеются ли просроченные задолженности перед финансовыми организациями?</label>
              <p className="text-muted font-13">
                внесите наименование финансовой организации, сумму долга и количество просроченных дней.
              </p>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder=""
                context={ context }
                stateName='biDebtsInfo'
              />
            </div>
          </div>
        </div>
      </section>
    )
  }
}
