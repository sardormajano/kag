import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import DatePicker from '/client/components/fuc/DatePicker'
import Select from 'react-select'

export default class PartnerInfo extends Component {
  render() {
    const { context } = this.props

    return (
      <div
        style={{ display: context.state.biBorrowerMarried ? '' : 'none' }}
      >
        <div className="row">
          <div className="col-md-4">
           <div className="form-group">
             <label htmlFor="field-1" className="control-label">
               ФИО супруги
             </label>
             <Input
               className='form-control'
               placeholder='ФИО полностью'
               context={ context }
               stateName='biBorrowerPartnerFullName'
             />
           </div>
          </div>
          <div className="col-md-4">
           <div className="form-group">
             <label htmlFor="field-1" className="control-label">
               ИИН супруги
             </label>
             <Input
               className='form-control'
               placeholder='Введите ИИН'
               context={ context }
               stateName='biBorrowerPartnerIin'
             />
           </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
           <div className="form-group">
             <label htmlFor="field-1" className="control-label">
               Дата рождения супруги
             </label>
             <DatePicker
               placeholder='дд/мм/гггг'
               context={ context }
               stateName='biBorrowerPartnerBirthdate'
             />
           </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Образование супруги *</label>
              <Select
                options={ context.educationOptions }
                onChange={ ({ value }) => context.setState({ biBorrowerPartnerEducation: value }) }
                value={ context.state.biBorrowerPartnerEducation }
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Занятость супруги</label>
              <div className="checkbox checkbox-primary" style={{marginTop: 0}}>
                <input
                  type="checkbox"
                  id="hobby2"
                  onChange={ () => context.setState({ biBorrowerPartnerIsEmployed: !context.state.biBorrowerPartnerIsEmployed }) }
                  checked={ context.state.biBorrowerPartnerIsEmployed }
                />
                <label htmlFor="hobby2"> Трудоустроен/трудоустроена </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
