import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import NumericInput from 'react-numeric-input'
import InputMask from 'react-input-mask'
import Select from 'react-select'
import DatePicker from '/client/components/fuc/DatePicker'

import ActualAddress from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/foundersTable/ActualAddress'

export default class NewILegalEntityModal extends Component {
  render() {
    const { context, ...rest } = this.props

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >Добавить юр. лицо</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">ИИН/БИН</label>
                    <Input
                      className="form-control"
                      context={ context }
                      stateName='bileBin'
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">Краткое наименование заемщика</label>
                    <Input
                      className="form-control"
                      context={ context }
                      stateName='bileShortName'
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label className="control-label">Организационно правовая форма</label>
                    <Select
                      options={ context.enterpreneurTypeOptions }
                      value={ context.state.bileEnterpreneurType }
                      onChange={ ({ value }) => context.setState({ bileEnterpreneurType: value }) }
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label className="control-label">Категория субъекта</label>
                    <Select
                      options={ context.subjectCategoryOptions }
                      value={ context.state.bileSubjectCategory }
                      onChange={ ({ value }) => context.setState({ bileSubjectCategory: value }) }
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">Контактный телефон</label>
                    <InputMask
                      className="form-control"
                      value={ context.state.bilePhone }
                      onChange={ e => context.setState({ bilePhone: e.currentTarget.value })}
                      mask="+7(999) 999-99-99"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor="director-name" className="control-label">Дата регистрации/перерегистрации</label>
                    <DatePicker
                      context={ context }
                      stateName='bileRegistrationDate'
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor className="control-label">Доля, %</label>
                    <NumericInput
                      onChange={ val => context.setState({ bileShare: val }) }
                      value={ context.state.bileShare }
                      className="form-control"
                      style={ false }
                      min={ 0 }
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <ActualAddress
                prefix='bile'
                context={ context }
              />
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="form-group">
                  <label htmlFor className="control-label">Продолжение адреса</label>
                  <Input
                    className="form-control"
                    context={ context }
                    stateName='bileFounderAddressExtra'
                    placeholder='улица, номер дома, квартира, индекс'
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                Закрыть
              </button>
              <button
                type="button"
                data-dismiss="modal"
                className="btn btn-primary waves-effect waves-light"
                onClick={ context.bileFounderSaveHandler.bind(context) }
              >
                Сохранить
              </button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
