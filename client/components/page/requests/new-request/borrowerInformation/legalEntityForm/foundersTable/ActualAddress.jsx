import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'

export default class ActualAddress extends Component {
  renderActualAddressSelects() {
    const { context, prefix } = this.props

    return context.state[`${prefix}FounderAddresses`].map((actualAddress, index) => (
      context[`${prefix}FounderTerritoriesOptions`][index].length ? (
        <div className="row m-t-20" key={index}>
          <div className="col-md-12">
            <Select
              options={ context[`${prefix}FounderTerritoriesOptions`][index] }
              value={ context.state[`${prefix}FounderAddresses`][index] }
              onChange={ ({ value }) => context[`${prefix}FounderAddressSelectHandler`](index, value) }
            />
          </div>
        </div>
      ) : (
        <div  key={index}/>
      )
    ))
  }

  render() {
    const { context, ...rest } = this.props

    return (
      <div className="col-md-12">
        <div className="form-group">
          <h4>Выберите местонахождение</h4>
          <div style={{ zIndex: 2 }}>
            { this.renderActualAddressSelects() }
          </div>
        </div>
      </div>
    )
  }
}
