import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'

export default class ActualAddressModal extends Component {
  renderActualAddressSelects() {
    const { context } = this.props
    const { biActualAddresses } = context.state

    return biActualAddresses.map((actualAddress, index) => (
      context.actualTerritoriesOptions[index].length ? (
        <div className="row m-t-20" key={index}>
          <div className="col-md-12">
            <Select
              options={ context.actualTerritoriesOptions[index] }
              value={ context.state.biActualAddresses[index] }
              onChange={ ({ value }) => context.actualAddressSelectHandler(index, value) }
            />
          </div>
        </div>
      ) : (
        <div  key={index}/>
      )
    ))
  }

  render() {
    const { context, ...rest } = this.props

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >Выберите местонахождение</h4>
            </div>
            <div className="modal-body" style={{ zIndex: 2 }}>
              { this.renderActualAddressSelects() }
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                Закрыть
              </button>
              <button
                disabled={ !context.state.biActualAddressSavable }
                type="button"
                data-dismiss="modal"
                className="btn btn-primary waves-effect waves-light"
                onClick={ context.actualAddressSaveHandler.bind(context) }
              >
                Сохранить
              </button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
