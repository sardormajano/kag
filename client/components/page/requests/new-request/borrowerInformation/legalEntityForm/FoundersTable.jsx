import React, { Component } from 'react'

import Confirm from 'react-confirm-bootstrap'
import NumericInput from 'react-numeric-input'
import Input from '/client/components/fuc/Input'

import NewIndividualPersonModal from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/foundersTable/NewIndividualPersonModal'
import NewLegalEntityModal from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/foundersTable/NewLegalEntityModal'

export default class FoundersTable extends Component {
  renderFounders() {
    const { context, stateName } = this.props

    return context.state[stateName].map((item, index) => (
      <tr key={ index }>
        <th scope="row">{ index + 1 }</th>
        <td>{ item.type === 'le' ? item.bin : item.iin }</td>
        <td>{ item.type === 'le' ? item.shortName : item.fullName }</td>
        <td>{ item.type === 'le' ? item.registrationDate : item.birthDate }</td>
        <td>{ item.address.nameRu }</td>
        <td>{ item.share }%</td>
        <td className="actions text-center">
          <Confirm
            onConfirm={ () => context.deleteFounder(item) }
            body="Вы уверены, что хотите удалить учредителя?"
            confirmText="Да"
            cancelText="Отмена"
            title="Удаление учредителя"
          >
              <a
                className="on-default remove-row text-danger"
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context } = this.props

    return (
      <div>
        <div className="col-md-12">
          <div className="form-group">
            <label className="control-label">Учредители юридического лица заемщика *</label>{' '}
            <br />
            <a
              className="btn btn-purple btn-xs m-t-10"
              data-toggle="modal"
              data-target="#add-legal-entity-modal"
            >
              <i className="fa fa-plus m-r-5" />
              <span>учредитель Ю.Л.</span>
            </a>{' '}
            <a
              className="btn btn-purple btn-xs m-t-10"
              data-toggle="modal"
              data-target="#add-individual-person-modal"
            >
              <i className="fa fa-plus m-r-5" />
              <span>учредитель Ф.Л.</span>
            </a><br />
            <p className="text-muted font-13 m-t-10">
                Введите информацию об учредителях юридического лица с указанием доли в уставном капитале.
                Общая сумма долей должна быть равна 100. <br />
                Для внесения информации об учредителе который является юридическим лицом нажмите <strong>+учредитель Ю.Л.</strong> и заполните форму. <br />
                Для внесения информации об учредителе который является физическим лицом нажмите <strong>+учредитель Ф.Л.</strong> и заполните форму.
            </p><br />
            <div className="table-responsive">
              <table className="table m-0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ИИН/БИН</th>
                    <th>ФИО/Наименование</th>
                    <th>Дата рождения/Регистрации</th>
                    <th>Адрес</th>
                    <th>Доля,%</th>
                    <th>Действия</th>
                  </tr>
                </thead>
                <tbody>
                  { this.renderFounders() }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <NewIndividualPersonModal
          id="add-individual-person-modal"
          context={ context }
        />
        <NewLegalEntityModal
          id="add-legal-entity-modal"
          context={ context }
        />
      </div>
    )
  }
}
