import React, { Component } from 'react'

import ActivityModal from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/ActivityModal'

import Confirm from 'react-confirm-bootstrap'
import NumericInput from 'react-numeric-input'
import Input from '/client/components/fuc/Input'

function activityIdToName(_id) {
  return _id.nameRu
}

export default class ActivitiesTable extends Component {
  renderActivities() {
    const { context, stateName } = this.props

    return context.state[stateName].map((item, index) => (
      <tr key={ index }>
        <th scope="row">{ index + 1 }</th>
        <td>{ activityIdToName(item, context.props.activities) }</td>
        <td className="actions text-center">
          <Confirm
            onConfirm={ context.deleteActivityHandler.bind(context, item) }
            body="Вы уверены, что хотите удалить вид деятельности?"
            confirmText="Да"
            cancelText="Отмена"
            title="Удаление вида деятельности"
          >
              <a
                className="on-default remove-row text-danger"
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context } = this.props

    return (
      <div>
        <div className="col-md-12">
          <div className="form-group">
            <label className="control-label">Виды деятельности</label>{' '}
            <a
              className="btn btn-default waves-effect waves-light btn-xs"
              data-toggle="modal"
              data-target="#create-activity-modal"
            >
              <i className="fa fa-plus m-r-5" />
              <span>добавить запись</span>
            </a>
            <div className="table-responsive">
              <table className="table m-0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Наименование</th>
                    <th className="text-center">Действия</th>
                  </tr>
                </thead>
                <tbody>
                  { this.renderActivities() }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <ActivityModal
          id="create-activity-modal"
          context={ context }
        />
      </div>
    )
  }
}
