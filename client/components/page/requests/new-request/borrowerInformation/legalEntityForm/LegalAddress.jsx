import React, { Component } from 'react'

import LegalAddressModal from '/client/components/page/requests/new-request/borrowerInformation/legalEntityForm/LegalAddressModal'
import Input from '/client/components/fuc/Input'

function territoryIdToName(_id, territories) {
  return territories.find(t => t._id === _id).NameRus
}

export default class LegalAddress extends Component {
  render() {
    const { context } = this.props

    return (
      <div>
        <div className="col-md-6">
         <div className="form-group">
           <label htmlFor="field-1" className="control-label">
             Населенный пункт
           </label>
           <div className="input-group">
             <input
               className="form-control"
               placeholder="Нажмите кнопку Выбрать"
               value={ context.state.biLegalAddress.nameRu ? context.state.biLegalAddress.nameRu : '' }
               readOnly
             />
             <span className="input-group-btn">
               <button
                 type="button"
                 className="btn waves-effect waves-light bg-custom"
                 style={{color: '#ffffff'}}
                 data-toggle="modal"
                 data-target="#create-legal-address-modal"
               >
                 Выбрать
               </button>
             </span>
           </div>
         </div>
        </div>
        <div className="col-md-6">
          <div className="form-group">
            <label className="control-label">Продолжение адреса *</label>
            <Input
              className='form-control'
              placeholder='улица, номер дома, квартира, индекс'
              context={ context }
              stateName='biLegalAddressExtra'
            />
          </div>
        </div>
        <LegalAddressModal
          context={ context }
          id='create-legal-address-modal'
        />
      </div>
    )
  }
}
