import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'

export default class IndividualPersonAddressModal extends Component {
  renderIndividualPersonAddressSelects() {
    const { context } = this.props
    const { biipfIndividualPersonAddresses } = context.state

    return biipfIndividualPersonAddresses.map((individualPersonAddress, index) => (
      context.individualPersonTerritoriesOptions[index].length ? (
        <div className="row m-t-20" key={index}>
          <div className="col-md-12">
            <Select
              options={ context.individualPersonTerritoriesOptions[index] }
              value={ context.state.biipfIndividualPersonAddresses[index] }
              onChange={ ({ value }) => context.individualPersonAddressSelectHandler(index, value) }
            />
          </div>
        </div>
      ) : (
        <div  key={index}/>
      )
    ))
  }

  render() {
    const { context, ...rest } = this.props

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >Выберите местонахождение</h4>
            </div>
            <div className="modal-body" style={{ zIndex: 2 }}>
              { this.renderIndividualPersonAddressSelects() }
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                Закрыть
              </button>
              <button
                disabled={ !context.state.biipfIndividualPersonSavable }
                type="button"
                data-dismiss="modal"
                className="btn btn-primary waves-effect waves-light"
                onClick={ context.individualPersonAddressSaveHandler.bind(context) }
              >
                Сохранить
              </button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
