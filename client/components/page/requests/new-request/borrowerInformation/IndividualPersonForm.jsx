import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import InputMask from 'react-input-mask'
import Textarea from '/client/components/fuc/Textarea'
import DatePicker from '/client/components/fuc/DatePicker'
import Select from 'react-select'

import IndividualPersonAddress from '/client/components/page/requests/new-request/borrowerInformation/individualPersonForm/IndividualPersonAddress'

export default class IndividualPersonForm extends Component {

  render() {
    const { context } = this.props

    return (
      <section
        role="tabpanel"
        className="body current"
        style={{
          display: context.state.biBorrowerType === '2' ? '' : 'none',
          margin: '1em'
        }}
      >
        <h4 className="m-t-0 header-title">
          <b>Информация о заемщике - Физическое лицо</b>
        </h4>
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">ИИН</label>
              <Input
                className='form-control'
                placeholder='введите ИИН'
                context={ context }
                stateName='biipfIin'
                onBlur={context.biipfIinBlurHandler.bind(context)}
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">ФИО</label>
              <Input
                className='form-control'
                placeholder='ФИО полностью'
                context={ context }
                stateName='biipfFio'
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Дата рождения</label>
              <DatePicker
                context={ context }
                stateName='biipfDob'
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Образование заемщика</label>
              <Select
                options={ context.educationOptions }
                onChange={ ({ value }) => context.setState({ biipfEducation: value }) }
                value={ context.state.biipfEducation }
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label htmlFor="field-5" className="control-label">Мобильный телефон</label>
              <InputMask
                className="form-control"
                value={ context.state.biipfMobilePhone }
                onChange={ e => context.setState({ biipfMobilePhone: e.currentTarget.value })}
                mask="+7(999) 999-99-99"
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="form-group">
              <label className="control-label">Email</label>
              <Input
                className='form-control'
                placeholder='введите свой e-mail'
                context={ context }
                stateName='biipfEmail'
              />
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label className="m-b-10">Семейное положение заемщика:</label>
              <br />
              <div className="radio radio-info radio-inline">
                <input type="radio" id="inlineRadio1" checked={context.state.biipfMarried} onChange={() => context.setState({biipfMarried: true})}/>
                <label htmlFor="inlineRadio1"> Женат/Замужем </label>
              </div>
               <div className="radio radio-info radio-inline">
                 <input type="radio" id="inlineRadio2" checked={!context.state.biipfMarried} onChange={() => context.setState({biipfMarried: false})} />
                 <label htmlFor="inlineRadio2"> Не женат/Не замужем </label>
               </div>
            </div>
          </div>
        </div><br />
        <div className="row" style={{ display: context.state.biipfMarried ? '' : 'none'}}>
          <div className="row">
            <div className="col-md-4">
              <div className="form-group">
                <label className="control-label">ФИО супруги</label>
                <Input
                  className='form-control'
                  placeholder='ФИО полностью'
                  context={ context }
                  stateName='biipfSpouseFio'
                />
              </div>
            </div>
            <div className="col-md-4">
              <div className="form-group">
                <label className="control-label">ИИН супруги</label>
                <Input
                  className='form-control'
                  placeholder='введите ИИН'
                  context={ context }
                  stateName='biipfSpouseIin'
                />
              </div>
            </div>
          </div><br />
          <div className="row">
            <div className="col-md-4">
              <div className="form-group">
                <label className="control-label">Дата рождения супруги</label>
                <DatePicker
                  context={ context }
                  stateName='biipfSpouseDob'
                />
              </div>
            </div>
            <div className="col-md-4">
              <div className="form-group">
                <label className="control-label">Образование супруги</label>
                <Select
                  options={ context.educationOptions }
                  onChange={ ({ value }) => context.setState({ biipfSpouseEducation: value }) }
                  value={ context.state.biipfSpouseEducation }
                />
              </div>
            </div>
            <div className="col-md-4">
              <div className="form-group">
                <label className="control-label">Занятость супруги</label>
                <div className="checkbox checkbox-primary" style={{marginTop: 0}}>
                  <input
                    type="checkbox"
                    name="hobbies"
                    id="hobby1"
                    defaultValue="ski"
                    data-parsley-mincheck={2}
                    data-parsley-multiple="hobbies"
                    checked={context.state.biipfspouseEmployment}
                    onChange={(e) => context.setState({biipfspouseEmployment: e.target.checked})}
                  />
                  <label htmlFor="hobby1"> Трудоустроен/трудоустроена </label>
                </div>
              </div>
            </div>
          </div>
        </div><br />
        <div className="row">
          <div className="col-md-12">
            <div className="form-group m-b-0">
              <label htmlFor className="control-label">Адрес проживания заемщика</label>
            </div>
          </div>
        </div><br />
        <div className="row">
          <IndividualPersonAddress
            context={ context }
          />
        </div><br />
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Сертификат "Бастау бизнес"</label>
              <p className="text-muted font-13">
                Кем и когда выдан сертификат по программе "Бастау бизнес" или другими программами (для начинающих предпринимателей)
              </p>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                context={ context }
                stateName='biipfIndividualPersonCertificate'
              />
            </div>
          </div>
        </div>
      </section>
    )
  }
}
