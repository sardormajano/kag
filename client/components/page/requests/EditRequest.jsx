import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'

import MainData from '/client/components/page/requests/edit-request/MainData'
import SingleStep from '/client/components/page/requests/edit-request/SingleStep'
import Conclusions from '/client/components/page/requests/edit-request/Conclusions'
import Photos from '/client/components/page/requests/edit-request/Photos'
import ApplicationReview from '/client/components/page/requests/edit-request/ApplicationReview'

export default class EditRequest extends Component {
  renderStepsButtons() {
    const { context } = this.props
    const { steps } = context.props

    return steps.map((step, index) => (
      <li
        className={ context.state.activeStep === step.order ? "current" : "done" }
        key={ index }
      >
        <a
          href="#"
          onClick={ e => {
            e.preventDefault()

            if(!context.mdFieldsFilledOut())
              return

            context.setState({ activeStep: step.order })
          } }
        >
          <span className="number">{ step.order + 1 }.</span> { step.name }
        </a>
      </li>
    ))
  }

  renderSteps() {
    const { context } = this.props
    const { steps } = context.props

    return steps.map( (step, index) => (
      <SingleStep
        key={ index }
        activeStep={ context.state.activeStep }
        step={ step }
        context={ context }
      />
    ))
  }

  render() {
    const { context } = this.props
    const { steps, documentTypes } = context.props
    const mdFieldsAreFilledOut = context.mdFieldsFilledOut()
    const otherFieldsAreFilledOut = context.otherFieldsAreFilledOut()
    const stepsLength = steps.length

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">Главная</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">Главная</a>
                      </li>
                      <li className="active">
                        Текущая страница
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              {/* end row */}
              {/* Basic Form Wizard */}
              <div className="row">
                <div className="col-md-12">
                  <div className="card-box">
                    <h4 className="m-t-0 header-title"><b>Создать заявку</b></h4><br />
                    <p>
                      <span className="btn waves-light waves-effect w-md" style={{background: '#00acc1', color: 'white'}}>текущий шаг</span>{' '}
                      <span className="btn waves-light waves-effect w-md" style={{background: '#f3f3f3'}}>незаполненный шаг</span>{' '}
                      <span className="btn btn-success waves-light waves-effect w-md">заполненный шаг</span>
                    </p><br />
                    <p className="text-muted m-b-30 font-13">
                      Заполните поля и нажмите Следующий шаг для перехода в следующий раздел заполнения.
                      Для возврата нажмите Предыдущий шаг.
                    </p>
                    <form id="wizard-vertical" role="application" className="wizard clearfix vertical">
                      <div className="steps clearfix">
                        <ul role="tablist">
                          <li
                            role="tab"
                            className={ context.state.activeStep === 0 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: 0 })
                              } }
                            >
                              <span className="number">1.</span> Основные данные
                            </a>
                          </li>
                          { this.renderStepsButtons() }
                          <li
                            role="tab"
                            className={ context.state.activeStep === stepsLength + 1 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: stepsLength + 1 })
                              } }
                            >
                              <span className="number">{ stepsLength + 1 }.</span> Заключения
                            </a>
                          </li>
                          <li
                            role="tab"
                            className={ context.state.activeStep === stepsLength + 2 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: stepsLength + 2 })
                              } }
                            >
                              <span className="number">{ stepsLength + 2 }.</span> Фотоотчет
                            </a>
                          </li>
                          <li
                            role="tab"
                            className={ context.state.activeStep === stepsLength + 3 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: stepsLength + 3 })
                              } }
                            >
                              <span className="number">{ stepsLength + 3 }.</span> Рассмотрение заявки
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="content clearfix">
                        <MainData context={ context } activeStep={ context.state.activeStep} />
                        { this.renderSteps() }
                        <Conclusions context={ context } stepsLength={ stepsLength} activeStep={ context.state.activeStep} />
                        <Photos
                          context={ context }
                          stepsLength={ stepsLength}
                          activeStep={ context.state.activeStep}
                          documentTypes={ documentTypes }
                        />
                        <ApplicationReview context={ context } stepsLength={ stepsLength} activeStep={ context.state.activeStep} />
                      </div>
                      <div className="actions clearfix">
                        <ul role="menu" aria-label="Pagination">
                          <li
                            className={ mdFieldsAreFilledOut ? '' : 'disabled' }
                            style={{
                              display: context.state.activeStep === 0 ? '' : 'none'
                            }}
                          >
                            <a
                              href="#"
                              role="menuitem"
                              onClick={ e => {
                                e.preventDefault()

                                if(mdFieldsAreFilledOut) {
                                  context.saveChanges.call(context)
                                }
                              } }
                            >
                              Сохранить
                            </a>
                          </li>
                          <li
                            style={{
                              display: context.state.activeStep === 0 ? 'none' : ''
                            }}
                          >
                            <a
                              href="#"
                              role="menuitem"
                              onClick={ context.goToPrevStep.bind(context) }
                            >
                              Предыдущий шаг
                            </a>
                          </li>
                          <li
                            style={{
                              display: context.state._id && context.state.activeStep !== context.props.steps.length ? '' : 'none'
                            }}
                            className={ mdFieldsAreFilledOut ? '' : 'disabled' }
                            title={ mdFieldsAreFilledOut ? '' : 'Заполните обязательные поля' }
                          >
                            <a
                              href="#"
                              role="menuitem"
                              onClick={ e => {
                                e.preventDefault()

                                if(mdFieldsAreFilledOut) {
                                  context.goToNextStep.call(context)
                                }
                              } }
                            >
                              Следующий шаг
                            </a>
                          </li>
                          <li
                            style={{
                              display: otherFieldsAreFilledOut ? '' : 'none'
                            }}
                          >
                            <a
                              href="#"
                              className='btn btn-success'
                              onClick={context.completeRequest.bind(context)}
                              role="menuitem"
                            >
                              Завершить
                            </a>
                          </li>
                        </ul>
                      </div>
                    </form>
                    {/* End #wizard-vertical */}
                  </div>
                </div>
              </div>
              {/* End row */}
            </div> {/* container */}
          </div> {/* content */}
        </div>
      </div>
    )
  }
}
