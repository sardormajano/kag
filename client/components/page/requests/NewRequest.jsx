import React, { Component } from 'react'
import Scroll, { scroller } from 'react-scroll';

import Footer from '/client/components/fuc/Footer'

import LoanInformation from '/client/components/page/requests/new-request/LoanInformation'
import BorrowerInformation from '/client/components/page/requests/new-request/BorrowerInformation'
import Photos from '/client/components/page/requests/edit-request/Photos'
import ApplicationReview from '/client/components/page/requests/edit-request/ApplicationReview'
import SingleStep from '/client/components/page/requests/new-request/SingleStep'

export default class NewRequest extends Component {
  renderStepsButtons() {
    const { context } = this.props
    const { steps } = context.props

    return steps.map((step, index) => (
      <li
        className={ context.state.activeStep === step.order + 1 ? "current" : "done" }
        key={ index }
      >
        <a
          href="#"
          onClick={ e => {
            e.preventDefault()

            if(!context.liFieldsFilledOut()){
              Bert.alert({
                  message: "Для того чтобы продолжить заполнение, необходимо заполнить обязательные поля в 1 шаге.",
                  type: 'warning',
                  style: 'growl-top-right',
                  icon: 'fa-warning'
              })
              return
            }

            context.setState({ activeStep: step.order + 1 })
          } }
        >
          <span className="number">{ step.order + 2 }.</span> { step.name }
        </a>
      </li>
    ))
  }

  renderSteps() {
    const { context } = this.props
    const { steps } = context.props

    return steps.map( (step, index) => (
      <SingleStep
        key={ index }
        activeStep={ context.state.activeStep }
        step={ step }
        context={ context }
      />
    ))
  }

  render() {
    const { context } = this.props
    const { steps, documentTypes } = context.props
    const liFieldsAreFilledOut = context.liFieldsFilledOut()
    const otherFieldsAreFilledOut = context.otherFieldsAreFilledOut()
    const stepsLength = steps.length

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">Главная</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">Главная</a>
                      </li>
                      <li className="active">
                        Текущая страница
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              {/* end row */}
              {/* Basic Form Wizard */}
              <div className="row">
                <div className="col-md-12">
                  <div className="card-box">
                    <h4 className="m-t-0 header-title"><b>Создать заявку</b></h4><br />
                    <p>
                      <span className="btn waves-light waves-effect w-md" style={{background: '#00acc1', color: 'white'}}>текущий шаг</span>{' '}
                      <span className="btn waves-light waves-effect w-md" style={{background: '#f3f3f3'}}>незаполненный шаг</span>{' '}
                      <span className="btn btn-success waves-light waves-effect w-md">заполненный шаг</span>
                    </p><br />
                    <p className="text-muted m-b-30 font-13">
                      Заполните поля и нажмите Сохранить для перехода в следующий раздел заполнения.
                      Уделите внимание полям помеченным звездочкой, они обязательны для заполнения.
                    </p>
                    <form id="wizard-vertical" role="application" className="wizard clearfix vertical">
                      <div className="steps clearfix">
                        <ul role="tablist">
                          <li
                            role="tab"
                            className={ context.state.activeStep === 0 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: 0 })
                              } }
                            >
                              <span className="number">1.</span> Информация о кредите
                            </a>
                          </li>
                          <li
                            role="tab"
                            className={ context.state.activeStep === 1 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: 1 })
                              } }
                            >
                              <span className="number">2.</span> Информация о заемщике
                            </a>
                          </li>
                          { this.renderStepsButtons() }
                          <li
                            role="tab"
                            className={ context.state.activeStep === stepsLength + 2 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: stepsLength + 2 })
                              } }
                            >
                              <span className="number">{ stepsLength + 3 }.</span> Фотоотчет
                            </a>
                          </li>
                          <li
                            role="tab"
                            className={ context.state.activeStep === stepsLength + 3 ? 'current' : 'done'}
                          >
                            <a
                              href="#"
                              onClick={ e => {
                                e.preventDefault()
                                context.setState({ activeStep: stepsLength + 3 })
                              } }
                            >
                              <span className="number">{ stepsLength + 4 }.</span> Рассмотрение заявки
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="content clearfix">
                        <LoanInformation context={ context } activeStep={ context.state.activeStep} />
                        <BorrowerInformation context={ context } activeStep={ context.state.activeStep}/>
                        { this.renderSteps() }
                        <Photos
                          context={ context }
                          stepsLength={ stepsLength}
                          activeStep={ context.state.activeStep}
                          documentTypes={ documentTypes }
                        />
                        <ApplicationReview context={ context } stepsLength={ stepsLength} activeStep={ context.state.activeStep} />
                      </div>
                      <div className="actions clearfix">
                        <ul role="menu" aria-label="Pagination">
                          <li
                            // className={ liFieldsAreFilledOut ? '' : 'disabled' }
                            style={{
                              display: context.state.activeStep === 0 || context.state.activeStep === 1 ? '' : 'none'
                            }}
                          >
                            <a
                              href='#'
                              role="menuitem"
                              className="btn btn-primary waves-effect waves-light"
                              onClick={ e => {
                                e.preventDefault()
                                if(!liFieldsAreFilledOut) {
                                  context.setState({activeStep: 0})
                                  const warnFields = context.fieldsWarningState()

                                  warnFields.map(field => {
                                    let warnState = `${field}Warn`
                                    context.setState({
                                      [warnState]: true
                                    })
                                    scroller.scrollTo(field, {
                                      duration: 1500,
                                      delay: 100,
                                      smooth: true,
                                      offset: -100
                                    })
                                  })

                                  Bert.alert({
                                      message: "Заполните все необходимые поля для продолжения",
                                      type: 'danger',
                                      style: 'growl-top-right',
                                      icon: 'fa-exclamation'
                                  })
                                  return
                                }
                                if(liFieldsAreFilledOut) {
                                  context.saveChanges.call(context)
                                  context.setState({sendable: true})
                                }
                              } }
                            >
                              Сохранить
                            </a>
                          </li>
                          <li
                            style={{
                              display: context.state.activeStep === 0 || context.state.activeStep === 1 ? 'none' : ''
                            }}
                          >
                            <a
                              href="#"
                              className='btn btn-success'
                              onClick={e => {
                                e.preventDefault()
                                if(!context.state._id) {
                                  Bert.alert({
                                      message: "Прежде чем отправить документы на обработку, необходимо сохранить изменения в 1 и во 2 шаге",
                                      type: 'danger',
                                      style: 'growl-top-right',
                                      icon: 'fa-exclamation'
                                  })
                                } else {
                                  if(!otherFieldsAreFilledOut){
                                    Bert.alert({
                                        message: "Пожалуйста загрузите все обязательные документы.",
                                        type: 'danger',
                                        style: 'growl-top-right',
                                        icon: 'fa-exclamation'
                                    })
                                    return
                                  }
                                  if(otherFieldsAreFilledOut) {
                                      context.completeRequest.call(context)
                                  }
                                }
                              }}
                              role="menuitem"
                            >
                              Отправить
                            </a>
                          </li>
                        </ul>
                      </div>
                    </form>
                    {/* End #wizard-vertical */}
                  </div>
                </div>
              </div>
              {/* End row */}
            </div> {/* container */}
          </div> {/* content */}
        </div>
      </div>
    )
  }
}
