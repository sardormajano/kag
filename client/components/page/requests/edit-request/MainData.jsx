import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Textarea from '/client/components/fuc/Textarea'
import FileUpload from '/client/containers/fuc/FileUploadContainer'
import Select from 'react-select'
import NumericInput from 'react-numeric-input';

function addSpaces(str) {
  if(!str || str === 'null')
    return ''

  str = str.trim().split(' ').join('')
  str = str.replace(/[^\d]/g, '')
  str = parseInt(str).toLocaleString('ru-RU')

  return str
}

function spacedStringToNum(str) {
  return parseInt(str.replace(/\s/g, ''))
}

export default class MainData extends Component {

  render() {
    const { context } = this.props

    return (
      <section
        role="tabpanel"
        aria-labelledby="wizard-vertical-h-0"
        className="body current"
        aria-hidden="false"
        style={ {
          display: this.props.activeStep === 0 ? '' : 'none'
        } }
      >
        <h4 className="m-t-0 header-title">
          <b>Заполните поля</b>
        </h4>
        <div className="row m-t-20">
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Валюта кредитования *</label>
              <Select
                options={ context.currencyOptions }
                onChange={ ({ value }) => context.setState({ mdCurrency: value }) }
                value={ context.state.mdCurrency }
              />
            </div>
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Вид продукта *</label>
              <Select
                options={ context.productTypeOptions }
                onChange={ ({ value }) => context.setState({ mdProductType: value }) }
                value={ context.state.mdProductType }
              />
            </div>
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Тип предпринимателя *</label>
              <Select
                options={ context.enterpreneurTypeOptions }
                onChange={ ({ value }) => context.setState({ mdEnterpreneurType: value }) }
                value={ context.state.mdEnterpreneurType }
              />
            </div>
          </div>
        </div>
        <div className="row m-t-20">
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Сумма микрокредита *</label>
              <input
                className="form-control"
                placeholder="введите сумму"
                onChange={ e => {
                  let val = e.currentTarget.value
                  val = addSpaces(val)
                  context.setState({ mdMicrocreditSum: val })
                }}
                value={ context.state.mdMicrocreditSum }
              />
            </div>
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Ставка вознаграждения *</label>
              <NumericInput
                onChange={ val => context.setState({ mdInterestRate: val }) }
                value={ context.state.mdInterestRate }
                placeholder="введите ставку вознаграждения"
                className="form-control"
                style={ false }
                min={ 0 }
                max={ 100 }
                format={ num => {
                  return `${ num }%`
                }}
              />
            </div>
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Срок микрокредита *</label>
              <NumericInput
                onChange={ val => {
                  val = addSpaces('' + val)
                  context.setState({ mdTerm: val })
                } }
                value={ context.state.mdTerm }
                placeholder="срок в месяцах"
                className="form-control"
                style={ false }
              />
            </div>
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label className="control-label">Размер гарантии *</label>
              <input
                className="form-control"
                placeholder="введите размер гарантии"
                onChange={ e => {
                  let val = e.currentTarget.value
                  if(spacedStringToNum(val) > spacedStringToNum(context.state.mdMicrocreditSum))
                    return

                  val = addSpaces(val)
                  context.setState({ mdGuaranteeAmount: val })
                }}
                value={ context.state.mdGuaranteeAmount }
              />
            </div>
          </div>
        </div>
        <div className="row m-t-20">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Цель кредитования *</label>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder="опишите цель кредитования"
                context={ context }
                stateName='mdLendingPurpose'
              />
            </div>
          </div>
        </div>
        <div className="row m-t-20">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Порядок и сроки погашения основного долга *</label>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder="опишите порядок и сроки погашения основного долга"
                context={ context }
                stateName='mdMainDebtOrderAndTerm'
              />
            </div>
          </div>
        </div>
        <div className="row m-t-20">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Порядок и сроки погашения вознаграждения *</label>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder="опишите порядок и сроки погашения вознаграждения"
                context={ context }
                stateName='mdRemunerationOrderAndTerm'
              />
            </div>
          </div>
        </div>
        <div className="row m-t-20">
          <div className="col-md-12">
            <div className="form-group">
              <label className="control-label">Особые условия</label>
              <Textarea
                className="form-control"
                rows={4}
                maxLength={500}
                placeholder="опишите особые условия"
                context={ context }
                stateName='mdSpecialConditions'
              />
            </div>
          </div>
        </div>
      </section>
    )
  }
}
