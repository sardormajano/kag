import React from 'react'

export default (props) => {
  return (
    <section
      role="tabpanel"
      aria-labelledby="wizard-vertical-h-0"
      className="body current"
      aria-hidden="false"
      style={ {
        display: props.activeStep === props.stepsLength + 1 ? '' : 'none'
      } }
    >
        <div className="well">
          <h4 className="header-title"><b>Экспертные заключения</b></h4>
        </div>
        <div className="row m-t-20">
          <div className="col-md-2 col-sm-3">
            <div className="text-center card-box">
              <div className="clearfix" />
              <div className="member-card">
                <div className="thumb-lg member-thumb m-b-10 center-block">
                  <img src="http://via.placeholder.com/88x88" className="img-circle img-thumbnail" alt="profile-image" />
                  <i className="mdi mdi-check-circle member-star text-success" title="согласовано" />
                </div>
                <div>
                  <h5 className="m-b-5">Даурен Момынкулов</h5>
                  <p className="text-muted m-b-0" title="Департамент гарантирования">ДГ</p>
                  <p className="text-muted" title="Дата загрузки">01.08.2017</p>
                  <button type="button" className="btn btn-default" title="Скачать">
                    <i className="fa fa-download" />
                  </button>
                </div>
              </div>
            </div>
          </div> {/* end col */}
          <div className="col-md-2 col-sm-3">
            <div className="text-center card-box">
              <div className="clearfix" />
              <div className="member-card">
                <div className="thumb-lg member-thumb m-b-10 center-block">
                  <img src="http://via.placeholder.com/88x88" className="img-circle img-thumbnail" alt="profile-image" />
                  <i className="mdi mdi-check-circle member-star text-success" title="согласовано" />
                </div>
                <div>
                  <h5 className="m-b-5">Нурбол Кайранов</h5>
                  <p className="text-muted m-b-0" title="Служба внутреннего контроля">СВК</p>
                  <p className="text-muted" title="Дата загрузки">31.07.2017</p>
                  <button type="button" className="btn btn-default" title="Скачать">
                    <i className="fa fa-download" />
                  </button>
                  <button type="button" className="btn btn-danger" title="Удалить">
                    <i className="fa fa-trash" />
                  </button>
                </div>
              </div>
            </div>
          </div> {/* end col */}
          <div className="col-md-2 col-sm-3">
            <div className="text-center card-box">
              <div className="clearfix" />
              <div className="member-card">
                <div className="thumb-lg member-thumb m-b-10 center-block">
                  <img src="http://via.placeholder.com/88x88" className="img-circle img-thumbnail" alt="profile-image" />
                  <i className="mdi mdi-close-circle member-star text-danger" title="отклонено" />
                </div>
                <div>
                  <h5 className="m-b-5">Берик Сериков</h5>
                  <p className="text-muted m-b-0" title="Правовой департамент">ПД</p>
                  <p className="text-muted" title="Дата загрузки">31.07.2017</p>
                  <button type="button" className="btn btn-default" title="Скачать">
                    <i className="fa fa-download" />
                  </button>
                </div>
              </div>
            </div>
          </div> {/* end col */}
          <div className="col-md-2 col-sm-3">
            <div className="text-center card-box">
              <div className="clearfix" />
              <div className="member-card">
                <div className="thumb-lg member-thumb m-b-10 center-block">
                  <img src="http://via.placeholder.com/88x88" className="img-circle img-thumbnail" alt="profile-image" />
                  <i className="fa fa-spin fa-clock-o member-star text-primary" title="рассмотрение" />
                </div>
                <div>
                  <h5 className="m-b-5">Серик Рисков</h5>
                  <p className="text-muted m-b-0" title="Правовой департамент">ДРМ</p>
                  <p className="text-muted" title="Дата загрузки">на рассмотрении</p>
                </div>
              </div>
            </div>
          </div> {/* end col */}
        </div>
      </section>
  )
}
