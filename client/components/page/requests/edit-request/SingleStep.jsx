import React, { Component } from 'react'
import FileUploadContainer from '/client/containers/fuc/FileUploadContainer'

export default class SingleStep extends Component {
  renderDocuments() {
    const { step, context } = this.props
    const { documentTypes } = context.props

    return step.documentTypes.map((documentType, index) => (
      <FileUploadContainer
        key={ index }
        documentType={ documentType }
        context={ context }
        stateName={ step._id }
      />
    ))
  }

  render() {
    const { step, activeStep } = this.props

    return (
      <section
        style={ {
          display: activeStep === step.order ? '' : 'none'
        } }
      >
        <div className="well">
          { step.description }
        </div>
        <div className="row m-t-20">
          <div className="col-md-12">
            <div className="form-group">
              { this.renderDocuments() }
            </div>
          </div>
        </div>
      </section>
    )
  }
}
