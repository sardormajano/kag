import React from 'react'
import Textarea from '/client/components/fuc/Textarea'
import Select from 'react-select'
import FileUploadContainer from '/client/containers/fuc/FileUploadContainer'
import { Link } from 'react-router-dom'

import moment from 'moment';

export default (props) => {
  const { context } = props
  const { applicationReview } = context.state
  const { users, customRoles } = context.props

  const agreeingOptions = users.filter(user => {
    const { roles } = user.profile
    if(!roles || !roles.length)
      return

    return roles.some(role => role.value === '3')
  }).map(user => {
    return {
      value: user._id,
      label: user.profile.name ? user.profile.name.ru : user.profile.firstName + ' ' + user.profile.lastName
    }
  })

  const approvingOptions = users.filter(user => {
    const { roles } = user.profile
    if(!roles || !roles.length)
      return

    return roles.some(role => role.value === '4')
  }).map(user => {
    return {
      value: user._id,
      label: user.profile.name ? user.profile.name.ru : user.profile.firstName + ' ' + user.profile.lastName
    }
  })

  const conclusionType = {
    value: '0',
    label: 'Экспертное заключение'
  }


  return (
    <section
      role="tabpanel"
      aria-labelledby="wizard-vertical-h-0"
      className="body current"
      aria-hidden="false"
      style={ {
        display: props.activeStep === props.stepsLength + 3 ? '' : 'none'
      } }
    >
      <div className="well">
        <h4 className="header-title"><b>На данной странице отображается история заявки</b></h4>
      </div>
      <div className="row m-t-20">
        <div className="col-md-12">
          <div className="timeline timeline-left">
            {
              applicationReview ? applicationReview.map((item, index) => {
                const date = moment(item.createdAt).format('LLL').split(',')
                const theUser = users.find(user => user._id === item.createdBy)

                return (
                  <article className="timeline-item" key={ index }>
                    <div className="timeline-desk">
                      <div className="panel">
                        <div className="timeline-box">
                          <span className="arrow" />
                          <span className="timeline-icon bg-primary"><i className="mdi mdi-checkbox-blank-circle-outline" /></span>
                          <h4 className="text-primary">{date[0]}</h4>
                          <p className="timeline-date text-muted"><small>{date[1]}</small></p>
                          <p>{ theUser.profile.firstName || theUser.profile.name.ru } - { theUser.profile.roles ? theUser.profile.roles[0].label : '' } </p>
                          <p>{item.message}</p>
                        </div>
                      </div>
                    </div>
                  </article>
                )
              }) :
              ''
            }
            <article className="timeline-item">
              <div className="timeline-desk">
                <div className="panel">
                  <div className="timeline-box">
                    <span className="arrow" />
                    <span className="timeline-icon bg-primary"><i className="mdi mdi-checkbox-blank-circle-outline" /></span>
                    <div className="form-group">
                      <Textarea
                        context={ context }
                        stateName='arMessage'
                        className="form-control"
                        rows={5}
                        maxLength={500}
                        placeholder="Ваше сообщение"
                      />
                    </div>
                    <div className="row">
                      <div className="col-xs-12">
                        <button
                          type="submit"
                          className="btn btn-primary"
                          id="send"
                          onClick={ e => { e.preventDefault(); context.writeMessageHandler.call(context) } }
                        >
                          Написать сообщение
                        </button>
                      </div> {/* /col */}
                    </div> {/* /row */}
                    <hr />
                    <div className="row m-t-20">

                      <div className="form-group"
                          style={{ textAlign: "right", margin: "20px" }} >
                        <Link
                          to={`/stimulsoft-viewer/${context.state._id}/Application7`}
                          className="on-default"
                          title="Сформировать шаблон Экспертного заключения Представителя"
                          target="_blank"
                            >
                          <i className="fa fa-file-pdf-o" /> Сформировать шаблон Экспертного заключения Представителя
                        </Link>
                        <br/>
                        <Link
                          to={`/stimulsoft-viewer/${context.state._id}/Application8`}
                          className="on-default"
                          title="Сформировать шаблон Проектного заключения"
                          target="_blank"
                        >
                          <i className="fa fa-file-pdf-o" /> Сформировать шаблон Проектного заключения
                        </Link>
<br/>
                        <Link
                          to={`/stimulsoft-viewer/${context.state._id}/Application9`}
                          className="on-default"
                          title="Сформировать шаблон Заключения ДРМ"
                          target="_blank"
                        >
                          <i className="fa fa-file-pdf-o" /> Сформировать шаблон Заключения ДРМ
                        </Link>
<br/>
                        <Link
                          to={`/stimulsoft-viewer/${context.state._id}/Application10`}
                          className="on-default"
                          title="Сформировать шаблон Юридического заключения"
                          target="_blank"
                        >
                          <i className="fa fa-file-pdf-o" /> Сформировать шаблон Юридического заключения
                        </Link>
<br/>
                        <FileUploadContainer
                          documentType={ conclusionType }
                          context={ context }
                          stateName='arConclusion'
                        />
<br/><br/>
                      </div>
                      <div className="col-xs-12">
                        <button type="button" className="btn btn-success waves-effect w-md waves-light">
                          <i className="fa fa-lg fa-check m-r-5" />
                          <span>Согласовать</span>
                        </button>
                      </div><br /><br /><br />
                    </div> {/* /row */}
                    <hr />
                    <div className="col-xs 12">
                      <button type="button" className="btn btn-warning waves-effect w-md waves-light">
                        <i className="fa fa-lg fa-mail-reply m-r-5" />
                        <span>На доработку</span>
                      </button>
                    </div>
                    <hr />
                    <div className="row m-t-20">
                      <div className="col-xs-12">
                        <button type="button" className="btn btn-success waves-effect w-md waves-light">
                          <i className="fa fa-lg fa-check m-r-5" />
                          <span>Утвердить</span>
                        </button>{' '}
                        <button type="button" className="btn btn-danger waves-effect w-md waves-light">
                          <i className="fa fa-lg fa-times m-r-5" />
                          <span>Отклонить</span>
                        </button>
                      </div> {/* /col  */}
                    </div> {/* /row */}
                    <hr />
                    <div className="row m-t-20">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label htmlFor="field-2" className="control-label">Выберите согласующего</label>
                          <Select
                            options={ agreeingOptions }
                            onChange={ values => context.setState({ arAgreeing: values }) }
                            value={ context.state.arAgreeing }
                            multi
                          /><br/>
                        </div>
                        <button
                          type="button"
                          className="btn btn-default waves-effect w-md waves-light"
                          onClick={ context.sendForAgreementHandler.bind(context) }
                        >
                          Отправить на согласование
                        </button>
                        <br />
                      </div>
                    </div>
                    <hr />
                    <div className="row m-t-20">
                      <div className="col-xs-12">
                        <div className="form-group">
                          <label htmlFor="field-2" className="control-label">Выберите утверждающего</label>
                          <Select
                            options={ approvingOptions }
                            onChange={ values => context.setState({ arApproving: values }) }
                            value={ context.state.arApproving }
                            multi
                          /><br/>
                        </div>
                        <button
                          type="button"
                          className="btn btn-default waves-effect w-md waves-light"
                          onClick={ context.sendForApprovalHandler.bind(context) }
                        >
                          Отправить на утверждение
                        </button>
                      </div> {/* /col */}
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>
  )
}
