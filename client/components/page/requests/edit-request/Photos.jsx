import React from 'react'
import FileUploadContainer from '/client/containers/fuc/FileUploadContainer'

export default (props) => {
  const { documentTypes, context } = props

  if(!documentTypes.length)
    return <div />

  const documentTypeDoc = documentTypes.find(type => type._id === '1')

  const theDocumentType = {
    value: documentTypeDoc._id,
    label: documentTypeDoc.name.ru
  }

  return (
    <section
      role="tabpanel"
      aria-labelledby="wizard-vertical-h-0"
      className="body current"
      aria-hidden="false"
      style={ {
        display: props.activeStep === props.stepsLength + 2 ? '' : 'none'
      } }
    >
      <div className="row m-t-20">
        <div className="form-group">
          <label className="control-label" htmlFor="project-documents">Фото с выезда на место реализации проекта</label>
          <FileUploadContainer
            documentType={ theDocumentType }
            context={ context }
            stateName='phFiles'
          />
        </div>

      </div>
    </section>
  )
}
