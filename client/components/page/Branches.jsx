import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateBranchModal from '/client/components/page/branches/CreateBranchModal'
import EditBranchModal from '/client/components/page/branches/EditBranchModal'

import Confirm from 'react-confirm-bootstrap'

export default class Branches extends Component {
  renderBranchesTable() {
    const { context, locStrings } = this.props
    const { branches } = context.props

    return branches.map((branch, index) => (
      <tr className="gradeX" key={ index }>
        <td>{branch.name.ru}</td>
        <td>{branch.name.kz}</td>
        <td>{branch.shortName}</td>
        <td>{branch.code}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-branch-modal"
            onClick={() => {
              context.setState({
                ebmNameRu: branch.name.ru,
                ebmNameKz: branch.name.kz,
                ebmShortName: branch.shortName,
                ebmBranchId: branch._id,
                ebmCode: branch.code,
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={() => context.deleteBranch(branch._id)}
            body={locStrings.branchDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.branchDelete}
          >
              <a
                href="#"
                className="on-default remove-row"
                title={locStrings.delete}
                onClick={ e => {
                  e.preventDefault()
                } }
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.branches}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.branches}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-branch-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th>{locStrings.shortName}</th>
                          <th>{locStrings.code}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderBranchesTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateBranchModal
                id='create-branch-modal'
                context={ context }
              />
              <EditBranchModal
                id='edit-branch-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
