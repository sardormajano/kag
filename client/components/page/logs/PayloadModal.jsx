import React, { Component } from 'react'

export default class PayloadModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { payload } = context.state
    const { locStrings, lang } = context.props

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].viewContent}</h4>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <textarea
                  className='form-control'
                  onChange={e => {}}
                  cols={ 30 }
                  rows={ 10 }
                  value={ JSON.stringify(payload) }
                >
                </textarea>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
