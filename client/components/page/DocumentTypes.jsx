import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateDocumentTypeModal from '/client/components/page/documentTypes/CreateDocumentTypeModal'
import EditDocumentTypeModal from '/client/components/page/documentTypes/EditDocumentTypeModal'

import Confirm from 'react-confirm-bootstrap'
import Input from '/client/components/fuc/Input'

export default class DocumentTypes extends Component {
  renderDocumentTypesTable() {
    const { context, locStrings } = this.props
    const { documentTypes } = context.props
    let { filterRu, filterKz } = context.state

    const filteredDocumentTypes = documentTypes.filter( docType => {
      //filtering by NameRu
      if(!(new RegExp(filterRu, 'i')).test(docType.name.ru)) {
          return false;
      }

      //filtering by NameKz
      if(!(new RegExp(filterKz, 'i')).test(docType.name.kz)) {
          return false;
      }

      return true
    })

    return filteredDocumentTypes.map((documentType, index) => (
      <tr className="gradeX" key={ index }>
        <td>{documentType.name.ru}</td>
        <td>{documentType.name.kz}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-documentType-modal"
            onClick={() => {
              context.setState({
                emNameRu: documentType.name.ru,
                emNameKz: documentType.name.kz,
                emTooltip: documentType.tooltip,
                emMultiple: documentType.multiple,
                emRequired: documentType.required,
                emDocumentTypeId: documentType._id
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={() => context.deleteDocumentType(documentType._id)}
            body={locStrings.documentTypeDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.documentTypeDelete}
          >
            <a
              href="#"
              className="on-default remove-row"
              title={locStrings.delete}
              onClick={ e => {
                e.preventDefault()
              } }
            >
              <i className="fa fa-trash-o" />
            </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.documentTypes}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.documentTypes}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-documentType-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-info">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          {locStrings.filters}
                        </a>
                      </h4>
                    </div>
                    <div className="panel-body">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-md-6 control-label">{locStrings.filterByRuName}</label>
                            <div className="col-md-10">
                              <Input
                                type="text"
                                placeholder={locStrings.typeTextToSearch}
                                className="form-control"
                                context={ context }
                                stateName='filterRu'
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-md-6 control-label">{locStrings.filterByKzName}</label>
                            <div className="col-md-10">
                              <Input
                                type="text"
                                placeholder={locStrings.typeTextToSearch}
                                className="form-control"
                                context={ context }
                                stateName='filterKz'
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderDocumentTypesTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateDocumentTypeModal
                id='create-documentType-modal'
                context={ context }
              />
              <EditDocumentTypeModal
                id='edit-documentType-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
