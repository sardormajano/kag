import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import AsyncInput from '/client/containers/fuc/AsyncInputContainer'
import Select from 'react-select'

export default class EditOrganizationModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { locStrings, lang } = context.props
    const allFieldsFilledOut = context.state.eomNameKz
                                && context.state.eomNameRu
                                && context.state.eomShortName
                                && context.state.eomRepresentative
                                && context.state.eomEmail
                                && context.state.eomPassword
                                && context.state.eomMobilePhone
                                && context.state.eomBin
                                && context.state.eomBranch

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].organizationEdit}</h4>
            </div>
            <div className="modal-body">
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].fullKzName}</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eomNameKz'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].fullRuName}</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eomNameRu'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].shortDenomination}</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eomShortName'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].repFullName} *</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eomRepresentative'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].email}</label>
                      <AsyncInput
                        context={ context }
                        stateName='eomEmail'
                        checkMethod='email.exists'
                        checkException={ context.state.eomOrganizationId }
                        label='Эл. почта'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].password}</label>
                      <Input
                        type="password"
                        className="form-control"
                        context={ context }
                        stateName='eomPassword'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-5" className="control-label">{locStrings[lang].mobile}</label>
                      <AsyncInput
                        context={ context }
                        stateName='eomMobilePhone'
                        checkMethod='mobile.phone.exists'
                        checkException={ context.state.eomOrganizationId }
                        label='Мобильный телефон'
                        data-mask="+7(999) 999-99-99"
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-5" className="control-label">{locStrings[lang].phone}</label>
                      <AsyncInput
                        context={ context }
                        stateName='eomPhone'
                        checkMethod='phone.exists'
                        checkException={ context.state.eomOrganizationId }
                        label='Телефон'
                        data-mask="+7(9999) 99-99-99"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].bin}</label>
                      <AsyncInput
                        context={ context }
                        stateName='eomBin'
                        checkMethod='iin.bin.exists'
                        checkException={ context.state.eomOrganizationId }
                        label='БИН'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].adress}</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eomAddress'
                      />
                    </div>
                  </div>
                </div>
                <div
                  className="row"
                >
                  <div
                    className="col-md-12"
                    style={{ zIndex: 2 }}
                  >
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].branch}</label>
                      <Select
                        options={ context.branchOptions }
                        onChange={ ({ value }) => context.setState({ eomBranch: value }) }
                        value={ context.state.eomBranch }
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                type="button"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                className="btn btn-primary waves-effect waves-light"
                onClick={ () => {
                  this.closeButton.click()
                  context.updateOrganization()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
