import React from 'react'

import { Link } from 'react-router-dom'
import Input from '/client/components/fuc/Input'
import { cookie } from '/client/lib/coreLib'

export default Login = ({ context }) => (
  <section>
    <div className="container-alt">
      <div className="row">
        <div className="col-sm-12">
          <div className="wrapper-page">
            <div className="m-t-40 account-pages">
              <div className="text-center account-logo-box">
                <h2 className="text-uppercase">
                  <a href="index.html" className="text-success">
                    <span><img src="assets/images/logo.svg" alt height={36} /></span>
                  </a>
                </h2>
              </div>
              <div className="account-content">
                <form
                  className="form-horizontal"
                  action="#"
                  onSubmit={ context.formSubmitHandler.bind(context) }
                >
                  <div className="form-group text-center">
                    <br />
                    <div className="radio radio-info radio-inline">
                      <input
                        type="radio"
                        id="ru"
                        defaultValue="option55"
                        name="lang"
                        checked={context.state.lang === 'ru'}
                        title="язык отображения"
                        onChange={(event) => {
                          cookie.set('lang', 'ru', 9999)
                          context.onLangChangeHandler(event)
                        }}
                      />
                      <label htmlFor="ru" title="выберите язык"> РУС </label>
                    </div>
                    <div className="radio radio-info radio-inline">
                      <input
                        type="radio"
                        id="kz"
                        defaultValue="option44"
                        name="lang"
                        checked={context.state.lang === 'kz'}
                        title="тілді таңдау"
                        onChange={(event) => {
                          cookie.set('lang', 'kz', 9999)
                          context.onLangChangeHandler(event)
                        }}
                      />
                      <label htmlFor="kz" title="тілді таңдаңыз"> ҚАЗ </label>
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="col-xs-12">
                      <Input
                        className="form-control"
                        type="text"
                        required
                        placeholder={`${context.props.locStrings[context.state.lang].iin}/${context.props.locStrings[context.state.lang].bin}`}
                        context={ context }
                        stateName='login'
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-xs-12">
                      <Input
                        className="form-control"
                        type="password"
                        required
                        placeholder="Пароль"
                        context={ context }
                        stateName='password'
                      />
                    </div>
                  </div>
                  <div className="form-group text-center m-t-30">
                    <div className="col-sm-12">
                      <Link to='/reset-password' className="text-muted">
                        <i className="fa fa-lock m-r-5" />
                        {context.props.locStrings[context.state.lang].forgetPass}
                      </Link>
                    </div>
                  </div>
                  <div className="form-group account-btn text-center m-t-10">
                    <div className="col-xs-12">
                      <button
                        className="btn w-md btn-bordered btn-danger waves-effect waves-light"
                        type="submit"
                        disabled={ context.state.loginErrors.length ? true : false }
                      >
                        {context.props.locStrings[context.state.lang].logIn}
                      </button>
                    </div>
                  </div>
                </form>
                <div className="clearfix" />
              </div>
            </div>
            {/* end card-box*/}
            <div className="row m-t-50">
              <div className="col-sm-12 text-center">
                <p className="text-muted">{context.props.locStrings[context.state.lang].ifMFOKT}
                  <Link
                    to='signup'
                    className="text-primary m-l-5"
                  >
                    {context.props.locStrings[context.state.lang].signUpMFO}
                  </Link>
                </p>
              </div>
            </div>
          </div>
          {/* end wrapper */}
        </div>
      </div>
    </div>
  </section>
)
