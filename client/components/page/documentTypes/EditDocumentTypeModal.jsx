import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Checkbox from '/client/components/fuc/Checkbox'

export default class EditDocumentTypeModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { ermPairs } = context.state
    const { locStrings, lang } = context.props
    const allFieldsFilledOut = context.state.emNameRu && context.state.emNameKz && context.state.emTooltip

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].documentTypeEdit}</h4>
            </div>
            <div className="modal-body">
              <form className="form-horizontal" role="form">
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].ruName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='emNameRu'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].kzName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='emNameKz'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].hint}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='emTooltip'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-md-9 control-label">
                    <div className="checkbox checkbox-primary">
                      <Checkbox
                        context={ context }
                        stateName='emRequired'
                        id="emRequired"
                      />
                      <label htmlFor="emRequired">
                        {locStrings[lang].reqField}
                      </label>
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-md-9 control-label">
                    <div className="checkbox checkbox-primary">
                      <Checkbox
                        context={ context }
                        stateName='emMultiple'
                        id="emMultiple"
                      />
                      <label htmlFor="emMultiple">
                        {locStrings[lang].multiple}
                      </label>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                type="button"
                className="btn btn-primary waves-effect waves-light"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                onClick={ () => {
                  this.closeButton.click()
                  context.updateDocumentType()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
