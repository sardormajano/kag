import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Checkbox from '/client/components/fuc/Checkbox'

export default class CreateDocumentTypeModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { locStrings, lang } = context.props
    const allFieldsFilledOut = context.state.cmNameRu && context.state.cmNameKz && context.state.cmTooltip

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].documentTypeCreate}</h4>
            </div>
            <div className="modal-body">
              <form className="form-horizontal" role="form">
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].ruName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='cmNameRu'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].kzName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='cmNameKz'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].hint}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='cmTooltip'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-md-9 control-label">
                    <div className="checkbox checkbox-primary">
                      <Checkbox
                        context={ context }
                        stateName='cmRequired'
                        id="cmRequired"
                      />
                      <label htmlFor="cmRequired">
                        {locStrings[lang].reqField}
                      </label>
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-md-9 control-label">
                    <div className="checkbox checkbox-primary">
                      <Checkbox
                        context={ context }
                        stateName='cmMultiple'
                        id="cmMultiple"
                      />
                      <label htmlFor="cmMultiple">
                        {locStrings[lang].multiple}
                      </label>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                type="button"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                className="btn btn-primary waves-effect waves-light"
                onClick={ () => {
                  this.closeButton.click()
                  context.insertDocumentType()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
