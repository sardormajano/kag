import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'

import Select from 'react-select'
import { Link } from 'react-router-dom'

import Confirm from 'react-confirm-bootstrap'

export default class Requests extends Component {
  renderOrganizationArea(request) {
    const { context } = this.props
    const {requests, requestStatuses, branches, organizations} = context.props

    if(context.props.userIsOrganization)
      return

    return branches.find( branch => branch._id === organizations
      .find( organization => organization._id === request.loanInformation.liCreditOrganization)
      .profile.branch)
      .shortName
  }

  renderOrganization(request) {
    const { context } = this.props
    const {requests, requestStatuses, branches, organizations} = context.props

    if(context.props.userIsOrganization)
      return

    return organizations.find(area => area._id === request.loanInformation.liCreditOrganization).profile.shortName
  }

  renderRequestsTable() {
    const { context, locStrings } = this.props
    const { requests, requestStatuses, branches, organizations, userIsOrganization } = context.props
    let { filterOrganizations, filterBranches, filterStatus } = context.state

    const filteredRequests = requests.filter(request => {
      if(userIsOrganization) {
        if(!(organizations.find( organization => organization._id === request.author)))
        return false
      }

      //filtering by organizations
      if(!(new RegExp(filterOrganizations, 'i')).test(request.loanInformation.liCreditOrganization)) {
          return false;
      }

      //filtering by status
      if(!(new RegExp(filterStatus, 'i')).test(request.status)) {
          return false;
      }

      //filtering by branches
      if(
        filterBranches.length
        && !filterBranches.find(branch => branch.value === organizations.find(area => area._id === request.loanInformation.liCreditOrganization).profile.branch)
      )
        return false

      return true
    })

    return filteredRequests.map((request, index) => (
      <tr className="gradeX" key={ index }>
        <td>{request.requestNumber}</td>
        <td>{request.loanInformation.liRequestAdmissionDate}</td>
        <td style={{display: !userIsOrganization ? '' : 'none'}}>{this.renderOrganizationArea(request)}</td>
        <td style={{display: !userIsOrganization ? '' : 'none'}}>{this.renderOrganization(request)}</td>
        <td>{
          request.borrowerInformation.borrowerInformationLegalEntity.biOrganizationName ||
          request.borrowerInformation.borrowerInformationIndividualPerson.biipfFio
        }</td>
        <td className="text-center p-0">
          <span className={`label request-status ${requestStatuses.find(status => status.code === request.status).class}`}>
            { context.props.lang === 'ru' ? requestStatuses.find(status => status.code === request.status).nameRu :
                                            requestStatuses.find(status => status.code === request.status).nameKz}
          </span>
        </td>
        <td className="actions text-center">

          <Link
            to={`/requests/newRequest/${request._id}`}
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
          >
            <i className="fa fa-pencil" />
          </Link>

          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={() => context.deleteRequest(request._id)}
            body={locStrings.reqDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.reqDelete}
          >
              <a
                href="#"
                className="on-default remove-row"
                onClick={ e => {
                  e.preventDefault()
                } }
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props
    const { requests, requestStatuses, branches, organizations, userIsOrganization } = context.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.requests}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.requests}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-1">
                      <div className="m-b-30">
                        <Link
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          className="btn btn-primary waves-effect waves-light"
                          to={ context.state.toRequest ? `/requests/newRequest/${context.state.toRequest}` : "/requests/newRequest"}
                        >
                          { context.state.buttonLabel }
                          &nbsp;
                          <i className={ (context.state.buttonLabel === locStrings.reqFillContinueFalse) ? "mdi mdi-plus" : "mdi mdi-arrow-right"}></i>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-info panel-color bx-shadow-none">
                    <div className="panel-heading" role="tab" id="headingOne">
                      <h4 className="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          {locStrings.filters}
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" className="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                      <div className="panel-body panel-body-bordered">
                        <div className="row">
                          <div className="col-sm-4">
                            <div className="form-group">
                              <label className="col-md-2 control-label">{locStrings.branch}</label>
                              <div className="col-md-10">
                                <Select
                                  options={ context.branchOptions }
                                  onChange={ values => context.setState({ filterBranches: values }) }
                                  value={ context.state.filterBranches }
                                  multi
                                />
                              </div>
                            </div>
                          </div>
                          <div className="col-sm-4">
                            <div className="form-group">
                              <label className="col-md-2 control-label">{locStrings.organizations}</label>
                              <div className="col-md-10">
                                <Select
                                  options={ context.organizationOptions }
                                  onChange={ ({ value }) => context.setState({ filterOrganizations: value }) }
                                  value={ context.state.filterOrganizations }
                                  clearable={false}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="col-sm-4">
                            <div className="form-group">
                              <label className="col-md-2 control-label">Статус</label>
                              <div className="col-md-10">
                                <Select
                                  options={ context.requestStatusOptions }
                                  onChange={ ({ value }) => context.setState({ filterStatus: value }) }
                                  value={ context.state.filterStatus }
                                  clearable={false}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.reqNumber}</th>
                          <th>{locStrings.reqAdmissionDate}</th>
                          <th style={{display: !userIsOrganization ? '' : 'none'}}>{locStrings.reqArea}</th>
                          <th style={{display: !userIsOrganization ? '' : 'none'}}>{locStrings.organizations}</th>
                          <th>{locStrings.reqBorrower}</th>
                          <th className="text-center">{locStrings.reqStatus}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderRequestsTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
