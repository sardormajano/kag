import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import AsyncInput from '/client/containers/fuc/AsyncInputContainer'
import Select from 'react-select'

export default class CreateUserModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { locStrings, lang } = context.props
    const allFieldsFilledOut = context.state.cumIin
                                && context.state.cumFirstName
                                && context.state.cumLastName
                                && context.state.cumPassword
                                && context.state.cumEmail
                                && context.state.cumMobilePhone
                                && context.state.cumRoles.length
                                && context.state.cumBranch

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].userCreate}</h4>
            </div>
            <div className="modal-body">
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].userIin} *</label>
                      <AsyncInput
                        context={ context }
                        stateName='cumIin'
                        checkMethod='iin.bin.exists'
                        label='БИН'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].userName} *</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='cumFirstName'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].userSurname} *</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='cumLastName'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].userMiddleName}</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='cumMiddleName'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].password} *</label>
                      <Input
                        type="password"
                        className="form-control"
                        context={ context }
                        stateName='cumPassword'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].email} *</label>
                      <AsyncInput
                        context={ context }
                        stateName='cumEmail'
                        checkMethod='email.exists'
                        label='Эл. почта'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-5" className="control-label">{locStrings[lang].mobile} *</label>
                      <AsyncInput
                        context={ context }
                        stateName='cumMobilePhone'
                        checkMethod='mobile.phone.exists'
                        label='Мобильный телефон'
                        data-mask="+7(999) 999-99-99"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].role} *</label>
                      <Select
                        options={ context.roleOptions }
                        onChange={ values => context.setState({ cumRoles: values }) }
                        value={ context.state.cumRoles }
                        multi
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].branch} *</label>
                      <Select
                        options={ context.branchOptions }
                        onChange={ ({ value }) => context.setState({ cumBranch: value, branchId: value }) }
                        value={ context.state.cumBranch }
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div
                    style={{
                      display: context.branchCapital ? '' : 'none'
                    }}
                    className="col-md-6"
                  >
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].department}</label>
                      <Select
                        options={ context.departmentOptions }
                        onChange={ ({ value }) => context.setState({ cumDepartment: value }) }
                        value={ context.state.cumDepartment }
                      />
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div className="modal-footer">
              <button
                style={{ zIndex: 0 }}
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                style={{ zIndex: 0 }}
                type="button"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                className="btn btn-primary waves-effect waves-light"
                onClick={ () => {
                  this.closeButton.click()
                  context.insertUser()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
