import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import AsyncInput from '/client/containers/fuc/AsyncInputContainer'
import Select from 'react-select'

export default class EditUserModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { locStrings, lang } = context.props
    const allFieldsFilledOut = context.state.eumIin
                                && context.state.eumFirstName
                                && context.state.eumLastName
                                && context.state.eumPassword
                                && context.state.eumEmail
                                && context.state.eumMobilePhone
                                && context.state.eumRoles.length
                                && context.state.eumBranch

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].userEdit}</h4>
            </div>
            <div className="modal-body">
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].userIin} *</label>
                      <AsyncInput
                        context={ context }
                        stateName='eumIin'
                        checkMethod='iin.bin.exists'
                        checkException={ context.state.eumUserId }
                        label='ИИН'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].userName} *</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eumFirstName'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].userSurname} *</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eumLastName'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-1" className="control-label">{locStrings[lang].userMiddleName}</label>
                      <Input
                        className="form-control"
                        context={ context }
                        stateName='eumMiddleName'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].password} *</label>
                      <Input
                        type="password"
                        className="form-control"
                        context={ context }
                        stateName='eumPassword'
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].email} *</label>
                      <AsyncInput
                        context={ context }
                        stateName='eumEmail'
                        checkMethod='email.exists'
                        checkException={ context.state.eumUserId }
                        label='Эл. почта'
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="field-5" className="control-label">{locStrings[lang].mobile} *</label>
                      <AsyncInput
                        context={ context }
                        stateName='eumMobilePhone'
                        checkMethod='mobile.phone.exists'
                        checkException={ context.state.eumUserId }
                        label='Мобильный телефон'
                        data-mask="+7(999) 999-99-99"
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].role} *</label>
                      <Select
                        options={ context.roleOptions }
                        onChange={ values => context.setState({ eumRoles: values }) }
                        value={ context.state.eumRoles }
                        multi
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].branch} *</label>
                      <Select
                        options={ context.branchOptions }
                        onChange={ ({ value }) => context.setState({ eumBranch: value, branchId: value }) }
                        value={ context.state.eumBranch }
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div
                    style={{
                      display: context.branchCapital ? '' : 'none'
                    }}
                    className="col-md-12">
                    <div className="form-group">
                      <label htmlFor="field-2" className="control-label">{locStrings[lang].department}</label>
                      <Select
                        options={ context.departmentOptions }
                        onChange={ ({ value }) => context.setState({ eumDepartment: value }) }
                        value={ context.state.eumDepartment }
                      />
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div className="modal-footer">
              <button
                style={{ zIndex: 0 }}
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                style={{ zIndex: 0 }}
                type="button"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                className="btn btn-primary waves-effect waves-light"
                onClick={ () => {
                  this.closeButton.click()
                  context.updateUser()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
