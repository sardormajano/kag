import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'

export default class EditRoleModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { locStrings, lang } = context.props
    const { ermPairs } = context.state
    const allFieldsFilledOut = context.state.ermName
                                && ermPairs.length
                                && ermPairs[0].page
                                && ermPairs[0].actions.length

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].roleEdit}</h4>
            </div>
            <div className="modal-body">
              <form className="form-horizontal" role="form">
                <div className="form-group">
                  <label className="col-md-2 control-label">{locStrings[lang].name}</label>
                  <div className="col-md-10">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='ermName'
                    />
                  </div>
                </div>
                { ermPairs.map((pair, index) => (
                  <div key={ index } className="form-group">
                    <div className="col-md-6">
                      <Select
                        options={ context.pageOptions }
                        value={ pair.page }
                        onChange={ ({ value }) => context.setModalDataPair(index, 'page', value, true) }
                      />
                    </div>
                    <div className="col-md-6">
                      <Select
                        options={ context.actionOptions }
                        value={ pair.actions }
                        multi
                        onChange={ values => context.setModalDataPair(index, 'actions', values, true) }
                      />
                    </div>
                  </div>
                )) }
                <button
                  className="btn btn-purple"
                  onClick={ (e) => {
                    e.preventDefault()
                    context.setState({ ermPairs: [...context.state.ermPairs, { page: '', actions: [] }] })
                  } }
                >
                  {locStrings[lang].addPage}
                </button>
              </form>
            </div>
            <div className="modal-footer">
              <button
                style={{ zIndex: 0 }}
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                style={{ zIndex: 0 }}
                type="button"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                className="btn btn-primary waves-effect waves-light"
                onClick={ () => {
                  this.closeButton.click()
                  context.updateRole()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
