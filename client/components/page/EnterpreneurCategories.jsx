import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateEnterpreneurCategoryModal from '/client/components/page/enterpreneurCategories/CreateEnterpreneurCategoryModal'
import EditEnterpreneurCategoryModal from '/client/components/page/enterpreneurCategories/EditEnterpreneurCategoryModal'

import Confirm from 'react-confirm-bootstrap'

export default class EnterpreneurCategories extends Component {
  renderEnterpreneurCategoriesTable() {
    const { context, locStrings } = this.props
    const { enterpreneurCategories } = context.props

    return enterpreneurCategories.map((enterpreneurCategory, index) => (
      <tr className="gradeX" key={ index }>
        <td>{enterpreneurCategory.name.ru}</td>
        <td>{enterpreneurCategory.name.kz}</td>
        <td>{enterpreneurCategory.creditLimit}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-enterpreneurCategory-modal"
            onClick={() => {
              context.setState({
                emNameRu: enterpreneurCategory.name.ru,
                emNameKz: enterpreneurCategory.name.kz,
                emCreditLimit: enterpreneurCategory.creditLimit,
                emEnterpreneurCategoryId: enterpreneurCategory._id
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={ () => context.deleteEnterpreneurCategory(enterpreneurCategory._id) }
            body={locStrings.enterpreneurCategoryDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.enterpreneurCategoryDelete}
          >
            <a
              href="#"
              className="on-default remove-row"
              title={locStrings.delete}
              onClick={ e => {
                e.preventDefault()
              } }
            >
              <i className="fa fa-trash-o" />
            </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.enterpreneurCategories}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.enterpreneurCategories}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-enterpreneurCategory-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th>{locStrings.creditLimit}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderEnterpreneurCategoriesTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateEnterpreneurCategoryModal
                id='create-enterpreneurCategory-modal'
                context={ context }
              />
              <EditEnterpreneurCategoryModal
                id='edit-enterpreneurCategory-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
