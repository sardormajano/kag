import React, { Component } from 'react'
import moment from 'moment'

import Footer from '/client/components/fuc/Footer'
import PayloadModal from '/client/components/page/logs/PayloadModal'

import NumericInput from 'react-numeric-input';
import Input from '/client/components/fuc/Input'
import DatePicker from '/client/components/fuc/DatePicker'

export default class Logs extends Component {
  renderLogsTable() {
    const { context, locStrings } = this.props
    const { logs } = context.state
    let { filterIin, filterFio, filterIp, filterDate } = context.state

    if(!logs)
      return []

    const filteredLogs = logs.filter(log => {
      //filtering by IIN
      if(!filterIin) {
        filterIin = ''
      }

      if(!(new RegExp(filterIin, 'i')).test(log.userIIN)) {
          return false;
      }

      //filtering by FIO
      if(!(new RegExp(filterFio, 'i')).test(log.userFIO)) {
          return false;
      }

      //filtering by IP
      if(!(new RegExp(filterIp, 'i')).test(log.userIp)) {
          return false;
      }

      //filtering by DATE
      if(!(new RegExp(filterDate, 'i')).test(moment(log.createdAt).format('DD/MM/YYYY'))) {
          return false;
      }

      return true
    })

    return filteredLogs.map((log, index) => (
      <tr className="gradeX" key={ index }>
        <td>{ log.userIIN }</td>
        <td>{ log.userFIO }</td>
        <td>{ log.service }</td>
        <td>{ new Date(log.createdAt).toLocaleString('RU-ru') }</td>
        <td>{ log.userIp }</td>
        <td>
          <a
            href="#"
            onClick={ e => {
              context.setState({ payload: log.payload })
            } }
            className="btn btn-info"
            data-toggle="modal"
            data-target="#payload-modal"
          >
            {locStrings.viewContent}
          </a>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.logs}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.logs}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="panel panel-info">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          {locStrings.filters}
                        </a>
                      </h4>
                    </div>
                    <div className="panel-body">
                      <div className="row">
                        <div className="col-sm-3">
                          <div className="form-group">
                            <label className="col-md-2 control-label">IIN</label>
                            <div className="col-md-10">
                              <NumericInput
                                onChange={ val => context.setState({ filterIin: val }) }
                                value={ context.state.filterIin }
                                placeholder={locStrings.typeIin}
                                className="form-control"
                                style={ false }
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-3">
                          <div className="form-group">
                            <label className="col-md-2 control-label">FIO</label>
                            <div className="col-md-10">
                              <Input
                                type="text"
                                placeholder={locStrings.typeFioSearch}
                                className="form-control"
                                context={ context }
                                stateName='filterFio'
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-3">
                          <div className="form-group">
                            <label className="col-md-2 control-label">DATE</label>
                            <div className="col-md-10">
                              <DatePicker
                                context={ context }
                                stateName='filterDate'
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-3">
                          <div className="form-group">
                            <label className="col-md-2 control-label">IP</label>
                            <div className="col-md-10">
                              <Input
                                type="text"
                                placeholder={locStrings.typeIpSearch}
                                className="form-control"
                                context={ context }
                                stateName='filterIp'
                              />
                            </div>
                          </div>
                        </div>
                      </div><br />
                      <div className="row">
                        <div className="col-sm-12">
                          <div className="col-md-12" style={{display: 'flex', justifyContent: 'flex-end'}}>
                            <button
                              className="btn btn-primary waves-effect waves-light"
                              onClick={() => context.setState({
                                filterIin: '',
                                filterFio: '',
                                filterDate: '',
                                filterIp: ''
                              })}>
                              Сбросить фильтр</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <div className="row">
                      <div className="col-sm-6">
                        <form className="form-inline" role="form">
                          <div className="form-group">
                            <label>
                              {locStrings.page}:
                            </label>{' '}
                            <NumericInput
                              onChange={ val => context.setState({ page: val }) }
                              value={ context.state.page }
                              className="form-control"
                              style={ false }
                              min={ 1 }
                            />
                          </div>
                          <div className="form-group m-l-10">
                            <label>
                              {locStrings.amount}:
                            </label>{' '}
                            <NumericInput
                              placeholder={locStrings.amount}
                              onChange={ val => context.setState({ pageSize: val }) }
                              value={ context.state.pageSize }
                              className="form-control"
                              style={ false }
                              min={ 1 }
                            />
                          </div>
                          <button
                            type="submit"
                            className="btn btn-default waves-effect waves-light m-l-10 btn-md"
                            onClick={ context.getPage.bind(context) }
                          >
                            {locStrings.ok}
                          </button>
                        </form>
                      </div>
                    </div>
                    <br />
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>IIN</th>
                          <th>FIO</th>
                          <th>Service</th>
                          <th>Datetime</th>
                          <th>IP</th>
                          <th>Payload</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderLogsTable()}
                      </tbody>
                    </table>
                    <br />
                    <form className="form-inline" role="form">
                      <div className="form-group">
                        <label>
                          {locStrings.page}:
                        </label>{' '}
                        <NumericInput
                          onChange={ val => context.setState({ page: val }) }
                          value={ context.state.page }
                          className="form-control"
                          style={ false }
                          min={ 1 }
                        />
                      </div>
                      <div className="form-group m-l-10">
                        <label>
                          {locStrings.amount}:
                        </label>{' '}
                        <NumericInput
                          placeholder={locStrings.page}
                          onChange={ val => context.setState({ pageSize: val }) }
                          value={ context.state.pageSize }
                          className="form-control"
                          style={ false }
                          min={ 1 }
                        />
                      </div>
                      <button
                        type="submit"
                        className="btn btn-default waves-effect waves-light m-l-10 btn-md"
                        onClick={ context.getPage.bind(context) }
                      >
                        {locStrings.ok}
                      </button>
                    </form>
                    <br />
                  </div>
                </div>
                {/* end: page */}
              </div>
            </div>
          </div>
          <Footer />
        </div>
        <PayloadModal
          context={ context }
          id="payload-modal"
        />
      </div>
    )
  }
}
