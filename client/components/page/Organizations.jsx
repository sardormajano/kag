import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateOrganizationModal from '/client/components/page/organizations/CreateOrganizationModal'
import EditOrganizationModal from '/client/components/page/organizations/EditOrganizationModal'

import { idToName, passwordPlaceholder } from '/client/lib/collectionsRelated'

import Confirm from 'react-confirm-bootstrap'
import Input from '/client/components/fuc/Input'
import Select from 'react-select'
import NumericInput from 'react-numeric-input'

export default class Organizations extends Component {
  renderOrganizationsTable() {
    const { context, locStrings } = this.props
    const {
      branches,
      departments,
      roles,
      organizations,
    } = context.props
    let { filterBin, filterText, filterBranches } = context.state

    const filteredOrganizations = organizations.filter(organization => {
      //filtering by word
      if(
        !(new RegExp(filterText, 'i')).test(organization.profile.name ? organization.profile.name.ru : '')
        && !(new RegExp(filterText, 'i')).test(organization.profile.name ? organization.profile.name.kz : '')
        && !(new RegExp(filterText, 'i')).test(organization.profile.shortName)
      ) {
          return false;
      }

      //filtering by BIN
      if(!filterBin) {
        filterBin = ''
      }

      if(!(new RegExp(filterBin, 'i')).test(organization.username)) {
          return false;
      }

      //filtering by branches
      if(
        filterBranches.length
        && !filterBranches.find(branch => branch.value === organization.profile.branch)
      )
        return false

      return true
    })

    return filteredOrganizations.map((organization, index) => (
      <tr className="gradeX" key={ index }>
        <td>
          { organization.profile.shortName }
        </td>
        <td>{ idToName(organization.profile.branch, branches, 'name', 'ru') }</td>
        <td>{ organization.profile.phone }, { organization.profile.mobilePhone }</td>
        <td>{ organization.emails[0].address }</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-organization-modal"
            onClick={() => {
              context.setState({
                eomNameKz: organization.profile.name ? organization.profile.name.kz : '',
                eomNameRu: organization.profile.name ? organization.profile.name.ru : '',
                eomShortName: organization.profile.shortName,
                eomEmail: organization.emails[0].address,
                eomRepresentative: organization.profile.representative,
                eomPassword: passwordPlaceholder,
                eomMobilePhone: organization.profile.mobilePhone,
                eomPhone: organization.profile.phone,
                eomBin: organization.username,
                eomAddress: organization.profile.address,
                eomBranch: organization.profile.branch,
                eomRole: organization.profile.role,
                eomOrganizationId: organization._id,
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={() => context.deleteOrganization(organization._id)}
            body={locStrings.organizationDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.organizationDelete}
          >
            <a
              href="#"
              className="on-default remove-row"
              title={locStrings.delete}
              onClick={ e => {
                e.preventDefault()
              } }
            >
              <i className="fa fa-trash-o" />
            </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.organizations}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.organizations}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-organization-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-info">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          {locStrings.filters}
                        </a>
                      </h4>
                    </div>
                    <div className="panel-body">
                      <div className="row">
                        <div className="col-sm-4">
                          <div className="form-group">
                            <label className="col-md-2 control-label">{locStrings.branch}</label>
                            <div className="col-md-10">
                              <Select
                                options={ context.branchOptions }
                                onChange={ values => context.setState({ filterBranches: values }) }
                                value={ context.state.filterBranches }
                                multi
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-4">
                          <div className="form-group">
                            <label className="col-md-2 control-label">{locStrings.bin}</label>
                            <div className="col-md-10">
                              <NumericInput
                                onChange={ val => context.setState({ filterBin: val }) }
                                value={ context.state.filterBin }
                                placeholder={locStrings.typeBin}
                                className="form-control"
                                style={ false }
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-4">
                          <div className="form-group">
                            <label className="col-md-2 control-label">{locStrings.name}</label>
                            <div className="col-md-10">
                              <Input
                                type="text"
                                placeholder={locStrings.typeTextToSearch}
                                className="form-control"
                                context={ context }
                                stateName='filterText'
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.denomination}</th>
                          <th>{locStrings.direction}</th>
                          <th>{locStrings.phone}</th>
                          <th>{locStrings.email}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderOrganizationsTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateOrganizationModal
                id='create-organization-modal'
                context={ context }
              />
              <EditOrganizationModal
                id='edit-organization-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
