import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateActivityModal from '/client/components/page/activities/CreateActivityModal'
import EditActivityModal from '/client/components/page/activities/EditActivityModal'

import Confirm from 'react-confirm-bootstrap'
import Input from '/client/components/fuc/Input'
import NumericInput from 'react-numeric-input';

export default class Activities extends Component {
  renderActivitiesTable() {
    const { context, locStrings } = this.props
    const { activities } = context.state

    if(!activities)
      return []

    return activities.map((activity, index) => (
      <tr className="gradeX" key={ index }>
        <td>{activity.Id}</td>
        <td>{activity.NameRus}</td>
        <td>{activity.NameKaz}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-activity-modal"
            onClick={() => {
              context.setState({
                emNameRus: activity.NameRus,
                emNameKaz: activity.NameKaz,
                emId: activity.Id,
                emCode: activity.Code,
                emParent: activity.Parent,
                emAreaType: activity.AreaType,
                em_id: activity._id
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={() => context.deleteActivity(activity._id)}
            body={locStrings.activityDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.activityDelete}
          >
              <a
                href="#"
                className="on-default remove-row"
                title={locStrings.delete}
                onClick={ e => {
                  e.preventDefault()
                } }
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.activities}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.activities}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-activity-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          onClick={ context.updateKato.bind(context) }
                          className="btn btn-info waves-effect waves-light"
                        >
                          {locStrings.databaseUpdate} { ' ' }
                          <i className="mdi mdi-database" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <div className="row">
                      <div className="col-sm-6">
                        <form className="form-inline" role="form">
                          <div className="form-group">
                            <Input
                              stateName='searchText'
                              context={ context }
                              className="form-control"
                              placeholder={locStrings.typeTextToSearch}
                            />
                          </div>
                          <button
                            type="submit"
                            className="btn btn-default waves-effect waves-light m-l-10 btn-md"
                            onClick={ context.searchActivity.bind(context) }
                          >
                            {locStrings.search}
                          </button>
                        </form>
                      </div>
                      <div className="col-sm-6">
                        <form className="form-inline" role="form">
                          <div className="form-group">
                            <label>
                              {locStrings.page}:
                            </label>{' '}
                            <NumericInput
                              onChange={ val => context.setState({ page: val }) }
                              value={ context.state.page }
                              className="form-control"
                              style={ false }
                              min={ 1 }
                            />
                          </div>
                          <div className="form-group m-l-10">
                            <label>
                              {locStrings.amount}:
                            </label>{' '}
                            <NumericInput
                              placeholder={locStrings.amount}
                              onChange={ val => context.setState({ pageSize: val }) }
                              value={ context.state.pageSize }
                              className="form-control"
                              style={ false }
                              min={ 1 }
                            />
                          </div>
                          <button
                            type="submit"
                            className="btn btn-default waves-effect waves-light m-l-10 btn-md"
                            onClick={ context.getPage.bind(context) }
                          >
                            {locStrings.ok}
                          </button>
                        </form>
                      </div>
                    </div>
                    <br />
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderActivitiesTable()}
                      </tbody>
                    </table>
                    <br />
                    <form className="form-inline" role="form">
                      <div className="form-group">
                        <label>
                          {locStrings.page}:
                        </label>{' '}
                        <NumericInput
                          onChange={ val => context.setState({ page: val }) }
                          value={ context.state.page }
                          className="form-control"
                          style={ false }
                          min={ 1 }
                        />
                      </div>
                      <div className="form-group m-l-10">
                        <label>
                          {locStrings.amount}:
                        </label>{' '}
                        <NumericInput
                          placeholder={locStrings.amount}
                          onChange={ val => context.setState({ pageSize: val }) }
                          value={ context.state.pageSize }
                          className="form-control"
                          style={ false }
                          min={ 1 }
                        />
                      </div>
                      <button
                        type="submit"
                        className="btn btn-default waves-effect waves-light m-l-10 btn-md"
                        onClick={ context.getPage.bind(context) }
                      >
                        {locStrings.ok}
                      </button>
                    </form>
                    <br />
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateActivityModal
                id='create-activity-modal'
                context={ context }
              />
              <EditActivityModal
                id='edit-activity-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
