import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateEnterpreneurTypeModal from '/client/components/page/enterpreneurTypes/CreateEnterpreneurTypeModal'
import EditEnterpreneurTypeModal from '/client/components/page/enterpreneurTypes/EditEnterpreneurTypeModal'

import Confirm from 'react-confirm-bootstrap'

export default class EnterpreneurTypes extends Component {
  renderEnterpreneurTypesTable() {
    const { context, locStrings } = this.props
    const { enterpreneurTypes } = context.props

    return enterpreneurTypes.map((enterpreneurType, index) => (
      <tr className="gradeX" key={ index }>
        <td>{enterpreneurType.name.ru}</td>
        <td>{enterpreneurType.name.kz}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-enterpreneurType-modal"
            onClick={() => {
              context.setState({
                emNameRu: enterpreneurType.name.ru,
                emNameKz: enterpreneurType.name.kz,
                emEnterpreneurTypeId: enterpreneurType._id
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={ () => context.deleteEnterpreneurType(enterpreneurType._id) }
            body={locStrings.enterpreneurTypeDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.enterpreneurTypeDelete}
          >
            <a
              href="#"
              className="on-default remove-row"
              title={locStrings.delete}
              onClick={ e => {
                e.preventDefault()
              } }
            >
              <i className="fa fa-trash-o" />
            </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.enterpreneurTypes}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.enterpreneurTypes}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-enterpreneurType-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderEnterpreneurTypesTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateEnterpreneurTypeModal
                id='create-enterpreneurType-modal'
                context={ context }
              />
              <EditEnterpreneurTypeModal
                id='edit-enterpreneurType-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
