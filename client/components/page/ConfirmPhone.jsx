import React from 'react'
import { Link } from 'react-router-dom'
import Input from '/client/components/fuc/Input'
import AsyncInput from '/client/containers/fuc/AsyncInputContainer'

export default (props) => {
  return (
    <section>
        <div className="container-alt">
          <div className="row">
            <div className="col-sm-12">
              <div className="wrapper-page">
                <div className="m-t-40 account-pages">
                  <div className="text-center account-logo-box">
                    <h2 className="text-uppercase">
                      <a href="index.html" className="text-success">
                        <span><img src="assets/images/logo.svg" alt height={36} /></span>
                      </a>
                    </h2>
                    {/*<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>*/}
                  </div>
                  <div className="account-content">
                    <form className="form-horizontal" action="#">
                      <div className="form-group ">
                        <div className="col-xs-12">
                          <Input
                            className="form-control"
                            required
                            placeholder="Код ХХХХ"
                            context={ props.context }
                            stateName='code'
                          />
                        </div>
                      </div>
                      <div className="form-group text-center m-t-30">
                        <div className="col-sm-12">
                          <p className="text-muted">{`${props.locStrings.confirmPhoneTitle1} ${props.context.state.hiddenPhone} ${props.locStrings.confirmPhoneTitle2}`}</p>
                        </div>
                      </div>
                      <div
                        className="alert alert-info"
                        role="alert"
                        style={{
                          display: props.context.state.alertInfo ? '' : 'none'
                        }}
                      >
                        { props.context.state.alertInfo }
                      </div>
                      <div
                        className="alert alert-danger"
                        role="alert"
                        style={{
                          display: props.context.state.alertDanger ? '' : 'none'
                        }}
                      >
                        { props.context.state.alertDanger }
                      </div>
                      <div className="form-group text-center m-t-10">
                        <div className="col-xs-12">
                          <button
                            className="btn w-md btn-bordered btn-danger waves-effect waves-light"
                            type="submit"
                            onClick={ props.context.verifyPhone.bind(props.context) }
                          >
                            {props.locStrings.confirm}
                          </button>
                        </div>
                      </div>
                      <div className="form-group text-center m-t-30">
                        <div className="col-sm-12">
                          <p className="text-muted">{props.locStrings.noCode}</p>
                        </div>
                      </div>
                      <div className="form-group text-center m-t-30">
                        <div className="row">
                          <div className="col-md-6">
                            <a
                              href="#"
                              className="btn btn-white text-muted m-b-0 font-13"
                              onClick={ props.context.resendCode.bind(props.context) }
                            >
                              {props.locStrings.resend}
                            </a>
                          </div>
                          <div className="col-md-6">
                            <a
                              href="#"
                              className="btn btn-white text-muted m-b-0 font-13"
                              onClick={() => props.context.setState({changeNumber: true})}
                            >
                              {props.locStrings.changeNumber}
                            </a>
                          </div>
                        </div>
                      </div>
                      <div id="newNumberInputGroup" style={{display: props.context.state.changeNumber ? '' : 'none'}}>
                        <div className="form-group">
                          <div className="col-xs-12">
                            <AsyncInput
                              context={ props.context }
                              stateName='mobilePhone'
                              checkMethod='mobile.phone.exists'
                              label='Мобильный телефон'
                              data-mask="+7(999) 999-99-99"
                            />
                          </div>
                        </div>
                        <div className="form-group text-center m-t-10" id="newNumberInputButton">
                          <div className="col-xs-12">
                            <a
                              className="btn w-md btn-bordered btn-danger waves-effect waves-light"
                              onClick={props.context.changeNumber.bind(props.context)}
                              href="#"
                              >
                                {props.locStrings.change}
                              </a>
                          </div>
                        </div>
                      </div>
                    </form>
                    <div className="clearfix" />
                  </div>
                </div>
                {/* end card-box*/}
              </div>
              {/* end wrapper */}
            </div>
          </div>
        </div>
      </section>
  )
}
