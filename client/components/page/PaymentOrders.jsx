import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreatePaymentOrderModal from '/client/components/page/paymentOrders/CreatePaymentOrderModal'
import EditPaymentOrderModal from '/client/components/page/paymentOrders/EditPaymentOrderModal'

import Confirm from 'react-confirm-bootstrap'

export default class PaymentOrders extends Component {
  renderPaymentOrdersTable() {
    const { context, locStrings } = this.props
    const { paymentOrders } = context.props

    return paymentOrders.map((paymentOrder, index) => (
      <tr className="gradeX" key={ index }>
        <td>{paymentOrder.name.ru}</td>
        <td>{paymentOrder.name.kz}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-paymentOrder-modal"
            onClick={() => {
              context.setState({
                epmNameRu: paymentOrder.name.ru,
                epmNameKz: paymentOrder.name.kz,
                epmPaymentOrderId: paymentOrder._id
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={() => context.deletePaymentOrder(paymentOrder._id)}
            body={locStrings.paymentOrderDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.paymentOrderDelete}
          >
              <a
                href="#"
                className="on-default remove-row"
                title={locStrings.delete}
                onClick={ e => {
                  e.preventDefault()
                } }
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.paymentOrders}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.paymentOrders}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-paymentOrder-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderPaymentOrdersTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreatePaymentOrderModal
                id='create-paymentOrder-modal'
                context={ context }
              />
              <EditPaymentOrderModal
                id='edit-paymentOrder-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
