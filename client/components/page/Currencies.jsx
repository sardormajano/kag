import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateCurrencyModal from '/client/components/page/currencies/CreateCurrencyModal'
import EditCurrencyModal from '/client/components/page/currencies/EditCurrencyModal'

import Confirm from 'react-confirm-bootstrap'

export default class Currencies extends Component {
  renderCurrenciesTable() {
    const { context, locStrings } = this.props
    const { currencies } = context.props

    return currencies.map((currency, index) => (
      <tr className="gradeX" key={ index }>
        <td>{currency.name.ru}</td>
        <td>{currency.name.kz}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-currency-modal"
            onClick={() => {
              context.setState({
                ecmNameRu: currency.name.ru,
                ecmNameKz: currency.name.kz,
                ecmCurrencyId: currency._id
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={ () => context.deleteCurrency(currency._id) }
            body={locStrings.currencyDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.currencyDelete}
          >
            <a
              href="#"
              className="on-default remove-row"
              title={locStrings.delete}
              onClick={ e => {
                e.preventDefault()
              } }
            >
              <i className="fa fa-trash-o" />
            </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.currencies}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.currencies}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-currency-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderCurrenciesTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateCurrencyModal
                id='create-currency-modal'
                context={ context }
              />
              <EditCurrencyModal
                id='edit-currency-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
