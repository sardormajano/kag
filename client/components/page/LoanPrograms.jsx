import React, { Component } from 'react'

import Footer from '/client/components/fuc/Footer'
import CreateLoanProgramModal from '/client/components/page/loanPrograms/CreateLoanProgramModal'
import EditLoanProgramModal from '/client/components/page/loanPrograms/EditLoanProgramModal'

import Confirm from 'react-confirm-bootstrap'

export default class LoanPrograms extends Component {
  renderLoanProgramsTable() {
    const { context, locStrings } = this.props
    const { loanPrograms } = context.props

    return loanPrograms.map((loanProgram, index) => (
      <tr className="gradeX" key={ index }>
        <td>{loanProgram.name.ru}</td>
        <td>{loanProgram.name.kz}</td>
        <td className="actions text-center">
          <a
            href="#"
            style={{
              display: context.state.canCreateUpdate ? '' : 'none'
            }}
            className="on-default"
            title={locStrings.edit}
            data-toggle="modal"
            data-target="#edit-loanProgram-modal"
            onClick={() => {
              context.setState({
                ebmNameRu: loanProgram.name.ru,
                ebmNameKz: loanProgram.name.kz,
                ebmLoanProgramId: loanProgram._id,
              })
            }}
          >
            <i className="fa fa-pencil" />
          </a>
          <Confirm
            style={{
              display: context.state.canDelete ? '' : 'none'
            }}
            onConfirm={() => context.deleteLoanProgram(loanProgram._id)}
            body={locStrings.loanProgramDeleteConfirm}
            confirmText={locStrings.yes}
            cancelText={locStrings.cancel}
            title={locStrings.loanProgramDelete}
          >
              <a
                href="#"
                className="on-default remove-row"
                title={locStrings.delete}
                onClick={ e => {
                  e.preventDefault()
                } }
              >
                <i className="fa fa-trash-o" />
              </a>
          </Confirm>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.loanPrograms}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li className="active">
                        {locStrings.loanPrograms}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              <div className="panel">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="m-b-30">
                        <button
                          style={{
                            display: context.state.canCreateUpdate ? '' : 'none'
                          }}
                          data-toggle='modal'
                          data-target='#create-loanProgram-modal'
                          className="btn btn-primary waves-effect waves-light"
                        >
                          {locStrings.add}
                          <i className="mdi mdi-plus" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className>
                    <table className="table table-striped add-edit-table table-bordered" id="datatable-editable">
                      <thead>
                        <tr>
                          <th>{locStrings.ruName}</th>
                          <th>{locStrings.kzName}</th>
                          <th className="text-center">{locStrings.actions}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderLoanProgramsTable()}
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* end: page */}
              </div>
              <CreateLoanProgramModal
                id='create-loanProgram-modal'
                context={ context }
              />
              <EditLoanProgramModal
                id='edit-loanProgram-modal'
                context={ context }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
