import React from 'react'
import { Link } from 'react-router-dom'

import AsyncInput from '/client/containers/fuc/AsyncInputContainer'
import Input from '/client/components/fuc/Input'
import Select from 'react-select'
import { cookie } from '/client/lib/coreLib'

export default Signup = ({ context }) => (
  <section>
    <div className="container-alt">
      <div className="row">
        <div className="col-sm-12">
          <div className="wrapper-page">
            <div className="m-t-40 account-pages">
              <div className="text-center account-logo-box">
                <h2 className="text-uppercase">
                  <a href="index.html" className="text-success">
                    <span><img src="assets/images/logo.svg" alt height={36} /></span>
                  </a>
                </h2>
              </div>
              <div className="account-content">
                <form
                  className="form-horizontal"
                  action="#"
                  onSubmit={context.formSubmitHandler.bind(context)}
                >
                  <div className="form-group text-center">
                    <br />
                    <div className="radio radio-info radio-inline">
                      <input
                        type="radio"
                        id="ru"
                        defaultValue="option55"
                        name="lang"
                        checked={context.state.lang === 'ru'}
                        title="язык отображения"
                        onChange={(event) => {
                          cookie.set('lang', 'ru', 9999)
                          context.onLangChangeHandler(event)
                        }}
                      />
                      <label htmlFor="ru" title="выберите язык"> РУС </label>
                    </div>
                    <div className="radio radio-info radio-inline">
                      <input
                        type="radio"
                        id="kz"
                        defaultValue="option44"
                        name="lang"
                        checked={context.state.lang === 'kz'}
                        title="тілді таңдау"
                        onChange={(event) => {
                          cookie.set('lang', 'kz', 9999)
                          context.onLangChangeHandler(event)
                        }}
                      />
                      <label htmlFor="kz" title="тілді таңдаңыз"> ҚАЗ </label>
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].companyName}<span className="text-danger">*</span></label>
                      <Input
                        context={ context }
                        stateName='name'
                        className="form-control"
                        type="text"
                        required
                        placeholder="ТОО МФО КредитВсем"
                      />
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].companyEmail}<span className="text-danger">*</span></label>
                      <a href="javascript:;" tabIndex="-1" className="file-tooltip" title={context.props.locStrings[context.state.lang].companyEmailTitle}>
                        <i className="glyphicon glyphicon-question-sign" />
                      </a>
                      <Input
                        context={ context }
                        stateName='email'
                        className="form-control"
                        type="email"
                        required
                        placeholder="example@yourcompany.kz"
                      />
                    </div>
                  </div>
                  <div className="form-group ">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].companyBin}<span className="text-danger">*</span></label>
                      <AsyncInput
                        context={ context }
                        stateName='username'
                        checkMethod='iin.bin.exists'
                        label={context.props.locStrings[context.state.lang].companyBin}
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div
                      className="col-xs-12"
                      style={{ zIndex: 2 }}
                    >
                      <label htmlFor="field-2" className="control-label">{context.props.locStrings[context.state.lang].areaSelect} *</label>
                      <Select
                        options={ context.branchOptions }
                        onChange={ ({ value }) => context.setState({ branch: value }) }
                        value={ context.state.branch }
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].repFullName}<span className="text-danger">*</span></label>
                      <Input
                        context={ context }
                        stateName='representative'
                        className="form-control"
                        type="text"
                        required
                        placeholder="Бериков Серик"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].companyMobile}<span className="text-danger">*</span></label>
                      <a href="javascript:;" tabIndex="-1" className="file-tooltip" title={context.props.locStrings[context.state.lang].companyMobileTitle}>
                        <i className="glyphicon glyphicon-question-sign" />
                      </a>
                      <Input
                        context={ context }
                        stateName='mobilePhone'
                        className="form-control"
                        type="text"
                        required
                        placeholder={context.props.locStrings[context.state.lang].mobileFormat}
                        data-mask="+7(999) 999-99-99"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].typePassword}<span className="text-danger">*</span></label>
                      <Input
                        context={ context }
                        stateName='password'
                        className="form-control"
                        type="password"
                        required
                        placeholder={context.props.locStrings[context.state.lang].password}
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-xs-12">
                      <label className="control-label">{context.props.locStrings[context.state.lang].typePasswordAgain}<span className="text-danger">*</span></label>
                      <Input
                        context={ context }
                        stateName='passwordConfirm'
                        className="form-control"
                        type="password"
                        required
                        placeholder={context.props.locStrings[context.state.lang].typePasswordAgain}
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-xs-12">
                      <div className="checkbox checkbox-success">
                        <input
                          checked={ context.state.acceptConditions }
                          onChange={ e => context.setState({ acceptConditions: e.currentTarget.checked })}
                          id="checkbox-signup"
                          type="checkbox"
                        />
                        <label htmlFor="checkbox-signup">{context.props.locStrings[context.state.lang].accept} <a href="#">{context.props.locStrings[context.state.lang].conditions}</a></label>
                      </div>
                    </div>
                  </div>
                  <div className="form-group account-btn text-center m-t-10">
                    <div className="col-xs-12">
                      <button
                        className="btn w-md btn-danger btn-bordered waves-effect waves-light"
                        type="submit"
                        disabled={ !context.state.acceptConditions }
                      >
                        {context.props.locStrings[context.state.lang].signUp}
                      </button>
                    </div>
                  </div>
                </form>
                <div className="clearfix" />
              </div>
            </div>
            {/* end card-box*/}
            <div className="row m-t-50">
              <div className="col-sm-12 text-center">
                <p className="text-muted">{context.props.locStrings[context.state.lang].alreadyHaveAnAcc}
                  <Link
                    to='/login'
                    className="text-primary m-l-5"
                  >
                    {context.props.locStrings[context.state.lang].logIn}
                  </Link>
                </p>
              </div>
            </div>
          </div>
          {/* end wrapper */}
        </div>
      </div>
    </div>
  </section>
)
