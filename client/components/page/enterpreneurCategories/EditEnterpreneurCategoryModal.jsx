import React, { Component } from 'react'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'

function addSpaces(str) {
  if(!str || str === 'null')
    return ''

  str = str.trim().split(' ').join('')
  str = str.replace(/[^\d]/g, '')
  str = parseInt(str).toLocaleString('ru-RU')

  return str
}

export default class EditEnterpreneurCategoryModal extends Component {
  render() {
    const { context, ...rest } = this.props
    const { locStrings, lang } = context.props
    const { ermPairs } = context.state
    const allFieldsFilledOut = context.state.emNameRu && context.state.emNameKz

    return (
      <div
        { ...rest }
        className="modal fade"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 className="modal-title" >{locStrings[lang].enterpreneurCategoryEdit}</h4>
            </div>
            <div className="modal-body">
              <form className="form-horizontal" role="form">
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].ruName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='emNameRu'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].kzName}</label>
                  <div className="col-md-8">
                    <Input
                      type="text"
                      className="form-control"
                      context={ context }
                      stateName='emNameKz'
                    />

                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-4 control-label">{locStrings[lang].creditLimit}</label>
                  <div className="col-md-8">
                    <input
                      className="form-control"
                      onChange={ e => {
                        let val = e.currentTarget.value
                        val = addSpaces(val)
                        context.setState({ emCreditLimit: val })
                      }}
                      value={ context.state.emCreditLimit }
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default waves-effect"
                data-dismiss="modal"
                ref={ closeButton => this.closeButton = closeButton }
              >
                {locStrings[lang].close}
              </button>
              <button
                type="button"
                title={ !allFieldsFilledOut ? locStrings[lang].allFieldsFilledOut : '' }
                disabled={ !allFieldsFilledOut }
                className="btn btn-primary waves-effect waves-light"
                onClick={ () => {
                  this.closeButton.click()
                  context.updateEnterpreneurCategory()
                } }
              >{locStrings[lang].save}</button>
            </div>
          </div>{/* /.modal-content */}
        </div>{/* /.modal-dialog */}
      </div>
    )
  }
}
