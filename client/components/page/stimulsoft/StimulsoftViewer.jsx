import React, { Component } from 'react'

import { EducationsCollection } from '/api/educations'

export default class StimulsoftViewer extends Component {

  componentDidMount() {
    var requestId = this.props.match.params._id;
    var reportName = this.props.match.params._reportName;

    Meteor.call('request.get', requestId, (err, res) => {
      if(err) {
        console.log(err.reason)
      }
      else if(res) {
          //
          console.log("res");

          var report = new Stimulsoft.Report.StiReport();
          report.loadFile("/assets/reports/"+reportName+".mrt");

          // we can pass simple virable using this line of code
          // report.dictionary.variables.getByName("companyName").valueObject = "ТОО 'Рога и копыта'";    //

          // if we want to pass json object we should use such code
          //var data = res; // this.getReportData(res);

          const refOptions = {};
          refOptions.requestId = requestId;
          refOptions.creditOrganizationId = res.loanInformation.liCreditOrganization;
          refOptions.liPaymentOrderLoanId = res.loanInformation.liPaymentOrderLoan;
          refOptions.liPaymentOrderPercentId = res.loanInformation.liPaymentOrderPercent;

          refOptions.garantEducationId = res.borrowerInformation.borrowerInformationIndividualPerson.biipfEducation;
          refOptions.biEnterpreneurCategoryId = res.borrowerInformation.borrowerInformationLegalEntity.biEnterpreneurCategory;

          // todo: наверное надо научиться получать данные через подписку на коллекции
          // Meteor.subscribe('educations')
          // var edu = (EducationsCollection.find({ '_id' : (refOptions.garantEducationId) }).fetch())[0];
          // console.log(edu);


          // получение данных кредитной организации и добавление их в источник данных
          Meteor.call('reportRef.get', refOptions, (err, refs) => {
            if(err) {
              console.log(err.reason)
            }
            else if(refs) {

                // добавляем дополнительные данные в источник данных для отчета
                res.loanInformation.liCreditOrganizationName = refs.creditOrganizationNameRu;
                res.loanInformation.liPaymentOrderLoanName = refs.liPaymentOrderLoanNameRu;
                res.loanInformation.liPaymentOrderPercentName = refs.liPaymentOrderPercentNameRu;

                // если заёмщиком является юридическое лицо, то добавляем справочники по физическому лицу
                if (!!res.borrowerInformation && !!res.borrowerInformation.borrowerInformationIndividualPerson) {
                    res.borrowerInformation.borrowerInformationIndividualPerson.biipfEducationNameRu =
                        refs.garantEducationNameRu;
                }

                // если заёмщиком является юридическое лицо, то добавляем справочники по юридичесчкому лицу
                if (!!res.borrowerInformation && !!res.borrowerInformation.borrowerInformationLegalEntity) {
                    res.borrowerInformation.borrowerInformationLegalEntity.biEnterpreneurCategoryName =
                        refs.biEnterpreneurCategoryNameRu;
                }

                res.branchName = refs.branchNameRu;

                // формируем отчет
                var dataSet = new Stimulsoft.System.Data.DataSet("companyInfo");
                dataSet.readJson(res);
                report.dictionary.databases.clear();
                report.regData("companyInfo", "companyInfo", dataSet);

                // let's show the report to the user
                viewer.report = report;
                viewer.renderHtml("viewerContent");

        }
      })

      }
    })
  }




  render() {

    return (
      <div id="viewerContent">

      </div>
    )
  }
}
