import React from 'react'

import Footer from '/client/components/fuc/Footer'
export default Home = ({ context, locStrings }) => (
  <div>
    <div className="content-page">
      {/* Start content */}
      <div className="content">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="page-title-box">
                <h4 className="page-title">{locStrings.main}</h4>
                <ol className="breadcrumb p-0 m-0">
                  <li>
                    <a href="#">{locStrings.main}</a>
                  </li>
                  <li className="active">
                    {locStrings.currentPage}
                  </li>
                </ol>
                <div className="clearfix" />
              </div>
            </div>
          </div>
          {/* end row */}
        </div> {/* container */}
      </div> {/* content */}
      <Footer />
    </div>
  </div>
)
