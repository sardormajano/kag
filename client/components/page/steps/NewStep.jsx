import React, { Component } from 'react'

import Textarea from '/client/components/fuc/Textarea'
import Footer from '/client/components/fuc/Footer'

import Input from '/client/components/fuc/Input'
import Select from 'react-select'

import { Link } from 'react-router-dom'

export default class NewStep extends Component {
  renderDocumentTypesTable() {
    const { context, locStrings } = this.props
    const { documentTypes } = context.state
    const { levelDocumentUp, levelDocumentDown } = context

    return documentTypes.map((docType, index) => (
      <tr className="gradeX" key={ index }>
        <td>
          {docType.label}
        </td>
        <td className="actions text-center">
          <a
            href="#"
            title={locStrings.stepUp}
            className="on-default remove-row"
            onClick={ e =>  {
              e.preventDefault()
              levelDocumentUp.call(context, index)
            } }
          >
            <i className="fa fa-level-up" />
          </a>{' '}
          <a
            href="#"
            title={locStrings.stepDown}
            className="on-default remove-row"
            onClick={ e =>  {
              e.preventDefault()
              levelDocumentDown.call(context, index)
            } }
          >
            <i className="fa fa-level-down" />
          </a>
        </td>
      </tr>
    ))
  }

  render() {
    const { context, locStrings } = this.props

    return (
      <div>
        <div className="content-page">
          <div className="content">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <div className="page-title-box">
                    <h4 className="page-title">{locStrings.main}</h4>
                    <ol className="breadcrumb p-0 m-0">
                      <li>
                        <a href="#">{locStrings.main}</a>
                      </li>
                      <li>
                        <Link to='/steps'>{locStrings.steps}</Link>
                      </li>
                      <li className="active">
                        {locStrings.stepCreate}
                      </li>
                    </ol>
                    <div className="clearfix" />
                  </div>
                </div>
              </div>
              {/* end row */}
              {/* Basic Form Wizard */}
              <div className="row">
                <div className="col-md-12">
                  <div className="card-box">
                    <h4 className="m-t-0 header-title">
                      <Link
                        style={ { zIndex: 0 } }
                        type="submit"
                        className="btn btn-primary waves-effect waves-light"
                        to='/steps'
                      >
                        {locStrings.backToSteps}
                      </Link><br /><br /><br />
                      <b>{locStrings.stepCreateConfirm}</b>
                    </h4><br />
                    <form
                      role="form"
                      onSubmit={e => {
                        e.preventDefault()
                        context.createStep.call(context)
                      }}
                    >
                      <div className="form-group">
                        <label>{locStrings.name}</label>
                        <Input
                          className="form-control"
                          placeholder={locStrings.typeStepName}
                          context={ context }
                          stateName='name'
                        />
                      </div><br />
                      <div className="form-group">
                        <label>{locStrings.description}:</label>
                        <Textarea
                          className="form-control"
                          rows={4}
                          maxLength={500}
                          placeholder={locStrings.stepDescription}
                          context={ context }
                          stateName='description'
                        />
                      </div><br />
                      <div className="form-group">
                        <label htmlFor="field-2" className="control-label">{locStrings.docs} *</label>
                        <Select
                          options={ context.documentTypeOptions }
                          onChange={ values => context.setState({ documentTypes: values }) }
                          value={ context.state.documentTypes }
                          multi
                        />
                      </div><br />
                      <table
                        className="table table-striped add-edit-table table-bordered"
                        style={{ display: context.state.documentTypes.length ? '' : 'none' }}
                      >
                        <thead>
                          <tr>
                            <th>{locStrings.docName}</th>
                            <th className="text-center">{locStrings.actions}</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.renderDocumentTypesTable()}
                        </tbody>
                      </table>
                      <hr/>
                      <button
                        style={ { zIndex: 0 } }
                        type="submit"
                        className="btn btn-primary waves-effect waves-light"
                      >
                        {locStrings.save}
                      </button>
                    </form>
                  </div>
                </div>
              </div>
              {/* End row */}
            </div> {/* container */}
          </div> {/* content */}
        </div>
      </div>
    )
  }
}
