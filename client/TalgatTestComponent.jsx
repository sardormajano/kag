import React, { Component } from 'react';

import moment from 'moment'

let test1 = 'test1';
let test2 = 'test2';

export default class TalgatTestComponent extends Component {
  componentDidMount() {
    Meteor.call('generateRequestNumber','o94kwvYndgXNRTNiv', (error, result) => {
      test1 = result;
    });
    Meteor.call('generateRequestNumber','o94kwvYndgXNRTNiv','1987-03-12 12:25:32', (error, result) => {
      test2 = result;
    })
  }
  render() {
    return (
      <div className="container">
        <h1>Testing requestGenerator</h1>
        <h3>Test case 1 - branchId given, no date given, should default to current date</h3>
        <h4>branchID: o94kwvYndgXNRTNiv - AKT</h4>
        {test1}
        <h3>Test case 2 - branchId given, date 12.03.1987 given</h3>
        {test2}
      </div>
    )
  }
}
