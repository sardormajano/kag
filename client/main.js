import { Meteor } from 'meteor/meteor'
import React from 'react'
import { render } from 'react-dom'

import moment from 'moment'
import { MOMENT_RU, DATEPICKER_RU } from '/client/lib/locales'
import { iinCheck } from '/client/lib/formValidationRelated'

import App from '/client/components/App'

designer = new Stimulsoft.Designer.StiDesigner(null, "StiDesigner", false)
viewer = new Stimulsoft.Viewer.StiViewer(null, "StiViewer", false)
Stimulsoft.Base.Localization.StiLocalization.setLocalizationFile("/plugins/stimulsoft/Localization/ru.xml")

Meteor.startup(function() {
  moment.locale('ru', MOMENT_RU)

  $.fn.datepicker.dates['en'] = DATEPICKER_RU

  const root = document.createElement('div')
  root.setAttribute('id', 'root')
  document.body.appendChild(root)
  document.body.classList.add('fixed-left')

  render(<App />, root)
});

// test gitkraken commit string from Igor
