import {
    BranchesCollection
} from '/api/branches'
import moment from 'moment';

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

const requestNumberGenerator = (branchId, date) => {
    let branch = BranchesCollection.findOne({
        _id: branchId
    });
    let code = "";
    if (branch) {
        code += branch.code;
    }
    let tempDate = new Date();
    date = (typeof date === 'undefined') ? tempDate : date;
    code += "-";
    code += moment(date).format('DDMMYYYY');
    code += "-";
    let randomPart = randomString(4, '123456789ABCDEFGHJKLMNPQRSTUVWXYZ');
    let notUniqueNumber = BranchesCollection.findOne({
        requestNumber: code + randomPart
    });
    while (notUniqueNumber) {
        console.log('not unique request number generated, trying again. Duplicate with ' + notUniqueNumber);
        randomPart = randomString(4, '123456789ABCDEFGHJKLMNPQRSTUVWXYZ');
        notUniqueNumber = BranchesCollection.findOne({
            requestNumber: code + randomPart
        });
    }
    code += randomPart;
    // console.log('unique RN generated: ' + code);
    return code;
}

export default requestNumberGenerator;

Meteor.methods({
    'generateRequestNumber': function(branchId, date) {
        return requestNumberGenerator(branchId, date);
    }
});
