import { BranchesCollection } from '/api/branches'
import { BanksCollection } from '/api/banks'
import { CustomRolesCollection } from '/api/customRoles'
import { DocumentTypesCollection } from '/api/documentTypes'
import { PagesCollection } from '/api/pages'
import { RequestStatusesCollection } from '/api/requestStatuses'

export const insertDefaultDocumentTypes = (documentTypes) => {
  documentTypes.forEach(documentType => {
    if(!DocumentTypesCollection.find({ _id: documentType._id }).count()) {
      DocumentTypesCollection.insert(documentType)
    }
  })
}

export const insertDefaultUsers = (users) => {
  if(Meteor.users.find().count() !== 0)
    return

  users.forEach(user => {
    let tuser = Accounts.createUser(user)
    Roles.addUsersToRoles(tuser, ['cu', 'r', 'd'], 'all')
  });
}

export const insertDefaultPages = (pages) => {
  pages.forEach(page => {
    if(!PagesCollection.find({ route: page.route }).count()) {
      PagesCollection.insert(page)
    }
  })
}

export const insertDefaultBranches = (branches) => {
  if(BranchesCollection.find({ _id: '0' }).count() !== 0)
    return

  branches.forEach(branch => BranchesCollection.insert(branch))
}

export const insertDefaultRoles = (roles) => {
  roles.forEach(role => {
    if(!CustomRolesCollection.find({ _id: role._id }).count()) {
      CustomRolesCollection.insert(role)
    }
  })
}

export const insertDefaultRequestStatuses = (statuses) => {
  statuses.forEach(status => {
    if(!RequestStatusesCollection.find({ code: status.code }).count()) {
      RequestStatusesCollection.insert(status)
    }
  })
}

export const insertDefaultBanks = (banks) => {
  banks.forEach(bank => {
    if(!BanksCollection.find({ bik: bank.bik }).count()) {
      BanksCollection.insert(bank)
    }
  })
}
