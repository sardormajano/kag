import {
  SMSC_LOGIN,
  SMSC_PASSWORD,
  MAILGUN_LOGIN,
  MAILGUN_PASSWORD,
  MAILGUN_FROM,
  MAILGUN_API_KEY,
  MAILGUN_DOMAIN
} from '/server/lib/globalVariables'

import { HTTP } from 'meteor/http'

export const sendSMS = (phone, message, callback = () => {}) => {
  HTTP.call('POST', `http://smsc.kz/sys/send.php?charset=utf-8&login=${SMSC_LOGIN}&psw=${SMSC_PASSWORD}&phones=${phone}&mes=${encodeURI(message)}`, {
  }, (error, result) => {
    if (!error) {
      callback()
    } else {
      console.log(error)
    }
  });
}

export const sendEmail = function(to, subject, text, html) {
  const myGun = new Mailgun({
    apiKey: MAILGUN_API_KEY,
    domain: MAILGUN_DOMAIN
  });

  myGun.send({
     to, subject, text, html,
     'from': MAILGUN_FROM,
 });
}
