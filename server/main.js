import { Meteor } from 'meteor/meteor'
import { CustomRolesCollection } from '/api/customRoles'
import { PagesCollection } from '/api/pages'

import {
  insertDefaultBranches,
  insertDefaultDocumentTypes,
  insertDefaultPages,
  insertDefaultRequestStatuses,
  insertDefaultRoles,
  insertDefaultUsers,
  insertDefaultBanks,
} from '/server/lib/defaultDataRelated'

import { sendEmail } from '/server/lib/smscAndMailgun'
import { getEmailConfirmationHtml } from '/server/lib/globalVariables'
import { banks } from '/server/lib/defaultBanks'

// start talgat imports
import '/server/lib/requestNumberGenerator';
// end talgat imports

Meteor.startup(() => {
  insertDefaultUsers([{
    username: 'superadmin',
    email: 'superadmin@kag.kz',
    password: 'superadmin',
    profile: {
      firstName: 'Супер',
      lastName: 'Администратор'
    }
  }])

  insertDefaultPages([
    {
      name: 'Роли',
      route: 'roles',
      collectionName: 'CustomRolesCollection'
    },
    {
      name: 'Пользователи',
      route: 'users',
      collectionName: 'UsersCollection'
    },
    {
      name: 'Департаменты',
      route: 'departments',
      collectionName: 'DepartmentsCollection'
    },
    {
      name: 'Филиалы',
      route: 'branches',
      collectionName: 'BranchesCollection'
    },
    {
      name: 'МФО/КТ',
      route: 'organizations',
      collectionName: 'UsersCollection'
    },
    {
      name: 'Шаблоны',
      route: 'templates',
      collectionName: 'TemplatesCollection'
    },
    {
      name: 'Структура заявки',
      route: 'steps',
      collectionName: 'StepsCollection'
    },
    {
      name: 'Заявки',
      route: 'requests',
      collectionName: 'RequestsCollection'
    },
    {
      name: 'Кредитный комитет',
      route: 'committee',
      collectionName: 'CommitteeCollection'
    },
    {
      name: 'Типы предпринимателей',
      route: 'enterpreneurTypes',
      collectionName: 'EnterpreneurTypesCollection'
    },
    {
      name: 'Категории предпринимателей',
      route: 'enterpreneurCategories',
      collectionName: 'EnterpreneurCategoriesCollection'
    },
    {
      name: 'Категории субъектов',
      route: 'subjectCategories',
      collectionName: 'SubjectCategoriesCollection'
    },
    {
      name: 'Типы продуктов',
      route: 'productTypes',
      collectionName: 'ProductTypesCollection'
    },
    {
      name: 'Валюты',
      route: 'currencies',
      collectionName: 'СurrenciesCollection'
    },
    {
      name: 'Логи',
      route: 'logs',
      collectionName: 'LogsCollection'
    },
    {
      name: 'Запросы',
      route: 'requests',
      collectionName: 'RequestsCollection'
    },
    {
      name: 'Типы документов',
      route: 'documentTypes',
      collectionName: 'DocumentTypesCollection'
    },
    {
      name: 'Статусы заявок',
      route: 'requestStatuses',
      collectionName: 'RequestStatusesCollection'
    },
    {
      name: 'Порядок погашения',
      route: 'paymentOrders',
      collectionName: 'PaymentOrdersCollection'
    },
    {
      name: 'Образование',
      route: 'education',
      collectionName: 'EducationsCollection'
    },
    {
      name: 'КАТО',
      route: 'territories',
      collectionName: 'TerritoriesCollection'
    },
    {
      name: 'ОКЭД',
      route: 'activities',
      collectionName: 'ActivitiesCollection'
    },
    {
      name: 'Программы кредитования',
      route: 'loanPrograms',
      collectionName: 'LoanProgramsCollection'
    },
    {
      name: 'Банки',
      route: 'banks',
      collectionName: 'BanksCollection'
    }
  ])

  // insertDefaultBranches([
  //   {
  //     _id: '0',
  //     name: {
  //       ru: "Астана",
  //       kz: "Астана"
  //     },
  //     shortName: "Астана",
  //     code: "AST"
  //   }
  // ])

  const requestsPage = PagesCollection.findOne({ route: 'requests' })
  const enterpreneurTypesPage = PagesCollection.findOne({ route: 'enterpreneurTypes' })
  const productTypesPage = PagesCollection.findOne({ route: 'productTypes' })
  const currenciesPage = PagesCollection.findOne({ route: 'currencies' })
  const banksPage = PagesCollection.findOne({ route: 'banks' })
  const customRolesPage = PagesCollection.findOne({ route: 'roles' })
  const documentTypesPage = PagesCollection.findOne({ route: 'documentTypes' })
  const enterpreneurCategoriesPage = PagesCollection.findOne({ route: 'enterpreneurCategories' })
  const stepsPage = PagesCollection.findOne({ route: 'steps' })
  const subjectCategoriesPage = PagesCollection.findOne({ route: 'subjectCategories' })
  const usersPage = PagesCollection.findOne({ route: 'users' })
  const rLabel = 'просматривать'
  const cuLabel = 'создавать/редактировать'
  const dLabel = 'удалять'
  const agsLabel = 'отправлять на согласование'
  const apsLabel = 'отправлять на утверждение'
  const agLabel = 'согласовывать'
  const apLabel = 'утверждать'

  insertDefaultDocumentTypes([
    {
      _id: "0",
      name: {
        ru: "Экспертное заключение",
        kz: ""
      },
      tooltip: "Загрузите экспертное заключение",
      multiple: true,
    },
    {
      _id: "1",
      name: {
        ru: "Фотоотчет",
        kz: ""
      },
      tooltip: "Фото с выезда на место реализации проекта",
      multiple: true,
    }
  ])

  insertDefaultRoles([
    {
      _id: '0',
      name: 'МФО',
      pairs: [
        {
          page: requestsPage._id,
          actions: [
            { value: 'r', label: rLabel },
            { value: 'cu', label: cuLabel },
            { value: 'd', label: dLabel },
          ]
        },
        {
          page: enterpreneurTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: productTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: currenciesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: banksPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: customRolesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: documentTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: enterpreneurCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: stepsPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: subjectCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: usersPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
      ]
    },
    {
      _id: '1',
      name: 'Отправитель на согласование',
      pairs: [
        {
          page: requestsPage._id,
          actions: [
            { value: 'ags', label: agsLabel },
          ]
        },
        {
          page: enterpreneurTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: productTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: currenciesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: banksPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: customRolesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: documentTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: enterpreneurCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: stepsPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: subjectCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: usersPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
      ]
    },
    {
      _id: '2',
      name: 'Отправитель на утверждение',
      pairs: [
        {
          page: requestsPage._id,
          actions: [
            { value: 'aps', label: apsLabel  },
          ]
        },
        {
          page: enterpreneurTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: productTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: currenciesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: banksPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: customRolesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: documentTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: enterpreneurCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: stepsPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: subjectCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: usersPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
      ]
    },
    {
      _id: '3',
      name: 'Согласующий',
      pairs: [
        {
          page: requestsPage._id,
          actions: [
            { value: 'ag', label: agLabel },
          ]
        },
        {
          page: enterpreneurTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: productTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: currenciesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: banksPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: customRolesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: documentTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: enterpreneurCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: stepsPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: subjectCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: usersPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
      ]
    },
    {
      _id: '4',
      name: 'Утверждающий',
      pairs: [
        {
          page: requestsPage._id,
          actions: [
            { value: 'ap', label: apLabel },
          ]
        },
        {
          page: enterpreneurTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: productTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: currenciesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: banksPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: customRolesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: documentTypesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: enterpreneurCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: stepsPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: subjectCategoriesPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
        {
          page: usersPage._id,
          actions: [
            { value: 'r', label: rLabel },
          ]
        },
      ]
    }
  ])

  insertDefaultRequestStatuses([
    {
      code: 0,
      nameRu: 'новый',
      nameKz: 'жаңа',
      class: 'status-new'
    },
    {
      code: 1,
      nameRu: 'на согласовании',
      nameKz: 'келісім бойынша',
      class: 'status-on-approval'
    },
    {
      code: 2,
      nameRu: 'на доработке',
      nameKz: 'аяқталуы туралы',
      class: 'status-on-review'
    },
    {
      code: 3,
      nameRu: 'на утверждении',
      nameKz: 'мақұлдау туралы',
      class: 'status-on-uo-approve'
    },
    {
      code: 4,
      nameRu: 'согласован',
      nameKz: 'келісілді',
      class: 'status-approved'
    },
    {
      code: 5,
      nameRu: 'утвержден',
      nameKz: 'мақұлдаған',
      class: 'status-uo-approved'
    },
    {
      code: 6,
      nameRu: 'отклонен',
      nameKz: 'қабылдамады',
      class: 'status-rejected'
    }
  ])

  insertDefaultBanks(banks)
})

Meteor.methods({
  'page.permission.check'(userId, pageRoute, actionName) {
    const theUser = Meteor.users.findOne(userId)

    if(!theUser)
      return false

    if(theUser.roles && "all" in theUser.roles)
      return true

    if(!theUser.emails[0].verified)
      return false

    if(!theUser.profile.roles || !theUser.profile.roles.length)
      return false

    const theRoleIds = theUser.profile.roles.map(role => role.value)

    const theRoles = CustomRolesCollection.find({ _id: { $in: theRoleIds } }).fetch()
    const thePage = PagesCollection.findOne({ route: pageRoute })

    let permitted = false

    theRoles.some(theRole => {
      theRole.pairs.some(pair => {
        const theAction = pair.actions.filter(action => action.value === actionName)

        if(pair.page === thePage._id && theAction.length) {
          permitted = true
          return true
        }
      })

      if(permitted)
        return true
    })

    return permitted
  },
  'collection.permission.check'(userId, collectionName, actionName) {
    const theUser = Meteor.users.findOne(userId)

    if(!theUser)
      return false

    if(theUser.roles && "all" in theUser.roles)
      return true

    if(!theUser.profile.roles || !theUser.profile.roles.length)
      return false

    const theRoleIds = theUser.profile.roles.map(role => role.value)

    const theRoles = CustomRolesCollection.find({ _id: { $in: theRoleIds } }).fetch()
    const thePage = PagesCollection.findOne({ collectionName })

    let permitted = false

    theRoles.some(theRole => {
      theRole.pairs.some(pair => {
        const theAction = pair.actions.filter(action => action.value === actionName)

        if(pair.page === thePage._id && theAction.length) {
          permitted = true
          return true
        }
      })

      if(permitted)
        return true
    })

    return permitted
  },
})
