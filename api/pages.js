import { Mongo } from 'meteor/mongo';

export const PagesCollection = new Mongo.Collection('pages');

if (Meteor.isServer) {
  Meteor.publish('pages', function(){
    return PagesCollection.find({deleted: {$ne: true}})
  });

  Meteor.publish('page', function(_id){
    return PagesCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.methods({
    'page.insert'(data) {
      PagesCollection.insert(data);
    },
    'page.delete'(_id) {
      PagesCollection.update({_id}, { $set: {deleted: true}});
    },
    'page.update'(_id, data) {
      PagesCollection.update({_id}, { $set: data});
    }
  });
}
