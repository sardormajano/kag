import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const CurrenciesCollection = new Mongo.Collection('currencies');

CurrenciesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'CurrenciesCollection', 'cu')
})

CurrenciesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'CurrenciesCollection', 'cu')
})

CurrenciesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'CurrenciesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('currencies', function(){
    return CurrenciesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('currencies.names', function(){
    return CurrenciesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('currency', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'CurrenciesCollection', 'r'))
      return

    return CurrenciesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'currency.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'currency.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      CurrenciesCollection.insert(data);
    },
    'currency.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'currency.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      CurrenciesCollection.update({ _id }, { $set: {deleted: true} });
    },
    'currency.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'currency.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      CurrenciesCollection.update({ _id }, { $set: data });
    }
  });
}
