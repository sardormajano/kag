import { Mongo } from 'meteor/mongo';

export const LogsCollection = new Mongo.Collection('logs');

if (Meteor.isServer) {
  Meteor.publish('logs', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'LogsCollection', 'r'))
      return

    return LogsCollection.find({
      deleted: { $ne: true }
    }, {
      sort: { createdAt: -1 }
    })
  });

  Meteor.publish('logs.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'LogsCollection', 'r'))
      return

    return LogsCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('log', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'LogsCollection', 'r'))
      return

    return LogsCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'get.logs.by.page'(page, pageSize) {
      return LogsCollection.find({
          deleted: {$ne: true},
        },
        {
          skip: pageSize * (page - 1),
          limit: pageSize,
          sort: { createdAt: -1 }
        }
      ).fetch()
    },
    'log.insert'(data) {
      LogsCollection.insert(data);
    },
    'log.delete'(_id) {
      LogsCollection.update({ _id }, { $set: {deleted: true} });
    },
    'log.update'(_id, data) {
      LogsCollection.update({ _id }, { $set: data });
    }
  });
}
