import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const BranchesCollection = new Mongo.Collection('branches');

BranchesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'BranchesCollection', 'cu')
})

BranchesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'BranchesCollection', 'cu')
})

BranchesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'BranchesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('branches', function(){
    return BranchesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('branches.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'BranchesCollection', 'r'))
      return

    return BranchesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('branch', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'BranchesCollection', 'r'))
      return

    return BranchesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'branch.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'branch.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      BranchesCollection.insert(data)
    },
    'branch.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'branch.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      BranchesCollection.update({ _id }, { $set: {deleted: true} })
    },
    'branch.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'branch.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      BranchesCollection.update({ _id }, { $set: data });
    }
  });
}
