import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const LoanProgramsCollection = new Mongo.Collection('loanPrograms');

LoanProgramsCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'LoanProgramsCollection', 'cu')
})

LoanProgramsCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'LoanProgramsCollection', 'cu')
})

LoanProgramsCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'LoanProgramsCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('loanPrograms', function(){
    return LoanProgramsCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('loanPrograms.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'LoanProgramsCollection', 'r'))
      return

    return LoanProgramsCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('loanProgram', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'LoanProgramsCollection', 'r'))
      return

    return LoanProgramsCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'loanProgram.insert'(data, userIp) {
      const log = {
        userIp: userIp,
        userId: Meteor.userId(),
        createdAt: Date.now(),
        service: 'loanProgram.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      LoanProgramsCollection.insert(data)
    },
    'loanProgram.delete'(_id, userIp) {
      const log = {
        userIp: userIp,
        userId: Meteor.userId(),
        createdAt: Date.now(),
        service: 'loanProgram.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      LoanProgramsCollection.update({ _id }, { $set: {deleted: true} })
    },
    'loanProgram.update'(_id, data, userIp) {
      const log = {
        userIp: userIp,
        userId: Meteor.userId(),
        createdAt: Date.now(),
        service: 'loanProgram.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      LoanProgramsCollection.update({ _id }, { $set: data });
    }
  });
}
