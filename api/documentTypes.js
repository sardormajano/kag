import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
import { StepsCollection } from '/api/steps'

export const DocumentTypesCollection = new Mongo.Collection('documentTypes');

DocumentTypesCollection.before.insert(function(userId, doc) {
  if(doc._id)
    return true

  return Meteor.call('collection.permission.check', userId, 'DocumentTypesCollection', 'cu')
})

DocumentTypesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'DocumentTypesCollection', 'cu')
})

DocumentTypesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'DocumentTypesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('documentTypes.all', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'DocumentTypesCollection', 'r'))
      return

    return DocumentTypesCollection.find({
      deleted: {$ne: true}
    })
  });

  Meteor.publish('documentTypes', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'DocumentTypesCollection', 'r'))
      return

    return DocumentTypesCollection.find({
      deleted: {$ne: true}
    }, {
      fields: {
        name: 1,
        busy: 1
      }
    })
  });

  Meteor.publish('documentTypes.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'DocumentTypesCollection', 'r'))
      return

    return DocumentTypesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('documentType', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'DocumentTypesCollection', 'r'))
      return

    return DocumentTypesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'documentType.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'documentType.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      DocumentTypesCollection.insert(data)
    },
    'documentType.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'documentType.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      StepsCollection.update(
        {},
        { $pull: {documentTypes: { "value" : _id} }},
        { multi: true })
      DocumentTypesCollection.update({ _id }, { $set: {deleted: true} })
    },
    'documentType.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'documentType.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      DocumentTypesCollection.update({ _id }, { $set: data });
    }
  });
}
