import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const BanksCollection = new Mongo.Collection('banks');

BanksCollection.before.insert(function(userId, doc) {
  return true
})

BanksCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'BanksCollection', 'cu')
})

BanksCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'BanksCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('banks', function(){
    return BanksCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('banks.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'BanksCollection', 'r'))
      return

    return BanksCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('bank', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'BanksCollection', 'r'))
      return

    return BanksCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'bank.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'bank.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      BanksCollection.insert(data)
    },
    'bank.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'bank.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      BanksCollection.update({ _id }, { $set: {deleted: true} })
    },
    'bank.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'bank.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      BanksCollection.update({ _id }, { $set: data });
    }
  });
}
