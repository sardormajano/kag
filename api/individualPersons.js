import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const IndividualPersonsCollection = new Mongo.Collection('individualPersons');

IndividualPersonsCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'IndividualPersonsCollection', 'cu')
})

IndividualPersonsCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'IndividualPersonsCollection', 'cu')
})

IndividualPersonsCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'IndividualPersonsCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('individualPersons', function(){
    return IndividualPersonsCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('individualPersons.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'IndividualPersonsCollection', 'r'))
      return

    return IndividualPersonsCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('individualPerson', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'IndividualPersonsCollection', 'r'))
      return

    return IndividualPersonsCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'individual.person.get'(iin) {
      return IndividualPersonsCollection.findOne({ iin })
    },
    'individualPerson.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'individualPerson.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      if(IndividualPersonsCollection.find({ iin: data.iin }).count()) {
        return false
      }

      IndividualPersonsCollection.insert(data)
      return true
    },
    'individualPerson.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'individualPerson.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      IndividualPersonsCollection.update({ _id }, { $set: {deleted: true} })
    },
    'individualPerson.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'individualPerson.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      IndividualPersonsCollection.update({ _id }, { $set: data });
    }
  });
}
