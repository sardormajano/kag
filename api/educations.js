import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const EducationsCollection = new Mongo.Collection('educations');

EducationsCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EducationsCollection', 'cu')
})

EducationsCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EducationsCollection', 'cu')
})

EducationsCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EducationsCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('educations', function(){
    return EducationsCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('educations.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'EducationsCollection', 'r'))
      return

    return EducationsCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('education', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'EducationsCollection', 'r'))
      return

    return EducationsCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'education.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'education.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      EducationsCollection.insert(data)
    },
    'education.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'education.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      EducationsCollection.update({ _id }, { $set: {deleted: true} })
    },
    'education.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'education.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      EducationsCollection.update({ _id }, { $set: data });
    }
  });
}
