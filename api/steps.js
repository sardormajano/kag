import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
import { DocumentTypesCollection } from '/api/documentTypes'

export const StepsCollection = new Mongo.Collection('steps');

function reserveDocumentTypes(arr) {
  DocumentTypesCollection.update({
    _id: {
      $in: arr
    },
    deleted: {
      $ne: true
    }
  }, {
    $set: {
      busy: true
    }
  }, {
    multi: true
  })
}

function unreserveDocumentTypes(arr) {
  DocumentTypesCollection.update({
    _id: {
      $in: arr
    },
    deleted: {
      $ne: true
    }
  }, {
    $set: {
      busy: false
    }
  }, {
    multi: true
  })
}

StepsCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'StepsCollection', 'cu')
})

StepsCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'StepsCollection', 'cu')
})

StepsCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'StepsCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('steps', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'StepsCollection', 'r'))
      return

    return StepsCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('steps.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'StepsCollection', 'r'))
      return

    return StepsCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: {
          name: 1,
          order: 1,
          documentTypes: 1,
        }
      });
  });

  Meteor.publish('step', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'StepsCollection', 'r'))
      return

    return StepsCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'get.step'(_id) {
      return StepsCollection.findOne(_id)
    },
    'level.step.up'(_id) {
      const theStep = StepsCollection.findOne(_id)
      const newOrder = theStep.order - 1

      if(!newOrder)
        return

      StepsCollection.update({
        order: newOrder,
        deleted: { $ne: true }
      }, {
        $inc: { order: 1 }
      })

      StepsCollection.update({
        _id
      }, {
        $inc: { order: -1 }
      })
    },
    'level.step.down'(_id) {
      const theStep = StepsCollection.findOne(_id)
      const newOrder = theStep.order + 1

      if(newOrder > StepsCollection.find({ deleted: { $ne: true} }).count())
        return

      StepsCollection.update({
        order: newOrder,
        deleted: { $ne: true }
      }, {
        $inc: { order: -1 }
      })

      StepsCollection.update({
        _id
      }, {
        $inc: { order: 1 }
      })
    },
    'step.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'step.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)
      const documentTypeIds = data.documentTypes.map(dt => dt.value)

      reserveDocumentTypes(documentTypeIds)

      const stepsNum = StepsCollection.find({ deleted: { $ne: true } }).count()
      data.order = stepsNum + 1

      StepsCollection.insert(data)
    },
    'step.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'step.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      const theStep = StepsCollection.findOne(_id)
      const documentTypeIds = theStep.documentTypes.map(dt => dt.value)

      unreserveDocumentTypes(documentTypeIds)
      const stepsNum = StepsCollection.find({ deleted: { $ne: true } }).count()

      StepsCollection.update({ order: { $gt: theStep.order }, deleted: { $ne: true} }, { $inc: { order: -1 } }, { multi: true })

      StepsCollection.update({ _id }, { $set: {deleted: true} })
    },
    'step.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'step.update',
        payload: {
          _id,
          data
        },
      }

      const documentTypeIds = data.documentTypes.map(dt => dt.value)
      const currentDocumentTypeIds = StepsCollection.findOne(_id).documentTypes.map(dt => dt.value)

      unreserveDocumentTypes(currentDocumentTypeIds)
      reserveDocumentTypes(documentTypeIds)

      LogsCollection.insert(log)

      StepsCollection.update({ _id }, { $set: data });
    }
  });
}
