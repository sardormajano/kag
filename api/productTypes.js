import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const ProductTypesCollection = new Mongo.Collection('productTypes');

ProductTypesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'ProductTypesCollection', 'cu')
})

ProductTypesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'ProductTypesCollection', 'cu')
})

ProductTypesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'ProductTypesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('productTypes', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'ProductTypesCollection', 'r'))
      return

    return ProductTypesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('productTypes.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'ProductTypesCollection', 'r'))
      return

    return ProductTypesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('productType', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'ProductTypesCollection', 'r'))
      return

    return ProductTypesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'productType.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'productType.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      ProductTypesCollection.insert(data);
    },
    'productType.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'productType.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      ProductTypesCollection.update({ _id }, { $set: {deleted: true} });
    },
    'productType.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'productType.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      ProductTypesCollection.update({ _id }, { $set: data });
    }
  });
}
