import { Mongo } from 'meteor/mongo';

import { UsersCollection as OrganizationsCollection } from '/api/users'
import { EducationsCollection } from '/api/educations'
import { PaymentOrdersCollection } from '/api/paymentOrders'
import { EnterpreneurCategoriesCollection } from '/api/enterpreneurCategories'
import { RequestsCollection } from '/api/requests'
import { BranchesCollection } from '/api/branches'

// export const PaymentOrdersCollection = new Mongo.Collection('paymentOrders');

if (Meteor.isServer) {

  Meteor.methods({
    'reportRef.get'(refOptions) {
      var result = {};

      // сведения кредитной организации
      if (!!refOptions.creditOrganizationId) {
          var co = (OrganizationsCollection.find({ '_id' : (refOptions.creditOrganizationId) }).fetch())[0];
          result.creditOrganizationNameRu = co.profile.name.ru;
          result.creditOrganizationNameKz = co.profile.name.kz;
      }

      // образование гарантополучателя
      if (!!refOptions.garantEducationId) {
          var edu = (EducationsCollection.find({ '_id' : (refOptions.garantEducationId) }).fetch())[0];
          result.garantEducationNameRu = edu.name.ru;
          result.garantEducationNameKz = edu.name.kz;
      }

      // порядок погашения основного долга
      if (!!refOptions.liPaymentOrderLoanId) {
          var pol = (PaymentOrdersCollection.find({ '_id' : (refOptions.liPaymentOrderLoanId) }).fetch())[0];
          if (!!pol) {
            result.liPaymentOrderLoanNameRu = pol.name.ru;
            result.liPaymentOrderLoanNameKz = pol.name.kz;
          }
      }

      // порядок погашения основного долга
      if (!!refOptions.liPaymentOrderPercentId) {
          var popi = (PaymentOrdersCollection.find({ '_id' : (refOptions.liPaymentOrderPercentId) }).fetch())[0];
          if (!!popi) {
            result.liPaymentOrderPercentNameRu = popi.name.ru;
            result.liPaymentOrderPercentNameKz = popi.name.kz;
        }
      }

      // размер бизнеса
      if (!!refOptions.biEnterpreneurCategoryId) {
          var ec = (EnterpreneurCategoriesCollection.find({ '_id' : (refOptions.biEnterpreneurCategoryId) }).fetch())[0];
          if (!!popi) {
            result.biEnterpreneurCategoryNameRu = ec.name.ru;
            result.biEnterpreneurCategoryNameKz = ec.name.kz;
        }
      }

      // получаем сведения о Представительстве
      // наименование области представителя
        if (!!refOptions.requestId) {
            var request = (RequestsCollection.find({ '_id' : (refOptions.requestId) }).fetch())[0];
            var creditOrganization = (OrganizationsCollection.find({ '_id' : (request.loanInformation.liCreditOrganization) }).fetch())[0];
            var branch = (BranchesCollection.find({ '_id' : ( creditOrganization.profile.branch ) }).fetch())[0];

            result.branchNameRu = branch.name.ru;
            result.branchNameKz = branch.name.kz;
        }

      return result;

    }
  })

}
