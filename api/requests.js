import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
import { RequestStatusesCollection } from '/api/requestStatuses'

export const RequestsCollection = new Mongo.Collection('requests');

RequestsCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'RequestsCollection', 'cu')
})

RequestsCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'RequestsCollection', 'cu')
})

RequestsCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'RequestsCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('requests', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'RequestsCollection', 'r'))
      return

    return RequestsCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('requests.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'RequestsCollection', 'r'))
      return

    return RequestsCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('request', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'RequestsCollection', 'r'))
      return

    return RequestsCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'request.get'(_id) {
      return RequestsCollection.findOne({ _id })
    },
    'request.get.incomplete'() {
      return RequestsCollection.findOne({
        incomplete: true,
        author: Meteor.userId()
      })
    },
    'request.upsert'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.upsert',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      data.author = Meteor.userId()
      let authorC = Meteor.users.findOne({_id: data.author});
      // console.log('data.author: ' + data.author);
      // console.log('author: ' + authorC);
      let branchId = authorC.profile.branch;
      data.status = (typeof data.status === 'undefined') ? 0 : data.status;
      let tempRN = Meteor.call('generateRequestNumber',branchId);
      data.requestNumber = (typeof data.requestNumber === 'undefined') ? tempRN : data.requestNumber;

      return RequestsCollection.upsert({ _id }, { $set: data }, { upsert: true })
    },
    'request.insert'(data, userIp) {
      data.author = Meteor.userId()
      let authorC = Meteor.users.findOne({_id: data.author});
      // console.log('data.author: ' + data.author);
      // console.log('author: ' + authorC);
      let branchId = authorC.profile.branch;
      data.status = (typeof data.status === 'undefined') ? 0 : data.status;
      let tempRN = Meteor.call('generateRequestNumber',branchId);
      data.requestNumber = (typeof data.requestNumber === 'undefined') ? tempRN : data.requestNumber;

      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      RequestsCollection.insert(data)
    },
    'request.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      RequestsCollection.update({ _id }, { $set: {deleted: true} })
    },
    'request.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      RequestsCollection.update({ _id }, { $set: data });
    },
    'request.complete'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.complete',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      RequestsCollection.update({ _id }, { $set: {
        incomplete: false,
        status: 0, //new
        applicationReview: [{
          createdAt: Date.now(),
          message: 'Заявка создана',
          createdBy: Meteor.userId()
        }]
      },
        $unset: {
          activeStep: ''
        } });
    },
    'request.write.message'(_id, message, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.write.message',
        payload: {
          _id,
          message
        },
      }
      LogsCollection.insert(log)

      RequestsCollection.update({ _id }, { $push: {
        applicationReview: {
          createdAt: Date.now(),
          message,
          createdBy: Meteor.userId()
        }
      } });
    },
    'request.change.status'(_id, newStatus, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.change.status',
        payload: {
          _id,
          newStatus
        },
      }
      LogsCollection.insert(log)

      const theRequest = RequestsCollection.findOne(_id)
      const theOldStatus = RequestStatusesCollection.findOne({ code: theRequest.status })
      const theNewStatus = RequestStatusesCollection.findOne({ code: newStatus })

      RequestsCollection.update({ _id }, {
        $set: {
          status: newStatus
        },
        $push: {
          applicationReview: {
            createdAt: Date.now(),
            message: `Статус заявки изменен: ${theOldStatus.name} -> ${theNewStatus.name}`,
            createdBy: Meteor.userId()
          }
        }
      });
    },
    'request.send.for.agreement'(_id, agreeing, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.send.for.agreement',
        payload: {
          _id,
          agreeing
        },
      }

      LogsCollection.insert(log)

      RequestsCollection.update({ _id }, {
        $set: {
          holders: agreeing
        },
        $push: {
          applicationReview: {
            createdAt: Date.now(),
            message: 'Передан на согласование',
            createdBy: Meteor.userId()
          }
        }
      })

      Meteor.call('request.change.status', _id, 1, userIp) //onAgreement
    },
    'request.send.for.approval'(_id, approving, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'request.send.for.approval',
        payload: {
          _id,
          approving
        },
      }

      LogsCollection.insert(log)

      RequestsCollection.update({ _id }, {
        $set: {
          holders: approving
        },
        $push: {
          applicationReview: {
            createdAt: Date.now(),
            message: 'Передан на утверждение',
            createdBy: Meteor.userId()
          }
        }
      })

      Meteor.call('request.change.status', _id, 3, userIp) //onApproval
    }
  });
}
