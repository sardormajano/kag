import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const CustomRolesCollection = new Mongo.Collection('customRoles');

CustomRolesCollection.before.insert(function(userId, doc) {
  if(doc._id)
    return true

  return Meteor.call('collection.permission.check', userId, 'CustomRolesCollection', 'cu')
})

CustomRolesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'CustomRolesCollection', 'cu')
})

CustomRolesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'CustomRolesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('customRoles', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'CustomRolesCollection', 'r'))
      return

    return CustomRolesCollection.find({ deleted: {$ne: true} });
  });

  Meteor.publish('customRoles.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'CustomRolesCollection', 'r'))
      return

    return CustomRolesCollection.find(
      {
        deleted: {$ne: true}
      }, {
        fields: { name: 1 }
      });
  });

  Meteor.publish('customRole', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'CustomRolesCollection', 'r'))
      return

    return CustomRolesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'customRole.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'customRole.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      CustomRolesCollection.insert(data);
    },
    'customRole.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'customRole.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      CustomRolesCollection.update({ _id }, { $set: {deleted: true} });
    },
    'customRole.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'customRole.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      CustomRolesCollection.update({ _id }, { $set: data });
    }
  });
}
