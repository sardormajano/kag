import { FilesCollection } from 'meteor/ostrio:files'

export const DocsCollection = new FilesCollection({
  storagePath: '/docs',
  downloadRoute: '/docs',
  permissions: 0774,
	parentDirPermissions: 0774,
  collectionName: 'DocsCollection',
  allowClientCode: true, // Disallow remove files from Client
  onBeforeUpload(file) {
    return true;
  }
})

if(Meteor.isServer) {
  Meteor.publish('docs.all', function () {
    return DocsCollection.find().cursor
  })

  Meteor.methods({
    'get.link'(file) {
      if(file.progress == 100)
        return DocsCollection.findOne(file._id).link()
    }
  })
}
