import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const SubjectCategoriesCollection = new Mongo.Collection('subjectCategories');

SubjectCategoriesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'SubjectCategoriesCollection', 'cu')
})

SubjectCategoriesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'SubjectCategoriesCollection', 'cu')
})

SubjectCategoriesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'SubjectCategoriesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('subjectCategories', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'SubjectCategoriesCollection', 'r'))
      return

    return SubjectCategoriesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('subjectCategories.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'SubjectCategoriesCollection', 'r'))
      return

    return SubjectCategoriesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('subjectCategory', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'SubjectCategoriesCollection', 'r'))
      return

    return SubjectCategoriesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'subjectCategory.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'subjectCategory.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      SubjectCategoriesCollection.insert(data)
    },
    'subjectCategory.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'subjectCategory.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      SubjectCategoriesCollection.update({ _id }, { $set: {deleted: true} })
    },
    'subjectCategory.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'subjectCategory.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      SubjectCategoriesCollection.update({ _id }, { $set: data });
    }
  });
}
