import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const EnterpreneurCategoriesCollection = new Mongo.Collection('enterpreneurCategories');

EnterpreneurCategoriesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EnterpreneurCategoriesCollection', 'cu')
})

EnterpreneurCategoriesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EnterpreneurCategoriesCollection', 'cu')
})

EnterpreneurCategoriesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EnterpreneurCategoriesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('enterpreneurCategories', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'EnterpreneurCategoriesCollection', 'r'))
      return

    return EnterpreneurCategoriesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('enterpreneurCategories.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'EnterpreneurCategoriesCollection', 'r'))
      return

    return EnterpreneurCategoriesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('enterpreneurCategory', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'EnterpreneurCategoriesCollection', 'r'))
      return

    return EnterpreneurCategoriesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'enterpreneurCategory.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'enterpreneurCategory.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      EnterpreneurCategoriesCollection.insert(data)
    },
    'enterpreneurCategory.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'enterpreneurCategory.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      EnterpreneurCategoriesCollection.update({ _id }, { $set: {deleted: true} })
    },
    'enterpreneurCategory.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'enterpreneurCategory.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      EnterpreneurCategoriesCollection.update({ _id }, { $set: data });
    }
  });
}
