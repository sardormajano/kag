import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const EnterpreneurTypesCollection = new Mongo.Collection('enterpreneurTypes');

EnterpreneurTypesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EnterpreneurTypesCollection', 'cu')
})

EnterpreneurTypesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EnterpreneurTypesCollection', 'cu')
})

EnterpreneurTypesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'EnterpreneurTypesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('enterpreneurTypes', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'EnterpreneurTypesCollection', 'r'))
      return

    return EnterpreneurTypesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('enterpreneurTypes.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'EnterpreneurTypesCollection', 'r'))
      return

    return EnterpreneurTypesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('enterpreneurType', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'EnterpreneurTypesCollection', 'r'))
      return

    return EnterpreneurTypesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'enterpreneurType.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'enterpreneurType.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      EnterpreneurTypesCollection.insert(data)
    },
    'enterpreneurType.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'enterpreneurType.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      EnterpreneurTypesCollection.update({ _id }, { $set: {deleted: true} })
    },
    'enterpreneurType.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'enterpreneurType.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      EnterpreneurTypesCollection.update({ _id }, { $set: data });
    }
  });
}
