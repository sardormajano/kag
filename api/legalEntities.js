import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const LegalEntitiesCollection = new Mongo.Collection('legalEntities');

LegalEntitiesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'LegalEntitiesCollection', 'cu')
})

LegalEntitiesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'LegalEntitiesCollection', 'cu')
})

LegalEntitiesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'LegalEntitiesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('legalEntities', function(){
    return LegalEntitiesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('legalEntities.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'LegalEntitiesCollection', 'r'))
      return

    return LegalEntitiesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('legalEntity', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'LegalEntitiesCollection', 'r'))
      return

    return LegalEntitiesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'legalEntity.get'(bin) {
      return LegalEntitiesCollection.findOne({ bin })
    },
    'legalEntity.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'legalEntity.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      if(LegalEntitiesCollection.find({ bin: data.bin }).count()) {
        return false
      }

      LegalEntitiesCollection.insert(data)
      return true
    },
    'legalEntity.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'legalEntity.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      LegalEntitiesCollection.update({ _id }, { $set: {deleted: true} })
    },
    'legalEntity.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'legalEntity.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      LegalEntitiesCollection.update({ _id }, { $set: data });
    }
  });
}
