import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const DepartmentsCollection = new Mongo.Collection('departments');

DepartmentsCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'DepartmentsCollection', 'cu')
})

DepartmentsCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'DepartmentsCollection', 'cu')
})

DepartmentsCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'DepartmentsCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('departments', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'DepartmentsCollection', 'r'))
      return

    return DepartmentsCollection.find({ deleted: {$ne: true} });
  });

  Meteor.publish('departments.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'DepartmentsCollection', 'r'))
      return

    return DepartmentsCollection.find({
      deleted: { $ne: true }
    }, {
      fields: { 'name.ru': 1 }
    });
  });

  Meteor.publish('department', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'DepartmentsCollection', 'r'))
      return

    return DepartmentsCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'department.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'department.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      DepartmentsCollection.insert(data);
    },
    'department.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'department.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      DepartmentsCollection.update({ _id }, { $set: {deleted: true} });
    },
    'department.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'department.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      DepartmentsCollection.update({ _id }, { $set: data });
    }
  });
}
