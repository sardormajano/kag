import { Mongo } from 'meteor/mongo'
import { LogsCollection } from '/api/logs'
import fetch from 'node-fetch'

export const ActivitiesCollection = new Mongo.Collection('activities');

const PAGE_SIZE = 10000

function insertActivities(arr) {
  arr.forEach(item => {
    if(!ActivitiesCollection.find({ Code: item.Code }).count()) {
      ActivitiesCollection.insert(item)
    }
  })
}

function okedUpdate(from) {
  fetch(`http://data.egov.kz/api/v2/oked/data?source={from: ${from}, size:10000}`)
    .then(function(res) {
      return res.json()
    }).then(function(json) {
      insertActivities(json)

      if(json.length) {
        okedUpdate(from + PAGE_SIZE)
      }
    });
}

ActivitiesCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'ActivitiesCollection', 'cu')
})

ActivitiesCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'ActivitiesCollection', 'cu')
})

ActivitiesCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'ActivitiesCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('activities.by.page', function(page, pageSize){
    return ActivitiesCollection.find({
        deleted: {$ne: true},
      },
      {
        skip: pageSize * (page - 1),
        limit: pageSize
      }
    );
  });

  Meteor.publish('activities', function(){
    return ActivitiesCollection.find(
      {
        deleted: {$ne: true}
      },
      {
        limit: 30
      }
    )
  });

  Meteor.publish('activities.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'ActivitiesCollection', 'r'))
      return

    return ActivitiesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('activity', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'ActivitiesCollection', 'r'))
      return

    return ActivitiesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.publish('activities.search', function(text){
    return ActivitiesCollection.find({
      $or: [
        {
          deleted: { $ne: true },
          NameKaz: { $regex: new RegExp(text, 'i') }
        },
        {
          deleted: { $ne: true },
          NameRus: { $regex: new RegExp(text, 'i') }
        },
      ]
    });
  });

  Meteor.publish('activities.by.parent', function(Parent) {
    return ActivitiesCollection.find(
      {
        Parent,
        deleted: {$ne: true}
      }
    )
  })

  Meteor.publish('activities.no.parent', function() {
    return ActivitiesCollection.find(
      {
        Parent: '',
        deleted: {$ne: true}
      }
    )
  })

  Meteor.methods({
    'get.activities.by.page'(page, pageSize) {
      return ActivitiesCollection.find({
          deleted: {$ne: true},
        },
        {
          skip: pageSize * (page - 1),
          limit: pageSize
        }
      ).fetch()
    },
    'get.activities.by.search'(text) {
      return ActivitiesCollection.find({
        $or: [
          {
            deleted: { $ne: true },
            NameKaz: { $regex: new RegExp(text, 'i') }
          },
          {
            deleted: { $ne: true },
            NameRus: { $regex: new RegExp(text, 'i') }
          },
        ]
      }).fetch()
    },
    'oked.update'() {
      okedUpdate(0)
    },
    'activity.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'activity.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      const parentActivity = ActivitiesCollection.findOne({ Id: data.Parent })
      data.Level = parentActivity.Level + 1

      ActivitiesCollection.insert(data)
    },
    'activity.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'activity.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      ActivitiesCollection.update({ _id }, { $set: {deleted: true} })
    },
    'activity.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'activity.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      const parentActivity = ActivitiesCollection.findOne({ Id: data.Parent })
      data.Level = parentActivity.Level + 1

      ActivitiesCollection.update({ _id }, { $set: data })
    },
    'activity.has.children'(Id) {
      return !!ActivitiesCollection.find({ Parent: Id }).count()
    },
    'get.activity'(_id) {
      return ActivitiesCollection.findOne(_id)
    }
  });
}
