import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const RequestStatusesCollection = new Mongo.Collection('requestStatuses');

if (Meteor.isServer) {
  Meteor.publish('requestStatuses', function(){
    return RequestStatusesCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('requestStatuses.names', function(){
    return RequestStatusesCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('requestStatus', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'RequestStatusesCollection', 'r'))
      return

    return RequestStatusesCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'requestStatus.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'requestStatus.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      RequestStatusesCollection.insert(data);
    },
    'requestStatus.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'requestStatus.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      RequestStatusesCollection.update({ _id }, { $set: {deleted: true} });
    },
    'requestStatus.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'requestStatus.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      RequestStatusesCollection.update({ _id }, { $set: data });
    }
  });
}
