import { Mongo } from 'meteor/mongo';
import { LogsCollection } from '/api/logs'
export const PaymentOrdersCollection = new Mongo.Collection('paymentOrders');

PaymentOrdersCollection.before.insert(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'PaymentOrdersCollection', 'cu')
})

PaymentOrdersCollection.before.update(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'PaymentOrdersCollection', 'cu')
})

PaymentOrdersCollection.before.remove(function(userId, doc) {
  return Meteor.call('collection.permission.check', userId, 'PaymentOrdersCollection', 'd')
})

if (Meteor.isServer) {
  Meteor.publish('paymentOrders', function(){
    return PaymentOrdersCollection.find({ deleted: {$ne: true} })
  });

  Meteor.publish('paymentOrders.names', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'PaymentOrdersCollection', 'r'))
      return

    return PaymentOrdersCollection.find({
        deleted: {
            $ne: true
          }
      }, {
        fields: { 'name.ru': 1 }
      });
  });

  Meteor.publish('paymentOrder', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'PaymentOrdersCollection', 'r'))
      return

    return PaymentOrdersCollection.find({ deleted: {$ne: true}, _id });
  });

  Meteor.methods({
    'paymentOrder.insert'(data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'paymentOrder.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      PaymentOrdersCollection.insert(data)
    },
    'paymentOrder.delete'(_id, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'paymentOrder.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      PaymentOrdersCollection.update({ _id }, { $set: {deleted: true} })
    },
    'paymentOrder.update'(_id, data, userIp) {
      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'paymentOrder.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      PaymentOrdersCollection.update({ _id }, { $set: data });
    }
  });
}
