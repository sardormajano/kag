import { Mongo } from 'meteor/mongo'
import { LogsCollection } from '/api/logs'
export const UsersCollection = Meteor.users
import { CustomRolesCollection } from '/api/customRoles'
import { Random } from 'meteor/random'

const passwordPlaceholder = '__password__'

if (Meteor.isServer) {
  import { sendSMS, sendEmail } from '/server/lib/smscAndMailgun'
  import { getEmailConfirmationHtml, getRandomCode, getResetPasswordHtml } from '/server/lib/globalVariables'

  Meteor.publish('organizations', function(){
    if(!Meteor.call('page.permission.check', this.userId, 'organizations', 'r'))
      return

    return UsersCollection.find({
      deleted: {$ne: true},
      'profile.isOrganization': true
    })
  });

  Meteor.publish('users', function(){
    if(!Meteor.call('collection.permission.check', this.userId, 'UsersCollection', 'r'))
      return

      return UsersCollection.find({
        deleted: {$ne: true},
        'profile.isOrganization': {$ne: true}
      })
  });

  Meteor.publish('user', function(_id){
    if(!Meteor.call('collection.permission.check', this.userId, 'UsersCollection', 'r'))
      return

    return UsersCollection.find({ deleted: {$ne: true}, _id })
  });

  Meteor.methods({
    'organization.insert'(data, userIp) {
      if(!Meteor.call('collection.permission.check', Meteor.userId, 'UsersCollection', 'cu'))
        return

      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'organization.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      const confirmationId = Random.id()
      const confirmCode = getRandomCode()

      const finalData = {
        username: data.bin,
        email: data.email,
        password: data.password,
        profile: {
          isOrganization: true,
          branch: data.branch,
          name: data.name,
          shortName: data.shortName,
          representative: data.representative,
          phone: data.phone,
          mobilePhone: data.mobilePhone,
          address: data.address,
          roles: [{value: '0'}],
          confirmationId,
          confirmCode
        }
      }

      const userId = Accounts.createUser(finalData)
      sendEmail(
        data.email,
        'Подтверждение почты',
        '',
        getEmailConfirmationHtml(confirmationId)
      )

      sendSMS(
        data.mobilePhone,
        `KazAgroGarant: Код для подтверждения моб.телефона:  ${confirmCode}`,
      )
    },
    'organization.delete'(_id, userIp) {
      if(!Meteor.call('collection.permission.check', Meteor.userId, 'UsersCollection', 'd'))
        return

      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'organization.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      UsersCollection.remove({ _id })
    },
    'organization.update'(_id, data, userIp) {
      if(!Meteor.call('collection.permission.check', Meteor.userId, 'UsersCollection', 'cu'))
        return

      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'organization.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      const theUser = UsersCollection.findOne(_id)

      const finalData = {
        profile: {
          isOrganization: true,
          branch: data.branch,
          name: data.name,
          shortName: data.shortName,
          representative: data.representative,
          phone: data.phone,
          mobilePhone: data.mobilePhone,
          address: data.address,
          roles: [{value: '0'}], //default mfo role
        }
      }

      UsersCollection.update({ _id }, { $set: finalData })

      if(data.bin !== theUser.username) {
        Accounts.setUsername(_id, data.bin)
      }

      if(data.password !== passwordPlaceholder) {
        Accounts.setPassword(_id, data.password);
      }

      if(data.email !== theUser.emails[0].address) {
        const confirmationId = Random.id()
        UsersCollection.update({ _id }, { $set: { 'profile.confirmationId': confirmationId } })
        Accounts.addEmail(_id, data.email)
        sendEmail(
          data.email,
          'Подтверждение почты',
          '',
          getEmailConfirmationHtml(confirmationId)
        )

        Accounts.removeEmail(_id, theUser.emails[0].address)
      }
    },
    'create.user'(data) {
      const log = {
        createdAt: Date.now(),
        service: 'user.register',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      const confirmationId = Random.id()
      const confirmCode = getRandomCode()

      data.profile.confirmationId = confirmationId
      data.profile.confirmCode = confirmCode

      const userId = Accounts.createUser(data)

      sendEmail(
        data.email,
        'Подтверждение почты',
        '',
        getEmailConfirmationHtml(confirmationId)
      )

      sendSMS(
        data.profile.mobilePhone,
        `KazAgroGarant: Код для подтверждения моб.телефона: ${confirmCode}`,
      )
    },
    'user.insert'(data, userIp) {
      if(!Meteor.call('collection.permission.check', Meteor.userId, 'UsersCollection', 'cu'))
        return

      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'user.insert',
        payload: {
          data,
        },
      }
      LogsCollection.insert(log)

      const confirmationId = Random.id()
      const confirmCode = getRandomCode()

      const finalData = {
        username: data.iin,
        email: data.email,
        password: data.password,
        profile: {
          branch: data.branch,
          department: data.department,
          firstName: data.firstName,
          lastName: data.lastName,
          middleName: data.middleName,
          mobilePhone: data.mobilePhone,
          roles: data.roles,
          confirmationId,
          confirmCode
        }
      }

      // if(finalData.profile.branch !== '0')
      //   delete finalData.profile.department

      const userId = Accounts.createUser(finalData)
      sendEmail(
        data.email,
        'Подтверждение почты',
        '',
        getEmailConfirmationHtml(confirmationId)
      )

      sendSMS(
        data.mobilePhone,
        `KazAgroGarant: Код для подтверждения моб.телефона: ${confirmCode}`,
      )
    },
    'user.delete'(_id, userIp) {
      if(!Meteor.call('collection.permission.check', Meteor.userId, 'UsersCollection', 'd'))
        return

      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'user.delete',
        payload: {
          _id
        },
      }
      LogsCollection.insert(log)

      UsersCollection.remove({ _id })
    },
    'user.update'(_id, data, userIp) {
      if(!Meteor.call('collection.permission.check', Meteor.userId, 'UsersCollection', 'cu'))
        return

      const {lastName, firstName, middleName} = Meteor.user().profile
      const FIO = `${lastName} ${firstName} ${middleName ? middleName : ''}`
      const log = {
        userIp: userIp,
        userIIN: Meteor.user().username,
        userFIO: FIO,
        createdAt: Date.now(),
        service: 'user.update',
        payload: {
          _id,
          data
        },
      }
      LogsCollection.insert(log)

      const theUser = UsersCollection.findOne(_id)

      const finalData = {
        profile: {
          branch: data.branch,
          department: data.department,
          firstName: data.firstName,
          lastName: data.lastName,
          middleName: data.middleName,
          mobilePhone: data.mobilePhone,
          roles: data.roles,
        }
      }

      if(finalData.profile.branch !== '0')
        delete finalData.profile.department

      UsersCollection.update({ _id }, { $set: finalData })

      if(data.iin !== theUser.username) {
        Accounts.setUsername(_id, data.iin)
      }

      if(data.password !== passwordPlaceholder) {
        Accounts.setPassword(_id, data.password);
      }

      if(data.email !== theUser.emails[0].address) {
        const confirmationId = Random.id()
        UsersCollection.update({ _id }, { $set: { 'profile.confirmationId': confirmationId } })
        Accounts.addEmail(_id, data.email)
        sendEmail(
          data.email,
          'Подтверждение почты',
          '',
          getEmailConfirmationHtml(confirmationId)
        )
        Accounts.removeEmail(_id, theUser.emails[0].address)
      }
    },
    'email.exists'(email, exception) {
      if(!exception)
        return UsersCollection.find({ "emails.0.address": email }, { limit: 1 }).count() > 0
      else {
        return UsersCollection.find({ _id: { $ne: exception }, "emails.0.address": email }, { limit: 1 }).count() > 0
      }
    },
    'mobile.phone.exists'(mobilePhone, exception) {
      if(!exception)
        return UsersCollection.find({ "profile.mobilePhone": mobilePhone }, { limit: 1 }).count() > 0
      else {
        return UsersCollection.find({ _id: { $ne: exception }, "profile.mobilePhone": mobilePhone }, { limit: 1 }).count() > 0
      }
    },
    'phone.exists'(phone, exception) {
      if(!exception)
        return UsersCollection.find({ "profile.phone": phone }, { limit: 1 }).count() > 0
      else {
        return UsersCollection.find({ _id: { $ne: exception }, "profile.phone": phone }, { limit: 1 }).count() > 0
      }
    },
    'iin.bin.exists'(username, exception) {
      if(!exception)
        return UsersCollection.find({ "username": username }, { limit: 1 }).count() > 0
      else {
        return UsersCollection.find({ _id: { $ne: exception }, "username": username }, { limit: 1 }).count() > 0
      }
    },
    'send.confirmation.email'(to) {
      const confirmationId = Random.id()
      UsersCollection.update({ 'emails.0.address': to }, { $set: { 'profile.confirmationId': confirmationId } })
      sendEmail(
        to,
        'Подтверждение почты',
        '',
        getEmailConfirmationHtml(confirmationId)
      )
    },
    'verify.email'(_id) {
      const theUser = UsersCollection.findOne({ 'profile.confirmationId': _id })
      if(theUser) {
        UsersCollection.update({ 'profile.confirmationId': _id}, {
          $unset: {
            'profile.confirmationId': ''
          },
          $set: {
            'emails.0.verified': true,
            'profile.roles': !theUser.profile.roles || !theUser.profile.roles.length ? [{
              value: '0',
              label: 'МФО',
            }] : theUser.profile.roles
          }
        })
        return theUser.emails[0].address
      }
      else {
        return false
      }
    },
    'send.confirmation.phone'(userId) {
      const confirmCode = getRandomCode()
      const theUser = UsersCollection.findOne(userId)

      UsersCollection.update({ _id: userId }, { $set: { 'profile.confirmCode': confirmCode } })

      sendSMS(
        theUser.profile.mobilePhone,
        `KazAgroGarant: Код для подтверждения моб.телефона: ${confirmCode}`,
      )
    },
    'verify.phone'(userId, code) {
      const theUser = UsersCollection.findOne(userId)

      if(theUser.profile.confirmCode !== code) {
        UsersCollection.update({ _id: userId }, {
          $inc: { 'profile.failNumber': 1 }
        })

        return UsersCollection.findOne(userId).profile.failNumber
      }

      UsersCollection.update({ _id: userId }, {
        $unset: {
          'profile.confirmCode': '',
          'profile.failNumber': '',
        },
        $set: { 'profile.mobileConfirmed': true }
      })

      return -1
    },
    'change.number'(userId, newNumber) {
      const theUser = UsersCollection.findOne(userId)

      if(theUser.profile.mobilePhone === newNumber)
        return false

      UsersCollection.update({ _id: userId }, { $set: { 'profile.mobilePhone': newNumber } })
    },
    'check.emailVerification'( username ) {
      const theUser = UsersCollection.findOne({ username })
      const {verified, address} = theUser.emails[0]
      const {firstName, lastName} = theUser.profile
      const passwordResetId = Random.id()

      if(verified) {
        UsersCollection.update({ 'emails.0.address': address }, { $set: { 'profile.passwordResetId': passwordResetId } })
        sendEmail(
          address,
          'Восстановление пароля',
          '',
          getResetPasswordHtml(passwordResetId, firstName, lastName)
        )
      }

      return theUser.emails[0]
    },
    'reset.password'(_id, newPassword) {
      const theUser = UsersCollection.findOne({ 'profile.passwordResetId': _id })

      if(theUser) {
        UsersCollection.update({ 'profile.passwordResetId': _id}, {
          $unset: {
            'profile.confirmationId': ''
          }
        })
        Accounts.setPassword(theUser._id, newPassword, [true])
      }
      else {
        return false
      }
    }
  });
}
