/**
* Author: Pride Systems
* Form wizard page
*/

!function($) {
    "use strict";

    var FormWizard = function() {};

    // FormWizard.prototype.createBasic = function($form_container) {
    //     $form_container.children("div").steps({
    //         headerTag: "h3",
    //         bodyTag: "section",
    //         transitionEffect: "slideLeft",
    //         onFinishing: function (event, currentIndex) { 
    //             //NOTE: Here you can do form validation and return true or false based on your validation logic
    //             console.log("Form has been validated!");
    //             return true; 
    //         }, 
    //         onFinished: function (event, currentIndex) {
    //            //NOTE: Submit the form, if all validation passed.
    //             console.log("Form can be submitted using submit method. E.g. $('#basic-form').submit()"); 
    //             $("#basic-form").submit();

    //         }
    //     });
    //     return $form_container;
    // },
    //creates form with validation
    // FormWizard.prototype.createValidatorForm = function($form_container) {
    //     $form_container.validate({
    //         errorPlacement: function errorPlacement(error, element) {
    //             element.after(error);
    //         }
    //     });
    //     $form_container.children("div").steps({
    //         headerTag: "h3",
    //         bodyTag: "section",
    //         transitionEffect: "slideLeft",
    //         onStepChanging: function (event, currentIndex, newIndex) {
    //             $form_container.validate().settings.ignore = ":disabled,:hidden";
    //             return $form_container.valid();
    //         },
    //         onFinishing: function (event, currentIndex) {
    //             $form_container.validate().settings.ignore = ":disabled";
    //             return $form_container.valid();
    //         },
    //         onFinished: function (event, currentIndex) {
    //             alert("Submitted!");
    //         }
    //     });

    //     return $form_container;
    // },
    //creates vertical form
    FormWizard.prototype.createVertical = function($form_container) {
        $form_container.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slide",
            stepsOrientation: "vertical",
            enableAllSteps: true,
            titleTemplate: '<span class="number">#index#</span> #title# '
            //this option does not work unfortunately atm, issue of the plugin, so not using it
            //saveState: true
        });

        //for some fuckedup reason enableAllSteps: true option fucks up the css for steps and doesn't apply the done class to steps.
        //so doing it manually here.
        $.each($("ul[role='tablist'] li"), function(index, value){ 
            var className = $(value).attr('class');
            if (!className || className === "last") {
                $(value).addClass('done');
            }
        });

        return $form_container;
    },
    FormWizard.prototype.init = function() {
        //initialzing various forms

        //basic form
        //this.createBasic($("#basic-form"));

        //form with validation
        //this.createValidatorForm($("#wizard-validation-form"));

        //vertical form
        this.createVertical($("#wizard-vertical"));
    },
    //init
    $.FormWizard = new FormWizard, $.FormWizard.Constructor = FormWizard
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.FormWizard.init()
}(window.jQuery);