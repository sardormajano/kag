module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '195.189.68.130',
      username: 'dauren',
      // pem: './path/to/pem'
      password: '4thebest'
      // or neither for authenticate from ssh-agent
    }
  },

  meteor: {
      name: 'kag',
      path: '..',
      volumes: {
        '/docs':'/docs'
      },
      dockerImage: 'abernix/meteord:base',
      servers: {
        one: {}
      },
      buildOptions: {
        serverOnly: true,
      },
      env: {
        ROOT_URL: 'http://195.189.68.130/',
        MONGO_URL: 'http://195.189.68.130:3001/'
      },
      //dockerImage: 'kadirahq/meteord'
      deployCheckWaitTime: 120,
      enableUploadProgressBar: true
  },

  mongo: {
    port: 27017,
    version: '3.4.1',
    servers: {
      one: {}
    }
  }
};
